export const addDays = function (stats) {
    return stats.map((stat, index) => {
        const statCpy = {}
        Object.assign(statCpy, stat)
        statCpy.day = index

        return statCpy
    })
}
