import functools


def check_running(method):

    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):

        if self._simulation is None or not self._simulation.is_alive():
            raise RuntimeError('Simulation is not running!')

        return method(self, *args, **kwargs)

    return wrapper
