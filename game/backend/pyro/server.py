import os
import signal
import logging

import Pyro5.api
import Pyro5.client
import Pyro5.core
import Pyro5.server

from core.base.config import LOGGER_NAME
from core.base.exceptions import StopSimulation, AbortSimulation
from .utils import check_running


log = logging.getLogger(LOGGER_NAME)


@Pyro5.server.behavior(instance_mode='single')
class SimulationServer:

    def __init__(self, game_id, bracket, host=None, port=None, ns_host=None, ns_port=None):
        self._game_id = game_id
        self._bracket = bracket
        self._host = host
        self._port = port or 0
        self._ns_host = ns_host or ''
        self._ns_port = ns_port
        self._simulation = None
        self._client = None

    @property
    def _name(self):
        return ''.join([str(ord(c)) for c in (f'Game.ID.{self._game_id}.Bracket.{self._bracket}'.replace(' ', ''))])

    @property
    def client(self):

        if self._client is None:
            url = f'PYRONAME:{self._name}'

            if self._ns_host:
                url = f'{url}@{self._ns_host}'

                if self._ns_port:
                    url = f'{url}:{self._ns_port}'

            self._client = Pyro5.client.Proxy(url)

        return self._client

    @Pyro5.server.expose
    @property
    @check_running
    def simulation(self):
        return self._simulation.to_dict(include_plants=True)

    @Pyro5.server.expose
    @check_running
    def toggle_suspend(self):
        return self._simulation.toggle_suspend()

    @Pyro5.server.expose
    @check_running
    def toggle_fast_forward(self):
        return self._simulation.toggle_fast_forward()

    @Pyro5.server.expose
    @check_running
    def reset_clock(self, day):
        self._simulation.reset_clock(day)

    @Pyro5.server.expose
    @check_running
    def set_queue_policy(self, plant_id, unit_id, policy):
        log.info(
            f'Plant {plant_id}: Change queue policy to {policy} ({unit_id})')
        self._simulation.set_queue_policy(plant_id, unit_id, policy)

    @Pyro5.server.expose
    @check_running
    def set_lot_size(self, plant_id, unit_id, size):
        log.info(f'Plant {plant_id}: Change lot size to {size} ({unit_id})')
        self._simulation.set_lot_size(plant_id, unit_id, size)

    @Pyro5.server.expose
    @check_running
    def set_contract(self, plant_id, unit_id, contract):
        log.info(
            f'Plant {plant_id}: Change contract to {contract} ({unit_id})')
        self._simulation.set_contract(plant_id, unit_id, contract)

    @Pyro5.server.expose
    @check_running
    def set_reorder_point(self, plant_id, unit_id, reorder_point):
        log.info(
            f'Plant {plant_id}: Change reorder point to {reorder_point} ({unit_id})')
        self._simulation.set_reorder_point(plant_id, unit_id, reorder_point)

    @Pyro5.server.expose
    @check_running
    def set_order_size(self, plant_id, unit_id, size):
        log.info(f'Plant {plant_id}: Change order size to {size} ({unit_id})')
        self._simulation.set_order_size(plant_id, unit_id, size)

    @Pyro5.server.expose
    @check_running
    def purchase_machine(self, plant_id, unit_id, quantity):
        n = self._simulation.manage_machines(
            plant_id, unit_id, quantity, todo='purchase')
        log.info(f'Plant {plant_id}: Purchase {n} machines ({unit_id})')

        return n

    @Pyro5.server.expose
    @check_running
    def retire_machine(self, plant_id, unit_id, quantity):
        n = self._simulation.manage_machines(
            plant_id, unit_id, quantity, todo='retire')
        log.info(f'Plant {plant_id}: Retire {n} machines ({unit_id})')

        return n

    def run(self, simulation):
        register_signal_handlers()

        self._simulation = simulation
        self._simulation.stop_callback = stop_callback
        self._simulation.start()

        try:

            with Pyro5.server.Daemon(self._host, self._port) as daemon:
                ns = Pyro5.core.locate_ns(self._ns_host, self._ns_port)
                uri = daemon.register(self)
                ns.register(self._name, uri)
                log.info(f'Registered {self} at {uri}')
                daemon.requestLoop()

        except StopSimulation:

            if self._simulation.exc is not None:
                raise self._simulation.exc

        finally:

            self._simulation.stop_callback = None
            self._simulation.stop()

    def __str__(self):
        return f'<SimulationServer {self._name}>'


def stop_callback():
    os.kill(os.getpid(), signal.SIGINT)


def register_signal_handlers():

    def stop_signal_handler(signal, frame):
        raise StopSimulation

    signal.signal(signal.SIGINT, stop_signal_handler)

    def abort_signal_handler(signal, frame):
        raise AbortSimulation

    signal.signal(signal.SIGTERM, abort_signal_handler)
