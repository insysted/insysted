from sqlalchemy.dialects.postgresql import UUID
import pytest

from webapp.app import create_app
from webapp.db.models import User, Game, Team
from webapp.middleware import db as _db


@pytest.fixture(scope='session')
def app():
    _app = create_app('webapp.testing')

    with _app.test_request_context():
        yield _app


@pytest.fixture
def db(app):
    _db.create_all()

    yield _db

    _db.session.remove()
    _db.drop_all()


@pytest.fixture
def client(app):

    with app.test_client() as _client:
        yield _client


@pytest.fixture
def app_user(db):

    def _get_user(
            name='John Doe',
            email='john.doe@testing.com',
            password='john.doe.the.best',
            **kwargs):

        user = User(name=name, email=email, password=password, **kwargs)

        db.session.add(user)
        db.session.commit()

        return user

    return _get_user


@pytest.fixture
def app_game(db):
    def _get_game(
            id=1,
            title="title",
            description="description",
            simulation={},
            layout={},
            progress={},
            owner_id=None,
            template_id=None,
            leaderboard=True,
            **kwargs):

        game = Game(
            id=id, title=title,
            description=description,
            simulation=simulation,
            layout=layout,
            progress=progress,
            owner_id=owner_id,
            template_id=template_id,
            leaderboard=True,
            **kwargs)

        db.session.add(game)
        db.session.commit()

        return game

    return _get_game


@pytest.fixture
def app_team(db):
    def _get_team(
            name="name",
            game_id=1,
            bracket="a",
            **kwargs):

        team = Team(name=name, game_id=game_id, bracket=bracket, **kwargs)

        db.session.add(team)
        db.session.commit()

        return team

    return _get_team


@pytest.fixture
def conf_dict():
    return {
        'template': {
            'seed': 1,
            'clock': {
                'prior_days': 50,
                'play_days': 100,
                'post_days': 50,
                'days_per_hour': 1
            },
            'plant': {
                'wip_orders_max': 5000,
                'cash': 1000000,
                'units': {
                    'Workshop #1': {
                        'class': 'workshop',
                        'params': {
                            'machine': {
                                'purchase_price': 80000,
                                'retire_price': 50000,
                                'item_processing_time_type': 'constant',
                                'item_processing_time_constant': 10,
                                'item_processing_time_avg': 10,
                                'item_processing_time_sigma': 0,
                                'breakdown': {
                                    'proba': 0.15
                                },
                                'lot_setup_time_type': 'constant',
                                'lot_setup_time_constant': 5,
                                'lot_setup_time_avg': 5,
                                'lot_setup_time_sigma': 1,
                                'breakdown_type': 'constant',
                                'breakdown_constant': 5,
                                'breakdown_avg': 5,
                                'breakdown_sigma': 1,
                            },
                            'machine_quantity': 10,
                            'queue_policy': 'fifo'
                        }
                    },
                    'Warehouse #1': {
                        'class': 'warehouse',
                        'params': {
                            'supplier': 'Supplier',
                            'reorder_point': 2500,
                            'order_size': 1,
                            'inventory': 5000,
                            'unit_storage_cost': 0.1
                        }
                    },
                    'Sales Office': {
                        'class': 'office',
                        'params': {
                            'lot_size': 10,
                            'contracts': {
                                'default': {
                                    'lead_time': 72,
                                    'lead_time_max': 120,
                                    'batch_price': 100,
                                    'fix_price': 50,
                                    'batch_size': 10
                                },
                                'lucrative': {
                                    'lead_time': 48,
                                    'lead_time_max': 72,
                                    'batch_price': 150,
                                    'fix_price': 100,
                                    'batch_size': 10
                                }
                            },
                            'contract_active': 'default'
                        }
                    },
                    'Customer': {
                        'class': 'customer',
                        'params': {
                            'order_interarrival_time_normal_avg': 60,
                            'order_interarrival_time_normal_sigma': 0,
                            'order_interarrival_time_constant': 60,
                            'order_interarrival_time_exponential_lambda': 5,
                            'order_interarrival_time_type': 'constant',

                            'order_ammount_normal_avg': 1,
                            'order_ammount_normal_sigma': 0,
                            'order_ammount_constant': 1,
                            'order_ammout_type': 'constant',
                            'office': 'Sales Office'
                        }
                    },
                    'Supplier': {
                        'class': 'supplier',
                        'params': {
                            'delivery_time_type': 'constant',
                            'delivery_time_constant': 72,
                            'delivery_time_normal_avg': 72,
                            'delivery_time_normal_sigma': 24,
                            'lot_size': 1,
                            'contracts': {
                                'default': {
                                    'lead_time': 48,
                                    'lead_time_max': 72,
                                    'batch_price': 100,
                                    'fix_price': 150,
                                    'batch_size': 10
                                }
                            },
                            'contract_active': 'default',
                            'warehouse': 'Warehouse #1'
                        }
                    },
                    'Shipment #1': {
                        'class': 'shipment',
                        'params': {
                            'unit_storage_cost': 0
                        }
                    },
                    'Matcher #1': {
                        'class': 'matcher',
                        'params': {
                            'warehouse': 'Warehouse #1',
                            'shortage_cost': 0
                        }
                    }
                },
                'pipeline': {
                    'Product #1': [
                        {
                            'unit': 'Sales Office'
                        },
                        {
                            'unit': 'Matcher #1'
                        },
                        {
                            'unit': 'Workshop #1'
                        },
                        {
                            'unit': 'Shipment #1'
                        }
                    ]
                }
            }
        }
    }
