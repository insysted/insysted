import datetime
from http import HTTPStatus
from unittest import mock

from flask import url_for
from freezegun import freeze_time

from webapp.middleware import db


@freeze_time('1982-12-16')
@mock.patch('webapp.blueprints.api.auth.utils.get_jti')
@mock.patch('webapp.blueprints.api.auth.controller.create_refresh_token')
@mock.patch('webapp.blueprints.api.auth.controller.create_access_token')
def test_user_login_success(access_mock, refresh_mock, jti_mock, client, app_user):
    user_pass = 'secret'
    user = app_user(password=user_pass, is_confirmed=True)

    payload = {
        'identity': user.identity,
        'password': user_pass,
    }

    access_token = 'user-access-token'
    refresh_token = 'user-refresh-token'

    access_mock.return_value = access_token
    refresh_mock.return_value = refresh_token
    jti_mock.return_value = 'jti'

    resp = client.post(
        url_for('auth.login'),
        content_type='application/json',
        json=payload,
    )

    assert resp.status_code == HTTPStatus.OK

    access_mock.assert_called_once_with(user.identity)
    refresh_mock.assert_called_once_with(user.identity)

    db.session.refresh(user)
    assert user.login_at == datetime.datetime(1982, 12, 16)

    resp_data = resp.get_json()
    expect_data = {
        'auth': {
            'tokens': {
                'access': access_token,
                'refresh': refresh_token,
            },
            'user': {
                'id': user.id,
                'roles': [str(r) for r in user.roles],
            },
        },
        'success': True,
    }

    assert resp_data == expect_data


def test_user_login_wrong_password_failure(client, app_user):
    user = app_user(is_confirmed=True)

    payload = {
        'identity': user.identity,
        'password': 'wrong-password',
    }

    resp = client.post(
        url_for('auth.login'),
        content_type='application/json',
        json=payload,
    )

    assert resp.status_code == HTTPStatus.UNAUTHORIZED


def test_user_login_account_not_found_failure(client, db):
    db.create_all()

    payload = {
        'identity': 'user.does.not.exist@example.com',
        'password': 'secret',
    }

    resp = client.post(
        url_for('auth.login'),
        content_type='application/json',
        json=payload,
    )

    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_user_login_account_not_confirmed_failure(client, app_user):
    user_pass = 'secret'
    user = app_user(password=user_pass, is_confirmed=False)

    payload = {
        'identity': user.identity,
        'password': user_pass,
    }

    resp = client.post(
        url_for('auth.login'),
        content_type='application/json',
        json=payload,
    )

    assert resp.status_code == HTTPStatus.NOT_FOUND
