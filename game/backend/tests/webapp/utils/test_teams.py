from webapp.db.models import GameStatus
import pytest

from webapp.blueprints.api.utils.teams import (
    TeamExistsException,
    GameFinishedException,
    GameRunningException,
    GameNotFoundException,
    GameNotOwnedException,
    save_team,
    UserNotFoundException,
    TeamNotFoundException,
    UserIsInTeamException,
    join_team_by_id
)

from webapp.db.models import Team


def test_save_team(db, app_game):
    game = app_game(id=123)
    team = Team(name="name", bracket="a")
    save_team(team, game.id, None)


@pytest.mark.parametrize(
    'status',
    [
        GameStatus.SUSPENDED,
        GameStatus.ABORTED,
        GameStatus.DONE,
        GameStatus.STARTED
    ]
)
def test_game_running(db, app_game, status):
    game = app_game(status=status)
    team = Team(name="name")
    with pytest.raises(GameRunningException):
        save_team(team, game.id)


def test_game_not_found(db):
    team = Team(name="name")
    with pytest.raises(GameNotFoundException):
        save_team(team, 404)


def test_not_owned(db, app_game, app_user):
    user = app_user()
    game = app_game(owner_id=user.id)
    team = Team(name="name")

    with pytest.raises(GameNotOwnedException):
        save_team(team, game.id, user.id + 1)


def test_join_team(db, app_game, app_user, app_team):
    game = app_game()
    user = app_user()
    team = app_team(game_id=game.id)
    join_team_by_id(user.id, team.id)


def test_join_UserNotFound(db, app_game, app_team):
    game = app_game()
    team = app_team(game_id=game.id)
    with pytest.raises(UserNotFoundException):
        join_team_by_id(1, team.id)


def test_join_TeamNotFound(db, app_game, app_user):
    game = app_game()
    user = app_user()
    team = Team()
    with pytest.raises(TeamNotFoundException):
        join_team_by_id(user.id, team.id)


def test_join_UserIsInTeamException(db, app_game, app_user, app_team):
    game = app_game()
    user = app_user()
    team = app_team(game_id=game.id)
    join_team_by_id(user.id, team.id)
    with pytest.raises(UserIsInTeamException):
        join_team_by_id(user.id, team.id)


@pytest.mark.parametrize(
    'status',
    [
        GameStatus.ABORTED,
        GameStatus.DONE,
    ]
)
def test_join_game_running(db, app_game, app_user, app_team, status):
    game = app_game(status=status)
    user = app_user()
    team = app_team(game_id=game.id)
    with pytest.raises(GameFinishedException):
        join_team_by_id(user.id, team.id)
