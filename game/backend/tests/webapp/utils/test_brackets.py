from webapp.blueprints.api.utils.teams import num_to_bracket_name, set_bracket_hash_based, set_bracket_round_robin
import pytest


@pytest.mark.parametrize(
    'num, expected',
    [
        (0, 'A'),
        (1, 'B'),
        (25, 'Z'),
        (26, 'BA'),
        (26**2, 'BAA')
    ]
)
def test_num_to_bracket_name(num, expected):
    assert num_to_bracket_name(num) == expected


def test_set_bracket_hash_based_simple(db, app_game, app_team):
    game = app_game()
    team = app_team(game_id=game.id)
    before = team.bracket

    set_bracket_hash_based(team, game)

    assert before != team.bracket


def test_set_bracket_hash_based_complex(db, app_game, app_team):
    number_of_brackets = 4
    number_of_teams = 100

    game = app_game(number_of_brackets=number_of_brackets)

    bracket_counts = {}

    for i in range(number_of_teams):
        team = app_team(game_id=game.id, name=str(i))
        set_bracket_hash_based(team, game)
        if team.bracket not in bracket_counts:
            bracket_counts[team.bracket] = 1
        else:
            bracket_counts[team.bracket] += 1

    assert len(bracket_counts.keys()) == number_of_brackets

    for key in bracket_counts:
        assert bracket_counts[key] * number_of_brackets < number_of_teams * 1.5
        assert bracket_counts[key] * number_of_brackets > number_of_teams * 0.5


def test_set_bracket_round_robin_complex(db, app_game, app_team):
    number_of_brackets = 4
    number_of_teams = 100

    game = app_game(number_of_brackets=number_of_brackets)

    bracket_counts = {}

    for i in range(number_of_teams):
        team = app_team(game_id=game.id, name=str(i))
        set_bracket_round_robin(team, game)
        if team.bracket not in bracket_counts:
            bracket_counts[team.bracket] = 1
        else:
            bracket_counts[team.bracket] += 1

    assert len(bracket_counts.keys()) == number_of_brackets

    for key in bracket_counts:
        assert bracket_counts[key] * number_of_brackets == number_of_teams
