from moodle.moodleApi import MoodleApi, MoodleTokenInvalidException, MoodleInvalidCourseException
from moodle.moodleTeams import get_team_obj_using_moodle

import pytest
import os


@pytest.fixture
def api_token():
    return os.getenv('MOODLE_API_TOKEN')


@pytest.fixture
def course_id():
    return os.getenv('MOODLE_COURSE')


@pytest.fixture
def moodle_url():
    return os.getenv('MOODLE_URL') + "/some?randomUrl&to#test&that%this-is-ignored"


@pytest.fixture
def moodleApi(moodle_url, api_token):
    return MoodleApi(moodle_url, api_token)


skip_reason = "Moodle testing is disabled set TEST_MOODLE to any value"


@pytest.mark.skipif(len(os.getenv('TEST_MOODLE')) == 0, reason=skip_reason)
def test_token(api_token):
    assert len(api_token) > 1


@pytest.mark.skipif(len(os.getenv('TEST_MOODLE')) == 0, reason=skip_reason)
@pytest.mark.parametrize(
    'token',
    [
        None,
        '',
    ]
)
def test_throws_if_token_is_none(token):
    with pytest.raises(MoodleTokenInvalidException):
        m = MoodleApi("https://url.com", token)


@pytest.mark.skipif(len(os.getenv('TEST_MOODLE')) == 0, reason=skip_reason)
def test_throws_if_token_invalid(moodle_url, course_id):
    m = MoodleApi(moodle_url, 'token')
    with pytest.raises(MoodleTokenInvalidException):
        m.get_groupings(course_id)


@pytest.mark.skipif(len(os.getenv('TEST_MOODLE')) == 0, reason=skip_reason)
def test_throws_if_course_invalid(moodleApi):
    with pytest.raises(MoodleInvalidCourseException):
        moodleApi.get_groupings(123)


@pytest.mark.skipif(len(os.getenv('TEST_MOODLE')) == 0, reason=skip_reason)
def test_get_course_groupings(moodleApi, course_id):
    assert len(moodleApi.get_course_groupings_and_groups(course_id)) > 0


@pytest.mark.skipif(len(os.getenv('TEST_MOODLE')) == 0, reason=skip_reason)
def test_get_groupings_groups_no_groupings(moodleApi, course_id):
    ug = moodleApi.get_groupings_groups([], course_id)[0]

    assert ug.id == '-1'
    assert ug.name == 'ungrouped'
    assert len(ug.groups) > 0


@pytest.mark.skipif(len(os.getenv('TEST_MOODLE')) == 0, reason=skip_reason)
def test_create_team_using_moodle(moodle_url, course_id):
    user_data = dict(
        lis_outcome_service_url=moodle_url,
        context_id=course_id,
        group=os.getenv('MOODLE_GROUPID')
    )

    team = get_team_obj_using_moodle(1, user_data)

    assert len(team.name) > 0
    assert len(team.bracket) > 0
