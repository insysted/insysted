from unittest import mock

import pytest

from core.order import Order, OrderLot
from core.shipment import Shipment


@pytest.fixture
def shipment(plant):
    return Shipment(plant, unit_storage_cost=10)


def test_shipment_receive_lots(shipment):
    lots = []
    order_num = 10

    for _ in range(order_num):
        order = mock.MagicMock(spec=Order)
        lot = mock.MagicMock(spec=OrderLot)
        lot.configure_mock(**{'order': order, 'size': 1})
        shipment.queue.put(lot)
        lots.append(lot)

    shipment._plant.close_order.return_value = False

    shipment.update_state()

    assert len(shipment._order_lots) == order_num

    for lot in lots:
        lot.finish.assert_called()


def test_shipment_close_orders(shipment):
    lots = []
    order_num = 10

    for _ in range(order_num):
        order = mock.MagicMock(spec=Order)
        lot = mock.MagicMock(spec=OrderLot)
        lot.configure_mock(**{'order': order, 'size': 1})
        shipment.queue.put(lot)
        lots.append(lot)

    shipment._plant.close_order.return_value = True

    shipment.update_state()

    assert len(shipment._order_lots) == 0
    shipment._plant.close_order.call_count = order_num


def test_shipment_total_lots(shipment):
    shipment._plant.close_order.return_value = False
    num = 10

    for _ in range(num):
        order = mock.MagicMock(spec=Order)

        for _ in range(num):
            lot = mock.MagicMock(spec=OrderLot)
            lot.configure_mock(**{'order': order, 'size': 1})
            shipment.queue.put(lot)

    shipment.update_state()

    assert shipment.total_lots == num ** 2


def test_shipment_storage_costs(shipment):
    shipment._plant.close_order.return_value = False
    shipment._plant.simulation.new_day = True
    lot_num, lot_size = 10, 5

    for _ in range(lot_num):
        order = mock.MagicMock(spec=Order)
        lot = mock.MagicMock(spec=OrderLot)
        lot.configure_mock(**{'order': order, 'size': lot_size})
        shipment.queue.put(lot)

    shipment.update_state()

    costs = lot_num * lot_size * shipment._unit_storage_cost
    shipment._plant.pay_costs.assert_called_with(
        costs, 'shipment storage', credit=True)
