import pytest

from core.base.simulation import StatisticsMixin


@pytest.mark.parametrize(
    'type, values, expected',
    [
        ('sum', [0], 0),
        ('sum', [1], 1),
        ('sum', [1, 2, 3], 1 + 2 + 3),

        ('avg', [0], 0),
        ('avg', [1, 2], 1.5),
        ('avg', [1, 2, 3, 4, 5], 3),

        ('last', [0], 0),
        ('last', [1, 2, 3], 3),

        ('first', [0], 0),
        ('first', [1, 2, 3], 1),

        (None, [0], [0]),
        (None, [1, 2, 3], [1, 2, 3]),
    ])
def test_stats(type, values, expected):
    sm = StatisticsMixin()

    for value in values:
        sm._update_stats('metric', value, type)

    assert sm.get_stats()['metric'] == expected

    # check that the stats get cleared
    assert sm.get_stats() == dict()


def test_default():
    sm = StatisticsMixin()
    sm._set_default('metric', 'value')

    assert sm.get_stats()['metric'] == 'value'

    # check that the stats get cleared
    assert sm.get_stats() == dict()


def test_default_override():
    sm = StatisticsMixin()
    sm._set_default('metric', 'value')
    sm._update_stats('metric', 'diffrent value')

    assert sm.get_stats()['metric'] != 'value'
