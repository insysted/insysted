import pytest

from core.base.exceptions import StopSimulation
from core.clock import Clock


@pytest.mark.parametrize(
    'ticks, update_period, day',
    [
        (0, 60, 0),
        (24 * 60, 60, 1),
        (3 * 24 * 60 // 5, 5 * 60, 3),
        (5 * 24 * 60 // 15, 15 * 60, 5),
    ])
def test_clock_day(ticks, update_period, day):
    clock = Clock(ticks=ticks, tick_seconds=update_period)
    assert clock.day == day


@pytest.mark.parametrize(
    'ticks, update_period, minute',
    [
        (0, 60, 0),
        (2, 5 * 60, 10),
        (24 * 60 // 5 + 3, 5 * 60, 15),
        (24 * 6 + 5, 10 * 60, 50),
    ])
def test_clock_minute(ticks, update_period, minute):
    clock = Clock(ticks=ticks, tick_seconds=update_period)
    assert clock.minute == minute


@pytest.mark.parametrize(
    'ticks, update_period, new_day',
    [
        (0, 10, True),
        (15, 60, False),
        (24 * 60, 60, True),
        (2 * 24 * 60 + 1, 60, False),
    ])
def test_clock_new_day(ticks, update_period, new_day):
    clock = Clock(ticks=ticks, tick_seconds=update_period)
    assert clock.new_day == new_day


@pytest.mark.parametrize(
    'days, update_period, ticks',
    [
        (1, 5 * 60, 288),
        (2.5, 5 * 60, 720),
        (5.8, 10 * 60, 836),
    ])
def test_clock_to_ticks(days, update_period, ticks):
    clock = Clock(tick_seconds=update_period)
    assert clock.to_ticks(days) == ticks


@pytest.mark.parametrize(
    'update_period, ticks',
    [
        (5 * 60, 288), (10 * 60, 144), (15 * 60, 96),
    ])
def test_clock_ticks_per_day(update_period, ticks):
    clock = Clock(tick_seconds=update_period)
    assert clock._ticks_per_day == ticks


def test_clock_fast_forward():
    clock = Clock()

    assert clock.ticks == 0
    assert clock.tick_delay > 0

    assert clock.day < clock._prior_days
    assert clock.fast_forward

    clock._ticks = clock.to_ticks(clock._prior_days + 1)
    assert not clock.fast_forward

    clock._ticks = clock.to_ticks(clock._prior_days + clock._play_days + 1)
    assert clock.fast_forward


def test_clock_tick():
    clock = Clock(prior_days=1, play_days=1, post_days=1)

    with pytest.raises(StopSimulation):

        for _ in range(clock.to_ticks(clock.total_days)):
            clock.tick()


def test_clock_tick_first_day():
    clock = Clock()

    assert clock.ticks == 0
    assert clock.day == 0

    assert clock.new_day is True
    assert clock.last_tick_of_day is False

    clock.tick()

    assert clock.ticks == 1
    assert clock.day == 0

    assert clock.new_day is False
    assert clock.last_tick_of_day is False


@pytest.mark.parametrize(
    'ticks, update_period, last_tick_of_day',
    [
        (15 - 1, 60, False),
        (24 * 60 - 1, 60, True),
        (2 * 24 * 60, 60, False),
    ])
def test_clock_last_tick_of_day(ticks, update_period, last_tick_of_day):
    clock = Clock(ticks=ticks, tick_seconds=update_period)
    assert clock.last_tick_of_day == last_tick_of_day
