from unittest import mock

import pytest

from core.base.exceptions import PlantWorkshopError
from core.workshop import Workshop, Machine, MachineStatus, BreakdownBehavior
from core.queue import Queue
from .test_order import order_lot


@pytest.fixture
def workshop(plant):
    machine = {
        'purchase_price': 80000,
        'retire_price': 50000,
        'lot_setup_time_type': 'normal',
        'lot_setup_time_constant': 5,
        'lot_setup_time_avg': 5,
        'lot_setup_time_sigma': 1,
        'item_processing_time_type': 'normal',
        'item_processing_time_constant': 10,
        'item_processing_time_avg': 10,
        'item_processing_time_sigma': 0,
        'breakdown': {
            'proba': 0.15,
        },
        'breakdown_type': BreakdownBehavior.Constant,
        'breakdown_constant': 5,
        'breakdown_avg': 0,
        'breakdown_sigma': 0.25,
    }

    return Workshop(plant, machine)


@pytest.fixture
def machine():
    workshop = mock.MagicMock(spec=Workshop)
    workshop.id_ = 'workshop'
    params = {
        'workshop': workshop,
        'purchase_price': 1000,
        'retire_price': 500,
        'lot_setup_time_type': 'constant',
        'lot_setup_time_constant': 5,
        'lot_setup_time_avg': 5,
        'lot_setup_time_sigma': 1,
        'item_processing_time_type': 'constant',
        'item_processing_time_constant': 10,
        'item_processing_time_avg': 10,
        'item_processing_time_sigma': 0,
        'breakdown': {
            'proba': 0.5,
        },
        'breakdown_type': 'constant',
        'breakdown_constant': 5,
        'breakdown_avg': 0,
        'breakdown_sigma': 0.25,
    }

    return Machine(**params)


def test_machine_set_get_lot(machine, order_lot):
    base_clock = 123
    clock_ticks = 10
    machine._plant.simulation.clock.to_ticks.return_value = clock_ticks
    machine._plant.simulation.clock._ticks = base_clock
    machine._breakdown = None

    assert machine.status == MachineStatus.READY

    machine.set_lot(order_lot)

    assert machine.status == MachineStatus.BUSY

    with pytest.raises(PlantWorkshopError):
        machine.set_lot(order_lot)

    assert machine.get_lot() is None

    machine._plant.simulation.clock._ticks = base_clock + clock_ticks - 1
    machine.update_state()
    with pytest.raises(PlantWorkshopError):
        machine.set_lot(order_lot)
    assert machine.get_lot() is None

    machine._plant.simulation.clock._ticks = base_clock + clock_ticks
    machine.update_state()
    assert machine.status == MachineStatus.READY
    assert machine.get_lot() == order_lot


@mock.patch('numpy.random.Generator')
def test_machine_processing_lot(Generator, machine, order_lot):
    normalRnt = mock.MagicMock()
    normalRnt.normal = mock.MagicMock(return_value=1)
    Generator.return_value = normalRnt

    machine._breakdown = None

    base_clock = 123
    clock_ticks = 10
    machine._plant.simulation.clock.to_ticks.return_value = clock_ticks
    machine._plant.simulation.clock._ticks = base_clock

    assert machine.status == MachineStatus.READY

    machine.set_lot(order_lot)

    assert machine.status == MachineStatus.BUSY

    machine._plant.simulation.clock._ticks = base_clock + clock_ticks - 1
    machine.update_state()
    assert machine.status == MachineStatus.BUSY

    machine._plant.simulation.clock._ticks = base_clock + clock_ticks
    machine.update_state()
    assert machine.status == MachineStatus.READY


@mock.patch('numpy.random.Generator')
@mock.patch('core.workshop.Machine._boom', new_callable=mock.PropertyMock)
def test_machine_breakage_normal(machine_boom, Generator, machine, order_lot):
    normalRnt = mock.MagicMock()
    normalRnt.normal = mock.MagicMock(return_value=1)
    Generator.return_value = normalRnt

    machine_boom.return_value = True

    base_clock = 123
    clock_ticks = 10
    machine._plant.simulation.clock.to_ticks_from_minutes.return_value = clock_ticks
    machine._plant.simulation.clock._ticks = base_clock
    machine.breakdown_type = BreakdownBehavior.Normal

    assert machine.status == MachineStatus.READY

    machine.set_lot(order_lot)

    assert machine.status == MachineStatus.BUSY

    machine.update_state()

    assert machine.status == MachineStatus.BROKEN

    machine.rng.normal.assert_called_with(
        machine.breakdown_avg,
        machine.breakdown_sigma)

    machine._plant.simulation.clock._ticks = base_clock + clock_ticks - 1
    machine.update_state()
    assert machine.status == MachineStatus.BROKEN

    machine._plant.simulation.clock._ticks = base_clock + clock_ticks
    machine.update_state()
    assert machine.status == MachineStatus.BUSY


@mock.patch('numpy.random.Generator')
@mock.patch('core.workshop.Machine._boom', new_callable=mock.PropertyMock)
def test_machine_breakage_contant(machine_boom, Generator, machine, order_lot):
    normalRnt = mock.MagicMock()
    normalRnt.normal = mock.MagicMock(return_value=1)
    Generator.return_value = normalRnt

    machine_boom.return_value = True

    base_clock = 123
    clock_ticks = 10
    machine._plant.simulation.clock.to_ticks_from_minutes.return_value = clock_ticks
    machine._plant.simulation.clock._ticks = base_clock
    machine.breakdown_type = BreakdownBehavior.Constant

    assert machine.status == MachineStatus.READY

    machine.set_lot(order_lot)

    assert machine.status == MachineStatus.BUSY

    machine.update_state()

    assert machine.status == MachineStatus.BROKEN

    # if get_rng is never called machine.rng is not set -> no call to rng.normal,
    # however if it is set, we can not assume that it was called
    if machine.rng is not None:
        machine.rng.normal.assert_not_called()

    machine._plant.simulation.clock._ticks = base_clock + clock_ticks - 1
    machine.update_state()
    assert machine.status == MachineStatus.BROKEN

    machine._plant.simulation.clock._ticks = base_clock + clock_ticks
    machine.update_state()
    assert machine.status == MachineStatus.BUSY


def test_workshop_purchase_machine(workshop):
    assert len(workshop.machines) == 0

    workshop.purchase_machine()
    machine = workshop.machines[0]

    workshop.plant.pay_costs.assert_called_with(
        machine.purchase_price, "machine purchase")
    assert len(workshop.machines) == 1


def test_workshop_retire_machine(workshop):
    assert len(workshop.machines) == 0

    with pytest.raises(RuntimeError):
        workshop.retire_machine()

    workshop.purchase_machine()
    machine = workshop.machines[0]
    workshop.retire_machine()

    workshop.plant.receive_payment.assert_called_with(machine.retire_price)
    assert len(workshop._machines) == 0


def test_workshop_processing_loop(workshop):
    workshop.queue = mock.MagicMock(spec=Queue)
    workshop.queue.pop.return_value = 'new_lot'

    def machine_mock():
        machine = mock.MagicMock(spec=Machine)
        attrs = {
            'id_': 'abc123',
            'purchase_price': 1000,
            'status': MachineStatus.READY,
            'get_lot.return_value': 'fin_lot',
        }
        machine.configure_mock(**attrs)

        return machine

    workshop._initialize_machine = machine_mock

    for _ in range(3):
        workshop.purchase_machine()

    workshop.update_state()

    for machine in workshop.machines:
        machine.update_state.assert_called()
        machine.get_lot.assert_called()
        workshop.plant.pipeline_transfer.assert_called_with('fin_lot')
        workshop.queue.pop.assert_called()
        machine.set_lot.assert_called_with('new_lot')
