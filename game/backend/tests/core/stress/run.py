#!/usr/bin/env python3
import sys
import json
import time
import logging
from pathlib import Path

import click
import numpy as np
from joblib import Parallel, delayed

from core.simulation import Simulation
from core.base.config import LOGGER_NAME
from core.base.exceptions import StopSimulation


libpath = Path(__file__).absolute().parent.parent.parent.parent
sys.path.append(str(libpath))


CLOCK_UPDATE_PERIOD_MINUTES = (5, 6, 10, 12, 15)


def stress_test(test_i, test_num, conf_dict):
    print(f'>> Start {test_i} of {test_num} tests...', flush=True)
    logging.getLogger(LOGGER_NAME).setLevel(logging.ERROR)

    sim = Simulation.from_dict(conf_dict)
    sim.add_plant('The Chocolate Factory')
    sim.start()

    t_start = time.time()
    sim.join()
    t_elapsed = time.time() - t_start

    print(
        f'<< Test {test_i} done. Elapsed time {t_elapsed:.2f} sec', flush=True)

    return t_elapsed


@click.command()
@click.option('-c', '--config-file', default='simulation.json', help='Path to config file.')
@click.option('-r', '--results-file', default='results.json', help='Path to results file.')
@click.option('-d', '--days', type=int, default=100, help='Number of simulated days.')
@click.option('-t', '--test-runs', type=int, default=10, help='Number of test runs per parameter.')
@click.option('-j', '--jobs', type=click.IntRange(min=-1), default=-1, help='Number of parallel jobs.')
def start_tests(config_file, results_file, days, test_runs, jobs):
    """Start core stress tests.
    """

    with open(config_file) as f:
        conf_dict = json.load(f)

    # Run in a fast forward mode, i.e., only prior days are defined:
    conf_dict['template']['clock'].update({
        'prior_days': days,
        'play_days': 0,
        'post_days': 0,
    })

    print('Start stress tests:')
    results = []

    try:

        for update_period in CLOCK_UPDATE_PERIOD_MINUTES:
            print(f'!! Clock update period: {update_period} min', flush=True)
            conf_dict['template']['clock']['tick_seconds'] = update_period * 60

            run_times = Parallel(jobs)(
                delayed(stress_test)(i + 1, test_runs, conf_dict)
                for i in range(test_runs))

            run_times = np.array(run_times)
            mins, secs = divmod(run_times.mean(), 60)
            avg_time = f'{mins:.0f} min {secs:.0f} ∓ {run_times.var():.2f} sec'
            print(f'--- Average runtime: {avg_time}', flush=True)

            results.append({
                'update_period': update_period,
                'avg_time': avg_time,
                'test_runs': test_runs,
                'run_days': days,
            })

    except KeyboardInterrupt:
        pass
    finally:

        if results:
            print(f'Write results to {results_file}')

            with open(results_file, 'w') as f:
                f.write(json.dumps(results, indent=2))


if __name__ == '__main__':
    start_tests()
