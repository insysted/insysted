from unittest import mock

import pytest

from core.plant import Plant


@pytest.fixture
def plant():
    plant = mock.MagicMock(spec=Plant)

    plant.id_ = 'Plant'
    plant.simulation.clock.day = 123
    plant.simulation.clock._ticks = 1234

    return plant
