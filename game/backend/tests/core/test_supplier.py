from unittest import mock

import pytest

from core.order import Order, OrderLot
from core.supplier import Supplier
from .test_order import order


@pytest.fixture
def supplier(plant):
    kwargs = {
        'plant': plant,
        'contracts': {
            'default': {
                'lead_time': 3,
                'lead_time_max': 5,
                'batch_price': 100,
                'fix_price': 50,
                'batch_size': 10,
            },
        },
        'contract_active': 'default',
        'lot_size': 1,
        'delivery_time_type': 'normal',
        'delivery_time_constant': 3,
        'delivery_time_normal_avg': 3,
        'delivery_time_normal_sigma': 1,
        'warehouse': 'Warehouse #1',
    }

    return Supplier(**kwargs)


@mock.patch('numpy.random.Generator')
def test_supplier_receive_order(Generator, supplier):
    normalRnt = mock.MagicMock()
    normalRnt.normal = mock.MagicMock(return_value=1)
    Generator.return_value = normalRnt

    clock_ticks = 10
    supplier._plant.simulation.clock.to_ticks_from_hours.return_value = clock_ticks
    supplier._plant.simulation.clock._ticks = 123

    order = mock.MagicMock(spec=Order)
    supplier.get_order = mock.Mock()
    supplier.get_order.side_effect = [order, None]

    assert len(supplier._orders) == 0
    assert len(supplier._proc_ticks) == 0

    assert supplier._plant.simulation.clock._ticks == 123
    supplier.update_state()
    assert supplier._plant.simulation.clock._ticks == 123

    assert len(supplier._orders) == 1
    assert supplier._orders[0] == order
    assert len(supplier._proc_ticks) == 1
    assert supplier._proc_ticks[0] == 123 + clock_ticks

    supplier.get_order.assert_called()
    order.accept.assert_called()

    supplier.rng.normal.assert_called_with(
        supplier.delivery_time_normal_avg,
        supplier.delivery_time_normal_sigma)


def test_supplier_send_finished_order(supplier, order):
    order.lots = [mock.MagicMock(spec=OrderLot) for _ in range(10)]

    clock_ticks = 10
    supplier._orders = list([order])
    supplier._proc_ticks = list([clock_ticks])
    supplier._plant.simulation.clock._ticks = 10

    supplier.update_state()

    for lot in order.lots:
        lot.finish.assert_called()

    order.customer.finished_orders.put.assert_called_with(order)


def test_supplier_send_finished_order_parallel(supplier):

    supplier._plant.simulation.clock._ticks = 0

    orderCount = 10

    supplier._orders = list(mock.MagicMock(spec=Order)
                            for _ in range(orderCount))
    for order in supplier._orders:
        order.lots = [mock.MagicMock(spec=OrderLot) for _ in range(10)]
        order.customer = mock.MagicMock()

    supplier._proc_ticks = list(i + 1 for i in range(orderCount))

    for tick in range(orderCount):
        expectedOrders = orderCount - tick
        assert len(supplier._orders) == expectedOrders
        assert len(supplier._proc_ticks) == expectedOrders
        supplier._plant.simulation.clock._ticks += 1
        supplier.update_state()


def test_supplier_correct_time(supplier):
    supplier._plant.simulation.clock._ticks = 0

    supplier._orders = list([mock.MagicMock(spec=Order)])
    for order in supplier._orders:
        order.lots = [mock.MagicMock(spec=OrderLot) for _ in range(10)]
        order.customer = mock.MagicMock()
    supplier._proc_ticks = list([12])

    for _ in range(12):
        supplier.update_state()
    assert len(supplier._orders) == 1

    supplier._plant.simulation.clock._ticks = 11
    supplier.update_state()
    assert len(supplier._orders) == 1

    supplier._plant.simulation.clock._ticks = 12
    supplier.update_state()
    assert len(supplier._orders) == 0
