import pytest

from core.queue import Queue, QueueEmptyError, QueuePolicy, QUEUE_POLICIES


def test_queue_policy_error():
    q = Queue()

    for p in (QueuePolicy.FIFO, QueuePolicy.LIFO):
        assert p in QUEUE_POLICIES
        q.policy = p

    with pytest.raises(ValueError):
        q.policy = 'foo'


def test_queue_policies():
    q = Queue()

    for i in range(10):
        q.put(i)

    q.policy = 'fifo'

    assert q.pop() == 0
    assert q.pop() == 1
    assert q.pop() == 2

    q.policy = 'lifo'

    assert q.pop() == 9
    assert q.pop() == 8
    assert q.pop() == 7


def test_queue_size():
    q = Queue()

    assert len(q) == 0
    assert q.empty

    with pytest.raises(QueueEmptyError):
        q.pop()

    items = 10

    for i in range(items):
        q.put(i)

    assert len(q) == items
    assert not q.empty


@pytest.mark.parametrize('policy', ('fifo', 'lifo'))
def test_queue_put_back(policy):
    q = Queue(policy)

    for i in range(10):
        q.put(i)

    i = q.pop()
    q.put(i, back=True)

    assert i == q.pop()
