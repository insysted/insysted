from unittest import mock

import pytest

from core.base.exceptions import PlantWarehouseError
from core.matcher import InventoryMatcher
from core.warehouse import Warehouse
from .test_order import order_lot


@pytest.fixture
def warehouse():
    warehouse = mock.MagicMock(spec=Warehouse)
    warehouse.id_ = 'warehouse'

    return warehouse


@pytest.fixture
def matcher(plant, warehouse):
    plant.get_unit.return_value = warehouse

    return InventoryMatcher(plant, warehouse.id_)


def test_matcher_get_warehouse(matcher, warehouse):
    assert matcher._warehouse is warehouse
    matcher._plant.get_unit.assert_called_with(warehouse.id_)


def test_matcher_withdraw_inventory_success(matcher, order_lot):
    matcher.queue.put(order_lot)

    assert len(matcher.queue) == 1

    matcher.update_state()

    matcher._warehouse.withdraw_inventory.assert_called_with(order_lot.size)
    matcher._plant.pipeline_transfer.assert_called_with(order_lot)
    assert len(matcher.queue) == 0


def test_matcher_withdraw_inventory_failure(matcher, order_lot):
    matcher.queue.put(order_lot)
    matcher._warehouse.withdraw_inventory.side_effect = PlantWarehouseError

    assert len(matcher.queue) == 1

    matcher.update_state()

    assert len(matcher.queue) == 1


@pytest.mark.parametrize(
    'num_lots',
    [
        (1),
        (2),
        (42),
    ])
def test_matcher_calculate_needed_inventory(matcher, order_lot, num_lots):
    for _ in range(num_lots):
        matcher.queue.put(order_lot)

    assert matcher.calculate_needed_inventory() == order_lot.size * num_lots
    assert len(matcher.queue) == num_lots


def test_matcher_charges_for_shortage(matcher, order_lot):
    matcher.shortage_cost = 1234

    matcher.queue.put(order_lot)
    matcher._warehouse.withdraw_inventory.side_effect = PlantWarehouseError

    matcher.update_state()

    matcher._plant.pay_costs.assert_called_with(
        matcher.shortage_cost, "shortage", credit=True)


def test_matcher_dose_not_charges_for_shortage(matcher, order_lot):
    matcher.shortage_cost = 1234

    matcher.queue.put(order_lot)

    matcher.update_state()

    matcher._plant.pay_costs.assert_not_called()


def test_matcher_dose_not_charge_for_shortage_twice(matcher, order_lot):
    matcher.shortage_cost = 1234

    matcher.queue.put(order_lot)
    matcher._warehouse.withdraw_inventory.side_effect = PlantWarehouseError
    matcher.update_state()
    matcher.update_state()

    matcher._plant.pay_costs.assert_called_once()
