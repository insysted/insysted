from unittest import mock

import pytest

from core.customer import Customer
from core.office import Office
from core.order import Order
from core.queue import QueueEmptyError


@pytest.fixture
def office(plant):
    params = {
        'plant': plant,
        'contracts': {
            'default': {
                'lead_time': 3,
                'lead_time_max': 5,
                'batch_price': 100,
                'fix_price': 50,
                'batch_size': 10,
            },
        },
        'contract_active': 'default',
        'lot_size': 1,
    }

    return Office(**params)


def test_set_lot_size_success(office):
    size = 10
    assert office.lot_size != size

    office.lot_size = size

    assert office.lot_size == size


@pytest.mark.parametrize('size', [-1, 0])
def test_set_lot_size_failure(size, office):
    assert office.lot_size != size

    with pytest.raises(ValueError):
        office.lot_size = size


def test_set_contract_success(office):
    contract = 'default'

    assert contract in office._contracts

    office.contract = contract

    assert office._contract_active == contract


def test_set_contract_failure(office):
    contract = 'contract does not exist'

    assert contract not in office._contracts

    with pytest.raises(ValueError):
        office.contract = contract


def test_get_order(office):
    assert office.queue.empty

    with pytest.raises(QueueEmptyError):
        office.queue.pop()

    order = mock.MagicMock(spec=Order)
    office.queue.put(order)

    assert office.get_order() is order
    assert office.queue.empty
    assert office.get_order() is None


def test_put_order(office):
    assert office.queue.empty

    customer = mock.MagicMock(spec=Customer)
    office.put_order(customer, number_of_orders=2)

    assert len(office.queue) == 2

    order = office.get_order()

    assert order.customer is customer

    order2 = office.get_order()
    assert order.fix_price != order2.fix_price
    assert order2.fix_price == 0
