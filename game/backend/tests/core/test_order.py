import math
from unittest import mock

import pytest

from core.customer import Customer
from core.order import Order, OrderLot
from core.queue import Queue


@pytest.fixture
def order():
    customer = mock.MagicMock(spec=Customer)
    customer.id_ = "customer"
    customer.finished_orders = mock.MagicMock(spec=Queue)
    params = {
        'customer': customer,
        'batch_size': 1,
        'batch_price': 1,
        'fix_price': 1,
        'lead_time': 2,
        'lead_time_max': 4,
        'lot_size': 1,
    }

    order = Order(**params)
    order.to_dict = mock.MagicMock(return_value={
                                   "customer": "customer", "revenue": 1, "lead_time": 1, "lead_time_max": 2})
    return order


@pytest.fixture
def order_lot():
    lot = mock.MagicMock(spec=OrderLot)
    lot.size = 10

    return lot


def test_order_lot_is_finished():
    order = mock.MagicMock(spec=Order)
    lot = OrderLot(order, size=10, pipeline_id=None)

    assert not lot.is_finished

    for _ in range(3):
        lot.finish()

    assert lot.is_finished
    lot.order.update_state.assert_called()
    lot.order.update_state.call_count == 1


def test_order_accept(order, plant):
    assert order.plant is None
    assert not order.is_finished
    assert not order.lots
    assert order.revenue is None

    order.accept(plant, pipeline_id=None)

    assert order.plant is plant
    assert order._accepted_at == plant.simulation.clock._ticks

    lots_expected = math.ceil(order.batch_size / order.lot_size)

    assert len(order.lots) == lots_expected


def test_order_revenue(order, plant):
    order.accept(plant, pipeline_id=None)

    order._plant.simulation.clock.to_ticks_from_hours.return_value = 1

    for lot in order.lots:
        lot.finish()

    assert order.is_finished
    assert order._finished_at - order._accepted_at <= order.lead_time

    rev_expected = order.batch_price + order.fix_price

    assert math.isclose(order.revenue, rev_expected)


def test_order_revenue_lead_time_excess(order, plant):
    order.accept(plant, pipeline_id=None)
    plant.simulation.clock._ticks = order._accepted_at + order.lead_time_max - 1
    plant.simulation.clock.to_ticks_from_hours.side_effect = [
        order.lead_time, order.lead_time_max]

    for lot in order.lots:
        lot.finish()

    assert order.is_finished
    assert order._finished_at - order._accepted_at > order.lead_time
    assert order._finished_at - order._accepted_at < order.lead_time_max

    rev_expected = order.batch_price + order.fix_price

    assert 0 < order.revenue < rev_expected


def test_order_revenue_lead_time_max_excess(order, plant):
    order.accept(plant, pipeline_id=None)
    plant.simulation.clock._ticks = order._accepted_at + order.lead_time_max
    plant.simulation.clock.to_ticks_from_hours.side_effect = [
        order.lead_time, order.lead_time_max]

    for lot in order.lots:
        lot.finish()

    assert order.is_finished
    assert order._finished_at - order._accepted_at > order.lead_time
    assert not order.revenue
