from unittest import mock

import pytest

from core.customer import Customer, OrderAmmountBehavior, OrderInterarrivalTimeBehavior
from core.office import Office
from core.order import Order


@pytest.fixture
def customer(plant):
    office = mock.MagicMock(spec=Office)
    office.id_ = 'Sales Office'
    plant.get_unit.return_value = office

    return Customer(
        plant,
        office,


        order_interarrival_time_normal_avg=1,
        order_interarrival_time_normal_sigma=2,
        order_interarrival_time_constant=3,
        order_interarrival_time_exponential_lambda=4,
        order_ammout_type=OrderAmmountBehavior.Constant,

        order_ammount_normal_avg=5,
        order_ammount_normal_sigma=6,
        order_ammount_constant=7,
        order_interarrival_time_type=OrderInterarrivalTimeBehavior.Constant,)


def test_customer_get_time_to_next_order_constant(customer):
    customer.order_interarrival_time_type = OrderInterarrivalTimeBehavior.Constant
    customer.order_interarrival_time_constant = 1234

    assert customer.get_time_to_next_order() == 1234


@mock.patch('numpy.random.Generator')
def test_customer_get_time_to_next_order_normal(Generator, customer):
    customer.order_interarrival_time_type = OrderInterarrivalTimeBehavior.Normal

    normalRnt = mock.MagicMock()
    normalRnt.normal = mock.MagicMock(return_value=42)
    Generator.return_value = normalRnt

    assert customer.get_time_to_next_order() == 42
    normalRnt.normal.assert_called_with(
        customer.order_interarrival_time_normal_avg, customer.order_interarrival_time_normal_sigma)


@mock.patch('numpy.random.Generator')
def test_customer_get_time_to_next_order_exponential(Generator, customer):
    customer.order_interarrival_time_type = OrderInterarrivalTimeBehavior.Exponential

    exponentialRnt = mock.MagicMock()
    exponentialRnt.exponential = mock.MagicMock(return_value=42)
    Generator.return_value = exponentialRnt

    assert customer.get_time_to_next_order() == 42
    exponentialRnt.exponential.assert_called_with(
        customer.order_interarrival_time_exponential_lambda)


def test_customer_get_order_ammount_constant(customer):
    customer.order_ammout_type = OrderAmmountBehavior.Constant
    customer.order_ammount_constant = 1234

    assert customer.get_order_ammount() == 1234


@mock.patch('numpy.random.Generator')
def test_customer_get_order_ammount_normal(Generator, customer):
    customer.order_ammout_type = OrderAmmountBehavior.Normal

    normalRnt = mock.MagicMock()
    normalRnt.normal = mock.MagicMock(return_value=42)
    Generator.return_value = normalRnt

    assert customer.get_order_ammount() == 42
    normalRnt.normal.assert_called_with(
        customer.order_ammount_normal_avg, customer.order_ammount_normal_sigma)


def test_customer_receive_orders(customer):
    customer._plant.simulation.new_day = False
    revenue = 123
    orders = []

    for _ in range(10):
        order = mock.MagicMock(spec=Order)
        order.revenue = revenue
        customer.finished_orders.put(order)
        orders.append(order)

    customer.update_state()

    for order in orders:
        order.plant.receive_payment.assert_called_with(order.revenue)
        assert order.plant.receive_payment.call_count == 1


def test_customer_put_orders(customer):
    clock_ticks = 10
    customer._plant.simulation.clock.to_ticks_from_minutes.return_value = clock_ticks
    customer._plant.simulation.clock._ticks = 123

    # set the time such that we expect an order in clock_ticks
    customer.next_order_time = customer._plant.simulation.clock._ticks + clock_ticks

    customer.update_state()
    customer.update_state()
    customer.office.put_order.assert_not_called()

    # let some time pass
    customer._plant.simulation.clock._ticks = customer._plant.simulation.clock._ticks + clock_ticks

    customer.update_state()
    customer.office.put_order.assert_called()

    assert customer.next_order_time == customer._plant.simulation.clock._ticks + clock_ticks


def test_customer_supply_chain_skip_update(customer):
    customer.supply_chain = [{
        'plant': 'Factory',
        'warehouse': 'Warehouse',
    }]
    customer.next_order_time = None  # this would normally result in a order

    customer.update_state()

    customer.office.put_order.assert_not_called()
