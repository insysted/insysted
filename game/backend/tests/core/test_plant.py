from core.workshop import MachineStatus
from core.plant import Plant
from core.simulation import Simulation

import pytest


def test_update(conf_dict):
    sim = Simulation.from_dict(conf_dict)

    plant = Plant.from_dict({
        'id_': 1,
        'simulation': sim,
        **conf_dict['template']['plant'],
    })

    assert plant._units['Workshop #1'].machines[0].status == MachineStatus.READY

    plant.update_state()

    assert plant._units['Workshop #1'].machines[0].status == MachineStatus.BUSY
