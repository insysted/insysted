from unittest import mock
import weakref

import pytest

from core.base.exceptions import PlantPaymentError, PlantWarehouseError
from core.order import Order
from core.plant import Plant
from core.queue import Queue
from core.warehouse import Warehouse
from core.matcher import InventoryMatcher
from .test_supplier import supplier, order


@pytest.fixture
def warehouse(plant, supplier):

    kwargs = {
        'plant': plant,
        'inventory': 5000,
        'reorder_point': 10,
        'order_size': 5,
        'unit_storage_cost': 0.1,
        'supplier': supplier.id_
    }
    wh = Warehouse(**kwargs)

    wh.supplier._contract_terms = dict()
    wh.supplier._contract_terms['batch_size'] = 10

    return wh


@pytest.fixture
def matcher(plant, warehouse):
    plant.get_unit.return_value = warehouse

    return InventoryMatcher(plant, warehouse.id_)


def test_warehouse_set_office_params_success(warehouse):
    params = {
        'reorder_point': 1000,
        'order_size': 5000,
    }

    for p, v in params.items():
        setattr(warehouse, p, v)


@pytest.mark.parametrize(
    'params',
    [
        {'reorder_point': 1000, 'order_size': 0},
        {'reorder_point': -1, 'order_size': 1000},
    ])
def test_warehouse_set_office_params_failure(params, warehouse):

    with pytest.raises(ValueError):

        for p, v in params.items():
            setattr(warehouse, p, v)


def test_warehouse_withdraw_inventory_success(warehouse):
    quantity = warehouse._inventory
    warehouse.withdraw_inventory(quantity)
    assert warehouse._inventory == 0


def test_warehouse_withdraw_inventory_failure(warehouse):
    quantity = warehouse._inventory + 1

    with pytest.raises(PlantWarehouseError):
        warehouse.withdraw_inventory(quantity)


def test_warehouse_receice_finished_orders_success(warehouse, order):
    warehouse.finished_orders.put(order)
    expected_inventory = warehouse._inventory + order.batch_size

    assert len(warehouse.finished_orders) == 1

    warehouse.update_state()

    assert warehouse.finished_orders.empty
    assert warehouse._inventory == expected_inventory
    warehouse._plant.pay_order.assert_called_with(order)


def test_warehouse_receice_finished_orders_failure(warehouse, order):
    warehouse.finished_orders.put(order)
    warehouse._plant.pay_order.side_effect = PlantPaymentError
    old_inventory = warehouse._inventory

    assert len(warehouse.finished_orders) == 1

    warehouse.update_state()

    assert len(warehouse.finished_orders) == 1
    assert warehouse._inventory == old_inventory


def test_warehouse_storage_costs_billed(warehouse):
    warehouse._plant.simulation.clock.new_day = True
    costs = warehouse._inventory * warehouse._unit_storage_cost
    warehouse.update_state()
    warehouse._plant.pay_costs.assert_called_with(
        costs, "warehouse storage", credit=True)


def test_warehouse_storage_costs_not_billed(warehouse):
    warehouse._plant.simulation.clock.new_day = False
    warehouse.update_state()
    warehouse._plant.pay_costs.assert_not_called()


def test_warehouse_reorder_inventory_success(order, warehouse):
    warehouse.supplier.put_order = mock.MagicMock()
    warehouse._inventory = 0
    warehouse._orders = 0

    warehouse.update_state()

    warehouse.supplier.put_order.assert_called_with(
        customer=warehouse, number_of_orders=warehouse._order_size)


@pytest.mark.parametrize(
    'inventory, finishedOrders, ongoingOrders',
    [
        (11, 0, 0),
        (0, 11, 0),
        (0, 0, 11)
    ])
def test_warehouse_reorder_inventory_failure(inventory, finishedOrders, ongoingOrders, warehouse):
    warehouse._reorder_point = 10
    warehouse._plant.pay_order.side_effect = PlantPaymentError

    warehouse._inventory = inventory

    q = Queue()
    for _ in range(finishedOrders):
        orderMock = mock.MagicMock(spec=Order)
        orderMock.batch_size = 1
        q.put(orderMock)
    warehouse.finished_orders = q

    warehouse._orders = ongoingOrders

    warehouse.supplier.put_order = mock.MagicMock()

    # make sure that no reordering happens
    warehouse.update_state()

    warehouse.supplier.put_order.assert_not_called()


def test_warehouse_supply_chain_get_office(warehouse):
    warehouse.office.supply_chain = {
        'plant': 'Factory X',
        'office': 'Sales Office',
    }
    plant = mock.MagicMock(spec=Plant)
    supp_unit = mock.MagicMock()
    plant.get_unit.return_value = supp_unit
    warehouse._plant.simulation.get_plant.return_value = plant

    assert warehouse.supplier == supp_unit

    warehouse._plant.simulation.get_plant.assert_called_with('Factory X')
    plant.get_unit.assert_called_with('Sales Office')
