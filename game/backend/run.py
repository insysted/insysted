#!/usr/bin/env python
import sys

import click
import Pyro5.nameserver

from core.utils import logger


log = logger.configure()


@click.group()
def manage():
    """Manage the game server.
    """


@manage.command()
@click.option('-h', '--host', default='localhost')
@click.option('-p', '--port', type=int, default=0)
def ns(host, port):
    """Start the Pyro5 name server.
    """
    log.info('Start the name server...')

    try:
        Pyro5.nameserver.start_ns_loop(host, port)
    except Exception as e:
        log.error(e)
        sys.exit(1)


if __name__ == '__main__':
    manage()
