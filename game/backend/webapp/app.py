from psycopg2cffi import compat

from .app_factory import create_app
from .middleware import db
from .worker.app import make_celery


compat.register()


app = create_app(__name__)
celery = make_celery(app)


@app.before_first_request
def create_db_tables():
    db.create_all()


@app.teardown_request
def remove_db_session(exc=None):
    db.session.remove()


application = app
