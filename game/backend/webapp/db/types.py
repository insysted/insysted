import json

from sqlalchemy.ext.mutable import MutableDict

from webapp.middleware import db


class JsonEncodedDict(db.TypeDecorator):
    impl = db.UnicodeText

    def process_bind_param(self, value, dialect):

        if value is not None:
            return json.dumps(value)

    def process_result_value(self, value, dialect):

        if value is not None:
            return json.loads(value)


MutableDict.associate_with(JsonEncodedDict)
