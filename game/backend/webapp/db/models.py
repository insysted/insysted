import uuid
from enum import Enum

from flask import current_app
from sqlalchemy.sql import expression
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship, synonym, backref
from werkzeug.security import generate_password_hash, check_password_hash

from webapp.middleware import db
from .types import JsonEncodedDict
from .mixins import TimestampsMixin


class Role(Enum):
    ADMIN = 0
    DESIGNER = 1
    PLAYER = 2


class UserRole(db.Model):
    __tablename__ = 'user_role'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Enum(Role), nullable=False, unique=True)

    def __str__(self):
        return self.name.name

    def __repr__(self):
        return f'<UserRole {self.name.name}>'


user_role_table = db.Table('user_role_association', db.Model.metadata,
                           db.Column('role_id', db.Integer,
                                     db.ForeignKey('user_role.id')),
                           db.Column('user_id', db.Integer,
                                     db.ForeignKey('user.id')),
                           )


class User(db.Model, TimestampsMixin):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(256), nullable=False)
    email = db.Column(db.String(128), nullable=False, unique=True)
    is_confirmed = db.Column(
        db.Boolean, server_default=expression.false(), nullable=False)
    games = relationship('Game', backref='owner', cascade="all, delete-orphan")
    templates = relationship(
        'GameTemplate', backref='designer', cascade="all, delete-orphan")
    actions = relationship('GameAction', backref='user',
                           cascade="all, delete-orphan")
    login_at = db.Column(db.DateTime)
    password_hash = db.Column(db.String(128), nullable=False)
    lti_user_id = db.Column(db.String(128), unique=True)
    keep = db.Column(db.Boolean, default=False)
    roles = relationship(
        'UserRole', secondary=user_role_table, backref='users', cascade="all, delete")
    identity = synonym('email')

    @property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, val):
        self.password_hash = generate_password_hash(val)

    def validate_password(self, password):
        return check_password_hash(self.password_hash, password)

    def grant_role(self, name):
        role = UserRole.query.filter_by(name=name).first()

        if role and role in self.roles:
            return

        if not role:
            role = UserRole(name=name)
            db.session.add(role)
            db.session.commit()

        self.roles.append(role)
        current_app.logger.info(f'Grant role {role} to {self}')

    def revoke_role(self, name):
        role = UserRole.query.filter_by(name=name).first()

        if not role or role not in self.roles:
            return

        self.roles.remove(role)
        current_app.logger.info(f'Revoke role {role} from {self}')

    def has_role(self, name):
        role = UserRole.query.filter_by(name=name).first()

        return role in self.roles


class GameAction(db.Model):
    __tablename__ = 'game_action'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    team_id = db.Column(UUID(as_uuid=True), db.ForeignKey('team.id'))
    meta = db.Column(JsonEncodedDict, nullable=False)
    created_at = db.Column(db.Unicode(256))


user_team_table = db.Table('user_team_association', db.Model.metadata,
                           db.Column('team_id', UUID(as_uuid=True),
                                     db.ForeignKey('team.id')),
                           db.Column('user_id', db.Integer,
                                     db.ForeignKey('user.id')),
                           )


class Team(db.Model, TimestampsMixin):
    __tablename__ = 'team'

    id = db.Column(UUID(as_uuid=True), primary_key=True,
                   default=uuid.uuid4, unique=True)
    name = db.Column(db.Unicode(256), nullable=False)
    plant = db.Column(JsonEncodedDict)
    actions = relationship('GameAction', order_by='GameAction.id.desc()',
                           backref='team', cascade='all,delete,delete-orphan')
    users = relationship('User', secondary=user_team_table, backref='team')
    game_id = db.Column(db.Integer, db.ForeignKey('game.id'))
    bracket = db.Column(db.Unicode(256), nullable=False)
    team_progress = db.Column(JsonEncodedDict)

    @property
    def cash(self):
        return self.plant['cash'] if self.plant else None

    @property
    def score(self):

        if self.plant and self.plant.get('stats'):
            return self.plant['stats'][-1]['score']
        else:
            return 0

    @property
    def plant_id(self):
        return self.name


class GameStatus(Enum):
    INITIALIZED = 0
    SUSPENDED = 1
    STARTED = 2
    ABORTED = 3
    DONE = 4
    ASYNC = 5


class BracketingBehavior(Enum):
    HashBased = 0
    RoundRobin = 1
    MoodleApi = 2
    LtiParameter = 3


def bracketingBehavior_from_str(str):
    if str == 'HashBased':
        return BracketingBehavior.HashBased
    elif str == 'RoundRobin':
        return BracketingBehavior.RoundRobin
    elif str == 'MoodleApi':
        return BracketingBehavior.MoodleApi
    elif str == 'LtiParameter':
        return BracketingBehavior.LtiParameter


class Game(db.Model, TimestampsMixin):
    __tablename__ = 'game'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(256), nullable=False, index=True)
    leaderboard = db.Column(db.Boolean)
    description = db.Column(db.UnicodeText)
    simulation = db.Column(JsonEncodedDict, nullable=False)
    layout = db.Column(JsonEncodedDict)
    status = db.Column(db.Enum(GameStatus), nullable=False,
                       default=GameStatus.INITIALIZED)
    progress = db.Column(JsonEncodedDict)
    teams = relationship('Team', backref='game',
                         cascade='all,delete,delete-orphan')
    start_at = db.Column(db.DateTime)
    stopped_at = db.Column(db.DateTime)
    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    template_id = db.Column(db.Integer, db.ForeignKey('game_template.id'))
    bracketing_behavior = db.Column(db.Enum(
        BracketingBehavior), nullable=False, default=BracketingBehavior.RoundRobin)
    number_of_brackets = db.Column(db.Integer, default=3)


class GameTemplate(db.Model, TimestampsMixin):
    __tablename__ = 'game_template'

    id = db.Column(db.Integer, primary_key=True)
    leaderboard = db.Column(db.Boolean)
    title = db.Column(db.Unicode(256), nullable=False, index=True)
    description = db.Column(db.UnicodeText)
    simulation = db.Column(JsonEncodedDict, nullable=False)
    layout = db.Column(JsonEncodedDict)
    designer_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    is_public = db.Column(
        db.Boolean, server_default=expression.false(), nullable=False)
    games = relationship('Game', backref='template')
