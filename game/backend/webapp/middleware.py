import logging

from flask_caching import Cache
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask_session import Session

from core.utils import logger as core_logger


db = SQLAlchemy()
migrate = Migrate()
cache = Cache()
jwt = JWTManager()
sess = Session()


def init_middleware(app):
    db.init_app(app)
    migrate.init_app(app, db)
    cache.init_app(app)
    jwt.init_app(app)
    sess.init_app(app)
    logging.basicConfig(level=app.logger.level)
    core_logger.configure(app.logger.level)
