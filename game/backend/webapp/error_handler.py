from flask import jsonify, current_app, request


def handle_error(err):
    current_app.logger.error(f'{request.url} {err.code} {err.name}')
    response = {'success': False, 'url': request.url}
    webarg_error = getattr(err, 'data', None)

    if webarg_error is None:
        response['message'] = err.description
    else:
        response.update({
            'message': 'Invalid request arguments',
            'errors': webarg_error['messages'],
        })

    return jsonify(response), err.code
