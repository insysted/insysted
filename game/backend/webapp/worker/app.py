import logging

from celery import Celery, signals

from core.base.config import LOGGER_NAME


celery = Celery(__name__, include=['webapp.worker.tasks'])


def make_celery(app):
    celery.conf.update(
        broker_url=app.config['CELERY_BROKER_URL'],
        result_backend=app.config['CELERY_RESULT_BACKEND_URL'],
        result_expires=app.config['CELERY_RESULT_EXPIRES'],
        task_routes=app.config['CELERY_TASK_ROUTES'],
        **app.config,
    )

    def call_with_app_context(self, *args, **kwargs):
        self.flask_app = app

        with app.app_context():
            return self.run(*args, **kwargs)

    celery.Task.__call__ = call_with_app_context

    return celery


@signals.after_setup_logger.connect
def on_after_setup_logger(**kwargs):
    logging.getLogger(LOGGER_NAME).handlers = []
