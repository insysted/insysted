from email.header import Header
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.utils import formataddr, formatdate, COMMASPACE
from smtplib import SMTP

from flask import render_template, current_app

from webapp.config.api import USER_ID_TOKEN_LIFE_TIME
from .time import human_readable


class Email:

    def __init__(self, recipients):
        self._recipients = recipients
        self._username = current_app.config['EMAIL_CONFIG']['username']
        self._password = current_app.config['EMAIL_CONFIG']['password']
        self._smtp_host = current_app.config['EMAIL_CONFIG']['smtp']['host']
        self._smtp_port = current_app.config['EMAIL_CONFIG']['smtp']['port']
        self._debug = current_app.config['DEBUG']

    @property
    def _subject(self):
        raise NotImplementedError

    @property
    def _body(self):
        raise NotImplementedError

    def send(self):
        msg = MIMEMultipart()

        msg['From'] = formataddr(('INSYSTED Project Team', self._username))
        msg['To'] = COMMASPACE.join(self._recipients)
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = Header(self._subject, charset='utf-8')

        text = MIMEText(self._body, 'plain', _charset='utf-8')
        msg.attach(text)

        with SMTP(self._smtp_host, self._smtp_port) as smtp:
            smtp.ehlo()

            if not self._debug:
                smtp.starttls()

            smtp.login(self._username, self._password)
            smtp.send_message(msg)


class AccountConfirmationEmail(Email):

    def __init__(self, email, url):
        super().__init__(recipients=(email,))
        self._url = url

    @property
    def _subject(self):
        return 'Please confirm the registration'

    @property
    def _body(self):
        return render_template(
            'email/confirmation.txt',
            url=self._url,
            valid_time=human_readable(USER_ID_TOKEN_LIFE_TIME),
        )
