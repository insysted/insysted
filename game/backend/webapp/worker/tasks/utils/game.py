from sqlalchemy.orm import joinedload

from webapp.db.models import Game
from webapp.middleware import db


def make_update_callback(app, game_id):

    def callback(update):
        with app.app_context():

            game = Game.query.options(
                joinedload(Game.teams)
            ).filter_by(id=game_id).one()

            plants = update['state'].pop('plants')

            for team in game.teams:
                if team.plant_id in plants:
                    team.plant = plants[team.plant_id]
                    team.team_progress = update['progress']

            game.simulation = update['state']
            game.progress = update['progress']

            db.session.commit()

    return callback
