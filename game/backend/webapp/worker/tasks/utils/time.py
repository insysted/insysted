TIME_DURATION_UNITS = (
    ('week', 60 * 60 * 24 * 7),
    ('day', 60 * 60 * 24),
    ('hour', 60 * 60),
    ('min', 60),
    ('sec', 1)
)


def human_readable(seconds):

    if seconds < 1:
        raise ValueError(seconds)

    parts = []

    for unit, div in TIME_DURATION_UNITS:
        amount, seconds = divmod(int(seconds), div)

        if not amount:
            continue

        part = f'{amount} {unit}'
        parts.append(part + 's' if amount > 1 else part)

    return ' '.join(parts)
