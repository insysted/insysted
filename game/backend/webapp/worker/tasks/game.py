import os
import gc
from datetime import datetime

from sqlalchemy.sql.expression import true

from celery.utils.log import get_task_logger
from celery import group
from sqlalchemy.orm import joinedload

from core.base.exceptions import AbortSimulation
from core.simulation import Simulation
from webapp.blueprints.api.utils.teams import GameRunningException
from pyro.server import SimulationServer
from webapp.db.models import Game, GameStatus
from webapp.middleware import db
from webapp.worker.app import celery
from .base.trackable import TrackableTask
from .utils.game import make_update_callback


logger = get_task_logger(__name__)

game_task_tracker = {}


def run_game_server(game_id):
    game = Game.query.options(joinedload(Game.teams)).filter(
        Game.id == game_id).one()

    logger.info(f'START {game}')

    game.status = GameStatus.STARTED
    db.session.commit()

    brackets = set()
    tasks = []
    for team in game.teams:
        if team.bracket not in brackets:
            tasks.append(run_bracket.s(game_id, team.bracket))
            brackets.add(team.bracket)

    # run the tasks
    game_task_tracker[int(game_id)] = group(
        tasks).apply_async(args=(), eta=game.start_at)


def revoke_game(game_id):
    if int(game_id) in game_task_tracker:
        for result in game_task_tracker[int(game_id)].results:
            result.revoke(terminate=True)
        game_task_tracker[int(game_id)].revoke(terminate=True)


async_games = {}


def get_async_game_key(game_id, bracket):
    return f'{game_id}-{bracket}'


def is_async_game_running(game_id, bracket):
    return get_async_game_key(game_id, bracket) in async_games


def stop_async_game(game_id, team):
    logger.warning(f'Stoping asnyc game {game_id} team {team.name}.')
    if is_async_game_running(game_id, team.bracket):
        key = get_async_game_key(game_id, team.bracket)
        async_games[key].revoke(terminate=True)
        async_games.pop(key)


def reset_async_game(game_id, team):
    logger.warning(f'Reseting asnyc game {game_id} team {team.name}.')
    if is_async_game_running(game_id, team.bracket):
        stop_async_game(game_id, team)

    team.plant = {}
    team.team_progress = {}

    db.session.commit()


def start_async_game(game_id, team):
    logger.warning(f'Starting asnyc game {game_id} team {team.name}.')
    async_games[get_async_game_key(game_id, team.bracket)] = run_bracket.s(
        game_id, team.bracket).apply_async()


@celery.task(bind=True, acks_late=True)
def run_bracket(self, game_id, bracket):
    logger.warning(f'Started {game_id} bracket {bracket}')
    game = Game.query.options(joinedload(Game.teams)).filter(
        Game.id == game_id).one()

    try:
        sim = Simulation.from_dict(game.simulation)
        sim._game_id = game_id
        sim._bracket = bracket
        sim.update_callback = make_update_callback(self.flask_app, game_id)

        for team in game.teams:
            if team.bracket == bracket:
                sim.add_plant(team.plant_id, team.plant)

        srv = SimulationServer(
            game_id,
            bracket,
            host=os.getenv('SERVER_DAEMON_HOST'),
            ns_host=os.getenv('PYRO_NS_HOST'),
            ns_port=int(os.getenv('PYRO_NS_PORT')),
        )

        srv.run(sim)

    except AbortSimulation:
        logger.warning(f'ABORT {game}-{bracket}')
        if game.status != GameStatus.ASYNC:
            game.status = GameStatus.ABORTED
            game.stopped_at = datetime.utcnow()
            db.session.commit()
    except Exception:
        logger.exception(f'ABORT {game}-{bracket}. Unexpected ERROR:')
        if game.status != GameStatus.ASYNC:
            game.status = GameStatus.ABORTED
            game.stopped_at = datetime.utcnow()
            db.session.commit()

    else:
        logger.info(f'FINISH {game}-{bracket}')
        if game.status != GameStatus.ASYNC:
            game.status = GameStatus.DONE
            game.stopped_at = datetime.utcnow()
            db.session.commit()

    # try to delete sim and server
    try:
        del sim
    except Exception:
        pass
    try:
        del srv
    except Exception:
        pass

    gc.collect()
