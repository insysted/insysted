from celery.utils.log import get_task_logger

from webapp.worker.app import celery
from .utils.email import AccountConfirmationEmail


logger = get_task_logger(__name__)


@celery.task(ignore_result=True)
def send_confirmation_email(email, url):
    logger.info(f'Send account confirmation to {email}')
    AccountConfirmationEmail(email, url).send()
