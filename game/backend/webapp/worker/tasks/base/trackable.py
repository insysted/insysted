from webapp.worker.app import celery
from webapp.middleware import cache
from .exceptions import TaskNotFound


class TrackableTask(celery.Task):

    TRACK_REVOKE_TIMEOUT = 300

    def apply_async(self, *args, **kwargs):
        run_id = kwargs.pop('run_id', None)

        if not run_id:
            raise ValueError('run_id is not set')

        res = super().apply_async(*args, **kwargs)
        cache.set(self._result_key(run_id), res.id, timeout=0)

        return res

    def result(self, run_id):
        return self._get_task_result(run_id).get()

    def status(self, run_id):
        return self._get_task_result(run_id).status

    def revoke(self, run_id, **kwargs):

        try:
            res = self._get_task_result(run_id)
        except TaskNotFound:
            return False

        res.revoke(**kwargs)

        cache.delete(self._result_key(run_id))
        cache.set(self._revoke_key(run_id), True, self.TRACK_REVOKE_TIMEOUT)

        return True

    def is_revoked(self, run_id):
        return cache.get(self._revoke_key(run_id)) or False

    def untrack(self, run_id):
        self.result(run_id).forget()
        cache.delete(self._result_key(run_id))

    def _get_task_result(self, run_id):
        res_id = cache.get(self._result_key(run_id))

        if res_id is None:
            raise TaskNotFound

        return celery.AsyncResult(res_id)

    def _key(self, run_id):
        return f'task:{self.__class__.__name__}:{run_id}'

    def _result_key(self, run_id):
        prefix = self._key(run_id)

        return f'{prefix}:result'

    def _revoke_key(self, run_id):
        prefix = self._key(run_id)

        return f'{prefix}:revoke'
