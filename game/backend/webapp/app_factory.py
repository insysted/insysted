from flask import Flask
from werkzeug.exceptions import HTTPException

from .app_cli import init_cli, celery_cli, user_cli
from .blueprints import auth, game, user
from .error_handler import handle_error
from .middleware import init_middleware


def create_app(name):
    app = Flask(name)

    env = app.config['ENV']
    app.config.from_object('webapp.config.base')
    app.config.from_object(f'webapp.config.{env}')

    for bp in (auth, game, user):
        app.register_blueprint(bp)

    app.register_error_handler(HTTPException, handle_error)

    for cli in (init_cli, celery_cli, user_cli):
        app.cli.add_command(cli)

    init_middleware(app)

    return app
