from .api.auth.controller import auth
from .api.game.controller import game
from .api.user.controller import user
