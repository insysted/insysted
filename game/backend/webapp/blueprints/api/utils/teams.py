from webapp.db.models import Game, Team, GameStatus, User, BracketingBehavior
from webapp.middleware import db
from moodle.moodleTeams import get_team_obj_using_moodle, get_teamname_using_moodle_or_default


import numpy as np
from sqlalchemy.exc import SQLAlchemyError

# brackets related
alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def num_to_bracket_name(num):
    if num == 0:
        return alpha[0]

    rtn = ""
    while num > 0:
        rtn = alpha[num % len(alpha)] + rtn
        num //= len(alpha)
    return rtn


def set_bracket_round_robin(team, game):
    # find the bracket with the lowest number of users
    lowest_bracket = alpha[0]
    lowest_bracket_count = 99999
    for i in range(game.number_of_brackets):
        bracket = num_to_bracket_name(i)
        count = Team.query.filter(
            Team.bracket == bracket,
            Team.game_id == game.id,
        ).count()

        if count < lowest_bracket_count:
            lowest_bracket = bracket
            lowest_bracket_count = count

    # add the team to this bracket
    team.bracket = lowest_bracket


def set_bracket_hash_based(team, game):
    # calculate a 'hash' from the teams name
    hash = sum(ord(letter) for letter in team.name)
    rng = np.random.Generator(np.random.MT19937(int(hash)))
    team.bracket = num_to_bracket_name(
        round(rng.uniform(0, game.number_of_brackets - 1)))


def set_bracket_parameter(team, user_data):
    if 'bracket' not in user_data or len(user_data['bracket']) <= 0:
        raise TeamCreateionException(
            f'Custom parameter bracket is not set to a valid value.')
    team.bracket = user_data['bracket']


def save_team_brackets_auto(team, game_id, user_data=None):
    game = Game.query.get(game_id)
    if game.bracketing_behavior == BracketingBehavior.HashBased:
        set_bracket_hash_based(team, game)
    elif game.bracketing_behavior == BracketingBehavior.RoundRobin:
        set_bracket_round_robin(team, game)
    elif game.bracketing_behavior == BracketingBehavior.LtiParameter:
        if user_data is None:
            raise TeamCreateionException(
                f'BracketingBehavior.LtiParameter requires user_data to be supplied.')
        set_bracket_parameter(team, user_data)
    else:
        raise TeamCreateionException(
            f'behavior {game.bracketing_behavior} Not implemented')

    save_team(team, game_id)

# team related


class TeamCreateionException(Exception):
    pass


class JoinTeamException(Exception):
    pass


class TeamExistsException(TeamCreateionException):
    pass


class GameRunningException(TeamCreateionException):
    pass


class GameNotFoundException(TeamCreateionException):
    pass


class GameNotOwnedException(TeamCreateionException):
    pass


class GameFinishedException(JoinTeamException):
    pass


def get_team(team, game_id):
    return Team.query.filter(
        Team.name == team.name,
        Team.game_id == game_id,
    ).one()


def team_exists(team, game_id):
    return not not Team.query.filter(
        Team.name == team.name,
        Team.game_id == game_id,
    ).scalar()


def save_team(team, game_id, current_user=None):
    # try to find the game
    try:
        game = Game.query.filter(Game.id == game_id).one()
    except SQLAlchemyError:
        raise GameNotFoundException(f'Game {game_id} not found')

    # check, that the game is not running
    if game.status != GameStatus.INITIALIZED and game.status != GameStatus.ASYNC:
        raise GameRunningException(f'Game {game.title} was already started.')

    # if current_user is set, check that the game belongs to this user
    if current_user is not None:
        if game.owner_id != current_user:
            raise GameNotOwnedException(
                f'Game {game.title} dose not belong to you.')

    # if everything else is ok create the team
    game.teams.append(team)
    db.session.commit()


class UserNotFoundException(JoinTeamException):
    pass


class TeamNotFoundException(JoinTeamException):
    pass


class UserIsInTeamException(JoinTeamException):
    pass


def join_team(user, team_id):
    # try to find the team
    team = Team.query.get(team_id)
    if team is None:
        raise TeamNotFoundException(f'Team {team_id} was not found.')

    # check, that the user is not in this team
    for t_user in team.users:
        if user.id == t_user.id:
            raise UserIsInTeamException(
                f'User {user.name} is already in {team.name}')

    # check, that the game is not finished
    game = Game.query.get(team.game_id)
    if game.status == GameStatus.DONE or game.status == GameStatus.ABORTED:
        raise GameFinishedException(
            f'Game {game.title} is already finished/aborted.')

    # add the user to the team
    team.users.append(user)
    db.session.commit()


def join_team_by_id(user_id, team_id):
    user = User.query.get(user_id)

    if user is None:
        raise UserNotFoundException(f'User {user_id} was not found.')

    join_team(user, team_id)


def handle_lti_teams(user_data, user, game):
    if not game.bracketing_behavior == BracketingBehavior.MoodleApi:
        team_name = get_teamname_using_moodle_or_default(user_data)
        team = Team(name=team_name, bracket="a")
        # create a team if needed
        if not team_exists(team, game.id):
            save_team_brackets_auto(
                team, game.id, user_data)  # set the bracket
    else:
        team = get_team_obj_using_moodle(game.id, user_data)
        if not team_exists(team, game.id):
            save_team(team, game.id)

    # join the team if needed
    team = get_team(team, game.id)

    if user.id not in (u.id for u in team.users):
        join_team_by_id(user.id, team.id)

    return team
