from datetime import datetime

from marshmallow import fields


class DateTimeField(fields.DateTime):

    FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'

    def _serialize(self, value, attr, obj, **kwargs):

        if value is not None:
            return value.strftime(self.FORMAT)

    def _deserialize(self, value, attr, data):

        if value is not None:
            return datetime.strptime(value, self.FORMAT)
