from webargs import fields

from webapp.config.api import RESULTS_LIMIT_DEFAULT, RESULTS_LIMIT_MAX


pagination_kwargs = {
    'offset': fields.Int(missing=0, validate=lambda x: x >= 0),
    'limit': fields.Int(
        missing=RESULTS_LIMIT_DEFAULT,
        validate=lambda x: 0 < x <= RESULTS_LIMIT_MAX,
    ),
}
