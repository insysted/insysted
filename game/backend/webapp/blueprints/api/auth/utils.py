from flask import session, current_app, redirect
from flask_jwt_extended import get_jti

from webapp.config.base import LTI_GROUP_ID
from webapp.middleware import cache


def cache_token(token, is_revoked, is_jti=False, refresh=False):
    token_jti = get_jti(encoded_token=token) if not is_jti else token
    key = 'JWT_ACCESS_TOKEN_EXPIRES' if not refresh else 'JWT_REFRESH_TOKEN_EXPIRES'
    timeout = current_app.config[key] * 1.2
    cache.set(token_jti, {'is_revoked': is_revoked}, timeout)


def lti_launch_error(exception):
    exc = exception['exception']
    current_app.logger.error(f'User LTI request failed: {exc}')

    return redirect('/login')


def lti_user_data():

    try:
        user_id = session['user_id']
        user_name = session.get('ext_user_username') or session.get(
            'lis_person_name_full') or f'User {user_id}'

        return {
            'id': user_id,
            'name': user_name,
            'email': session['lis_person_contact_email_primary'],
            'roles': session['roles'],
            'group': session.get(LTI_GROUP_ID, 'NONE'),
            'title': session.get('resource_link_title', 'NONE'),
            'course': session.get('context_label', 'NONE'),
        }

    except KeyError as e:
        raise ValueError(f'Required value is missing: {e}')
