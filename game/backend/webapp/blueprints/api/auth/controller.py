import uuid
import urllib
from http import HTTPStatus
from datetime import datetime

from flask import (
    Blueprint,
    abort,
    current_app,
    jsonify,
    redirect,
    session,
    request
)
from sqlalchemy import exc
from flask_jwt_extended import (
    get_raw_jwt,
    create_access_token,
    create_refresh_token,
    current_user,
    jwt_required,
    jwt_refresh_token_required,
)
from moodle.moodleApi import MoodleException
from pylti.flask import lti
from sqlalchemy.orm import joinedload
from webargs import fields
from webargs.flaskparser import use_kwargs
from webapp.worker.tasks.game import is_async_game_running, run_bracket, start_async_game

from webapp.db.models import User, Role, Game, GameStatus, Team
from webapp.middleware import db, cache, jwt
from .utils import cache_token, lti_launch_error, lti_user_data

from webapp.blueprints.api.utils.teams import GameRunningException, handle_lti_teams, TeamCreateionException


auth = Blueprint('auth', __name__, url_prefix='/api/auth')


@auth.route('/login/', methods=['POST'])
@use_kwargs({
    'identity': fields.Str(required=True),
    'password': fields.Str(required=True),
})
def login(identity, password):
    user = User.query.filter_by(identity=identity).first()

    if user is None or not user.is_confirmed:
        abort(HTTPStatus.NOT_FOUND,
              description='Account is either not found or not confirmed')

    if not user.validate_password(password):
        abort(HTTPStatus.UNAUTHORIZED, description='Invalid user credentials')

    current_app.logger.info(f'Generate tokens for {user}')

    access_token = create_access_token(identity)
    cache_token(access_token, is_revoked=False)

    refresh_token = create_refresh_token(identity)
    cache_token(refresh_token, is_revoked=False, refresh=True)

    user.login_at = datetime.utcnow()
    db.session.commit()

    auth = {
        'tokens': {
            'access': access_token,
            'refresh': refresh_token,
        },
        'user': {
            'id': user.id,
            'roles': [str(r) for r in user.roles],
        },
    }

    return jsonify(auth=auth, success=True)


@auth.route('/logout/')
@jwt_required
def logout():
    access_jti = get_raw_jwt()['jti']
    cache_token(access_jti, is_revoked=True, is_jti=True)

    return jsonify(message='You have successfully logged out', success=True)


@auth.route('/access-token/<int:user_id>/')
@jwt_required
def access_token(user_id):

    if not current_user.has_role(Role.ADMIN):
        abort(HTTPStatus.BAD_REQUEST, description='Not allowed')

    user = User.query.get(user_id)

    if user is None:
        abort(HTTPStatus.NOT_FOUND, description='User not found')

    current_app.logger.info(f'Generate access token for {user}')

    access_token = create_access_token(user.identity)
    cache_token(access_token, is_revoked=False)

    return jsonify(access_token=access_token, success=True)


@auth.route('/refresh-access-token/')
@jwt_refresh_token_required
def refresh_token():
    current_app.logger.info(f'Refresh token for {current_user}')

    access_token = create_access_token(current_user.identity)
    cache_token(access_token, is_revoked=False)

    auth = {
        'tokens': {
            'access': access_token,
        },
        'user': {
            'roles': [str(r) for r in current_user.roles],
        },
    }

    return jsonify(auth=auth, success=True)


@jwt.token_in_blacklist_loader
def is_token_revoked(decrypted_token):
    jti = decrypted_token['jti']
    token = cache.get(jti)

    return token is None or token['is_revoked']


@jwt.revoked_token_loader
def revoked_token_callback():
    return jsonify(message='Token has been revoked', success=False), HTTPStatus.UNAUTHORIZED


@jwt.user_loader_callback_loader
def app_user_loader_callback(identity):
    return User.query.filter_by(identity=identity).first()


@jwt.user_loader_error_loader
def app_user_loader_error(identity):
    msg = f'User {identity} not found'
    current_app.logger.error(msg)

    return jsonify(message=msg, success=False), HTTPStatus.NOT_FOUND


@jwt.unauthorized_loader
def app_unauthorized_error(reason):
    msg = f'Unauthorized access: {reason.capitalize()}'
    current_app.logger.warning(msg)

    return jsonify(message=msg, success=False), HTTPStatus.UNAUTHORIZED


@jwt.invalid_token_loader
def app_invalid_token_error(reason):
    msg = f'Invalid token: {reason.capitalize()}'
    current_app.logger.error(msg)

    return jsonify(message=msg, success=False), HTTPStatus.UNAUTHORIZED


@jwt.expired_token_loader
def app_expired_token_error(reason):
    msg = f'Expired token: {reason}'
    current_app.logger.error(msg)

    return jsonify(message=msg, success=False), HTTPStatus.UNAUTHORIZED


@auth.route('/lti/', methods=['GET', 'POST'])
@auth.route('/lti/<int:game_id>/', methods=['GET', 'POST'])
@lti(error=lti_launch_error, request='initial', app=current_app)
def process_lti_request(lti=lti, game_id=None):
    current_app.logger.debug(f'Received LTI request {session}')
    user = User.query.filter_by(lti_user_id=lti.user_id).first()

    try:
        user_data = lti_user_data()

        # extend user_data
        user_data_ext = dict()
        if 'custom_bracket' in request.form:
            user_data_ext['bracket'] = request.form['custom_bracket']
        if 'group' in user_data:
            user_data_ext['group'] = user_data['group']
        user_data_ext['lis_outcome_service_url'] = request.form['lis_outcome_service_url']
        user_data_ext['context_id'] = request.form['context_id']

    except ValueError as e:
        current_app.logger.error(f'LTI request failed: {e}')

        return redirect('/login')

    if user is None:
        user = User.query.filter_by(email=user_data['email']).first()

        if user is None:
            user = User(
                name=user_data['name'],
                email=user_data['email'],
                password=uuid.uuid4().hex,
                lti_user_id=lti.user_id,
                is_confirmed=True,
            )
            db.session.add(user)
            current_app.logger.info(f'Created LTI {user}')
        else:
            user.lti_user_id = lti.user_id

    user.grant_role(Role.PLAYER)

    if lti.is_role(lti, 'staff'):
        user.grant_role(Role.DESIGNER)

    user.login_at = datetime.utcnow()
    db.session.commit()

    current_app.logger.info(f'Generate tokens for LTI {user}')

    access_token = create_access_token(user.identity)
    cache_token(access_token, is_revoked=False)

    refresh_token = create_refresh_token(user.identity)
    cache_token(refresh_token, is_revoked=False, refresh=True)

    if game_id is None:
        query = urllib.parse.urlencode({
            'access_token': access_token,
            'refresh_token': refresh_token,
        })

        return redirect(f'/login?{query}')

    game = Game.query.get(game_id)

    if game is None:
        return redirect('/login')

    if user_data['group'] == 'NONE':
        return 'User is not in a group.'

    try:
        team = handle_lti_teams(user_data_ext, user, game)
    except TeamCreateionException as e:
        return str(e)
    except MoodleException as e:
        return str(e)

    if game.status == GameStatus.DONE:
        score = round(team.score, 1)
        current_app.logger.info(
            f'{game} is over. Posting score {score} to LTI consumer...')
        success = lti.post_grade(score)

        if not success:
            current_app.logger.error('Failed to post score {score}')
            return 'Failed to post score.'

    query = urllib.parse.urlencode({
        'access_token': access_token,
        'refresh_token': refresh_token,
        'user_roles': ','.join(str(r) for r in user.roles),
        'redirect': f'/game/play/{team.id}',
    })

    return redirect(f'/login?{query}')


@auth.route('/lti/async/<int:game_id>/', methods=['GET', 'POST'])
@lti(error=lti_launch_error, request='initial', app=current_app)
def process_lti_async_request(lti=lti, game_id=-1):
    current_app.logger.debug(f'Received LTI request {session}')
    user = User.query.filter_by(lti_user_id=lti.user_id).first()

    try:
        user_data = lti_user_data()

        # extend user_data
        user_data_ext = dict()
        if 'custom_bracket' in request.form:
            user_data_ext['bracket'] = request.form['custom_bracket']
        if 'group' in user_data:
            user_data_ext['group'] = user_data['group']
        user_data_ext['lis_outcome_service_url'] = request.form['lis_outcome_service_url']
        user_data_ext['context_id'] = request.form['context_id']

    except ValueError as e:
        current_app.logger.error(f'LTI request failed: {e}')

        return redirect('/login')

    if user is None:
        user = User.query.filter_by(email=user_data['email']).first()

        if user is None:
            user = User(
                name=user_data['name'],
                email=user_data['email'],
                password=uuid.uuid4().hex,
                lti_user_id=lti.user_id,
                is_confirmed=True,
            )
            db.session.add(user)
            current_app.logger.info(f'Created LTI {user}')
        else:
            user.lti_user_id = lti.user_id

    user.grant_role(Role.PLAYER)

    if lti.is_role(lti, 'staff'):
        user.grant_role(Role.DESIGNER)

    user.login_at = datetime.utcnow()
    db.session.commit()

    current_app.logger.info(f'Generate tokens for LTI {user}')

    access_token = create_access_token(user.identity)
    cache_token(access_token, is_revoked=False)

    refresh_token = create_refresh_token(user.identity)
    cache_token(refresh_token, is_revoked=False, refresh=True)

    game = Game.query.get(game_id)
    if game is None:
        return redirect('/login')

    if user_data['group'] == 'NONE':
        return 'User is not in a group.'

    if game.status != GameStatus.ASYNC:
        return "The game is not async"

    try:
        team = handle_lti_teams(user_data_ext, user, game)
    except TeamCreateionException as e:
        return str(e)
    except MoodleException as e:
        return str(e)

    # move each team into its own bracket
    team.bracket = team.name

    db.session.commit()

    # start the bracket
    if not is_async_game_running(game_id, team):
        start_async_game(game_id, team)

    # redirect user
    query = urllib.parse.urlencode({
        'access_token': access_token,
        'refresh_token': refresh_token,
        'user_roles': ','.join(str(r) for r in user.roles),
        'redirect': f'/game/play/{team.id}',
    })

    return redirect(f'/login?{query}')
