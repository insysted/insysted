from flask import current_app
from itsdangerous import URLSafeTimedSerializer, BadSignature, SignatureExpired


class UserTokenEncoder:

    def __init__(self):
        self._serializer = URLSafeTimedSerializer(
            secret_key=current_app.config['SECRET_KEY'],
            salt=current_app.config.get('USER_ID_TOKEN_SALT'),
        )

    def encode_token(self, identity):
        return self._serializer.dumps(identity)

    def decode_token(self, token, expire_after=None):

        try:
            return self._serializer.loads(token, max_age=expire_after)
        except (BadSignature, SignatureExpired):
            pass
