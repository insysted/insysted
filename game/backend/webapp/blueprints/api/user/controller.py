import urllib
from http import HTTPStatus

from flask import (
    Blueprint,
    jsonify,
    abort,
    request,
    current_app,
)
from sqlalchemy import exc
from flask_jwt_extended import jwt_required, current_user
from sqlalchemy.exc import SQLAlchemyError
from webargs import fields
from webargs.flaskparser import use_args, use_kwargs
from marshmallow.validate import Length

from webapp.config.api import USER_ID_TOKEN_LIFE_TIME, USER_PASSWORD_LENGTH_MIN
from webapp.db.models import Game, GameStatus, Team, User, Role
from webapp.middleware import db
from webapp.worker.tasks import send_confirmation_email
from webapp.blueprints.api.utils.teams import join_team_by_id, join_team_by_id, JoinTeamException
from .schemas import UserSchema
from .utils import UserTokenEncoder


user = Blueprint('user', __name__, url_prefix='/api/user')


@user.route('/')
@user.route('/<int:user_id>/')
@jwt_required
def get_user(user_id=None):

    if user_id is None:
        user = current_user
    else:

        if not current_user.has_role(Role.ADMIN):
            abort(HTTPStatus.BAD_REQUEST, description='Not allowed')

        try:
            user = User.query.get(user_id)
        except SQLAlchemyError:
            abort(HTTPStatus.NOT_FOUND, description='User not found')

    data = UserSchema().dump(user)

    return jsonify(data=data, success=True)


@user.route('/update/', methods=['POST'])
@use_kwargs({
    'name': fields.Str(required=True),
    'password': fields.Str(missing=None, validate=Length(min=USER_PASSWORD_LENGTH_MIN)),
    'keep': fields.Bool(missing=False),
})
@jwt_required
def update(name, password, keep):
    current_user.name = name
    current_user.keep = keep

    if password is not None:
        current_user.password = password

    db.session.commit()

    return jsonify(message='User has been updated', success=True)


@user.route('/register/', methods=['POST'])
@use_args(UserSchema)
def register(user):
    domain_suffixes = current_app.config['USER_ALLOWED_EMAIL_DOMAIN_SUFFIXES']

    if domain_suffixes is not None:
        email_domain = user.email.split('@')[-1]

        if not any(s in email_domain for s in domain_suffixes):
            abort(HTTPStatus.BAD_REQUEST, description='Not allowed email domain')

    if db.session.query(User.id).filter_by(email=user.email).scalar() is not None:
        return abort(HTTPStatus.BAD_REQUEST, description=f'Email "{user.email}" already exists')

    if db.session.query(User.id).filter_by(name=user.name).first() is not None:
        return abort(HTTPStatus.BAD_REQUEST, description=f'Name "{user.name}" already exists')

    user.grant_role(Role.PLAYER)
    user.grant_role(Role.DESIGNER)

    user.is_confirmed = True

    db.session.add(user)
    db.session.commit()

    current_app.logger.info(f'Add {user}')

    # token = UserTokenEncoder().encode_token(user.identity)
    # confirm_url = f'{request.host_url}confirm/account/{token}'

    # current_app.logger.info(f'Send confirmation letter to {user}')
    # send_confirmation_email.delay(user.email, confirm_url)

    return jsonify(message='User has been registered', success=True), HTTPStatus.CREATED


@user.route('/confirm/<token>/')
def confirm_account(token):
    enc = UserTokenEncoder()
    identity = enc.decode_token(token, expire_after=USER_ID_TOKEN_LIFE_TIME)

    if identity is None:
        abort(HTTPStatus.BAD_REQUEST, description='Invalid or expired link')

    user = User.query.filter_by(identity=identity).first()

    if user is None:
        abort(HTTPStatus.NOT_FOUND, description='User not found')

    if not user.is_confirmed:
        current_app.logger.info(f'Confirm {user} account')
        user.is_confirmed = True
        db.session.commit()

    return jsonify(message='Account has been confirmed', success=True)


@user.route('/team/join/<team_id>/')
@jwt_required
def join_team(team_id):
    try:
        join_team_by_id(current_user.id, team_id)
    except JoinTeamException as e:
        abort(HTTPStatus.BAD_REQUEST, description=str(e))

    current_app.logger.info(f'Join {current_user} to {team_id}')
    return jsonify(message='User has joined the team', success=True)


user_team_args = {
    'user_id': fields.Int(required=True),
    'team_id': fields.UUID(required=True),
}


@user.route('/team/add/', methods=['POST'])
@use_kwargs(user_team_args)
@jwt_required
def add_to_team(user_id, team_id):

    team = Team.query.get(team_id)
    game = Game.query.get(team.game_id)
    if game.owner_id != current_user.id:
        abort(HTTPStatus.UNAUTHORIZED,
              description=f'Game dose not belong to {current_user.name}')

    try:
        join_team_by_id(user_id, team_id)
    except JoinTeamException as e:
        abort(HTTPStatus.BAD_REQUEST, description=str(e))

    current_app.logger.info(f'Add {user} to {team}')

    return jsonify(message='User has been added to team', success=True)


@user.route('/team/remove/', methods=['POST'])
@use_kwargs(user_team_args)
@jwt_required
def remove_from_team(user_id, team_id):

    team = Team.query.join(Game).filter(
        Team.id == team_id,
        Team.users.any(User.id == user_id),
        Game.owner_id == current_user.id,
        Game.status.in_([
            GameStatus.INITIALIZED,
            GameStatus.SUSPENDED,
        ]),
    ).first()

    if team is None:
        abort(HTTPStatus.BAD_REQUEST,
              description='Team not found, user not in team, or game has started')

    user = User.query.get(user_id)

    if user is None:
        abort(HTTPStatus.NOT_FOUND, description='User not found')

    current_app.logger.info(f'Remove {user} from {team}')

    team.users.remove(user)
    db.session.commit()

    return jsonify(message='User has been removed from team', success=True)
