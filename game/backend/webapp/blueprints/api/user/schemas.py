from marshmallow import Schema, fields, post_load, EXCLUDE
from marshmallow.validate import Length

from webapp.config.api import USER_PASSWORD_LENGTH_MIN
from webapp.db.models import User
from ..utils.datetime import DateTimeField


class UserSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.Str(required=True, validate=Length(min=1))
    password = fields.Str(required=True, validate=Length(
        min=USER_PASSWORD_LENGTH_MIN), load_only=True)
    email = fields.Email(required=True)
    games = fields.Nested('GameSchema', only=('id',),
                          many=True, dump_only=True)
    templates = fields.Nested('GameTemplateSchema', only=(
        'id',), many=True, dump_only=True)
    teams = fields.Nested('TeamSchema', only=(
        'id', 'name'), many=True, dump_only=True)
    created_at = DateTimeField(dump_only=True)
    updated_at = DateTimeField(dump_only=True)
    is_confirmed = fields.Bool(dump_only=True)
    login_at = DateTimeField(dump_only=True)
    lti_user_id = fields.Str(dump_only=True)
    roles = fields.Function(lambda u: [str(r)
                            for r in u.roles], dump_only=True)
    keep = fields.Bool()

    class Meta:
        unknown = EXCLUDE

    @post_load
    def make_user(self, data, **kwargs):
        return User(**data)
