from marshmallow import Schema, fields, post_load, ValidationError, EXCLUDE
from marshmallow.validate import Length

from core.simulation import Simulation
from webapp.db.models import Game, GameStatus, Team
from ..utils.datetime import DateTimeField


class GameSchema(Schema):
    id = fields.Int(dump_only=True)
    title = fields.Str(required=True, validate=Length(min=1))
    leaderboard = fields.Bool(required=True)
    description = fields.Str(required=True, validate=Length(min=1))
    simulation = fields.Dict(required=True)
    layout = fields.Dict(missing={})
    status = fields.Function(lambda g: g.status.name, dump_only=True)
    progress = fields.Dict(dump_only=True)
    start_at = DateTimeField(dump_only=True)
    stopped_at = DateTimeField(dump_only=True)
    created_at = DateTimeField(dump_only=True)
    owner = fields.Nested('UserSchema', only=('id', 'name'), dump_only=True)
    teams = fields.Nested('TeamSchema', only=('id', 'name', 'users', 'cash',
                          'created_at', 'bracket', 'plant', 'team_progress'), many=True, dump_only=True)
    template_id = fields.Int(missing=None)
    bracketing_behavior = fields.Function(lambda g: g.bracketing_behavior.name)
    number_of_brackets = fields.Int()

    class Meta:
        unknown = EXCLUDE

    @post_load
    def make_game(self, data, **kwargs):

        return Game(**data)


class GameTemplateSchema(Schema):
    id = fields.Int(dump_only=True)
    title = fields.Str(dump_only=True)
    leaderboard = fields.Bool(required=True)
    description = fields.Str(dump_only=True)
    simulation = fields.Dict(dump_only=True)
    layout = fields.Dict(dump_only=True)
    created_at = DateTimeField(dump_only=True)
    is_public = fields.Bool(dump_only=True)
    designer = fields.Nested('UserSchema', only=('id', 'name'), dump_only=True)
    games = fields.Method('template_games')

    def template_games(self, obj):
        return Game.query.filter_by(template_id=obj.id).count()


class TeamSchema(Schema):
    id = fields.UUID(dump_only=True)
    name = fields.Str(required=True, validate=Length(min=1))
    cash = fields.Int(dump_only=True)
    plant = fields.Dict(dump_only=True)
    actions = fields.Nested('GameActionSchema', exclude=(
        'team',), many=True, dump_only=True)
    users = fields.Nested('UserSchema', only=(
        'id', 'name'), many=True, dump_only=True)
    team_progress = fields.Dict(dump_only=True)
    game = fields.Nested('GameSchema', exclude=(
        'simulation', 'template_id'), dump_only=True)
    created_at = DateTimeField(dump_only=True)
    bracket = fields.Str(required=True)

    class Meta:
        unknown = EXCLUDE

    @post_load
    def make_team(self, data, **kwargs):
        return Team(**data)


class GameActionSchema(Schema):
    id = fields.Int(dump_only=True)
    user = fields.Nested('UserSchema', only=('id', 'name'), dump_only=True)
    team = fields.Nested('TeamSchema', only=('id',), dump_only=True)
    meta = fields.Dict(dump_only=True)
    created_at = fields.Str(dump_only=True)
