from multiprocessing.sharedctypes import Value
from webapp.blueprints.api.utils.teams import save_team as save_team_util, TeamCreateionException
import os
import uuid
import re
from http import HTTPStatus
from datetime import datetime, timezone

from flask import Blueprint, jsonify, abort, current_app
from flask_jwt_extended import jwt_required, current_user
from sqlalchemy import func, or_, desc
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import joinedload
from webargs import fields, validate
from webargs.flaskparser import use_args, use_kwargs

from pyro.server import SimulationServer
from Pyro5.errors import CommunicationError
from webapp.db.models import Game, GameStatus, GameTemplate, GameAction, Team, User, Role, BracketingBehavior
from webapp.middleware import db
from webapp.worker.tasks import run_game_server, revoke_game
from webapp.worker.tasks.game import start_async_game, reset_async_game
from .schemas import GameSchema, GameTemplateSchema, TeamSchema
from ..utils.query import pagination_kwargs
from core.base.exceptions import PlantPaymentError
from ..utils.teams import team_exists, save_team as save_team_util, get_team as get_team_util, join_team_by_id

from core.simulation import Simulation
from schema import SchemaError
from core.base.schema import params_dict


game = Blueprint('game', __name__, url_prefix='/api/game')


@game.route('/')
@game.route('/<int:game_id>/')
@use_kwargs({'title': fields.Str(missing=''), **pagination_kwargs}, location='querystring')
@jwt_required
def get_game(title, offset, limit, game_id=None):

    if game_id is None:

        q = Game.query.options(joinedload(Game.teams)).filter_by(
            owner_id=current_user.id)

        if title:
            q = q.filter(Game.title.ilike(f'%{title}%'))

        games = q.order_by(desc(Game.created_at)).offset(
            offset).limit(limit).all()

        exclude = ('simulation', 'layout', 'teams.users', 'teams.plant')
        data = GameSchema(exclude=exclude).dump(games, many=True)

        response = jsonify(data=data, success=True)
        response.headers['x-total-count'] = q.count()

        return response

    game = Game.query.options(joinedload(Game.teams)).filter(
        Game.id == game_id,
        Game.owner_id == current_user.id,
    ).first()

    if game is None:
        abort(HTTPStatus.NOT_FOUND, description='Game not found')

    data = GameSchema().dump(game)
    for team in data['teams']:
        try:
            pruned_stats = [{'plant': {'cash': stat['plant']['cash']}}
                            for stat in team['plant']['stats']]
        except (KeyError, TypeError):
            pruned_stats = []
        team["plant"] = {'stats': pruned_stats}

    return jsonify(data=data, success=True)


def schema_error_to_string(e):
    formatted_error = str(e)
    formatted_error = formatted_error.replace('Key \'template\' error:\n', '')
    formatted_error = formatted_error.replace('\n', '')
    formatted_error = re.search(
        '(Key \'.*?\'( error:)?)*', formatted_error).group()
    formatted_error = formatted_error.replace('Key ', '')
    formatted_error = formatted_error.replace(' error:', '\u2192')

    if formatted_error.endswith('\u2192'):
        formatted_error = formatted_error[:-1]
    return formatted_error


@game.route('/undefined/', methods=['POST'])
@game.route('/<string:bracketing_behavior>/', methods=['POST'])
@game.route('/<string:bracketing_behavior>/<int:game_id>/', methods=['POST'])
@use_args(GameSchema)
@jwt_required
def save_game(game, bracketing_behavior=BracketingBehavior.RoundRobin, number_of_brackets=2, game_id=None):
    try:
        sim_dict = game.__dict__['simulation']
        Simulation.validate_config(sim_dict)
    except SchemaError as e:
        formatted_error = schema_error_to_string(e)

        if formatted_error.startswith("'plant'→'units'→'"):
            unit_name = formatted_error[17:-1]
            if unit_name in sim_dict['template']['plant']['units']:
                unit = sim_dict['template']['plant']['units'][unit_name]
                if 'class' not in unit:
                    formatted_error = formatted_error + 'has no class'
                else:
                    unit_class = unit['class']
                    if unit_class not in params_dict:
                        formatted_error = formatted_error + ' ' + \
                            unit_class + ' is not a valid unit class'
                    else:
                        if 'params' not in unit:
                            formatted_error = formatted_error + ' ' + unit_class + ' has no params'
                        else:
                            try:
                                params_dict[unit_class].validate(
                                    unit['params'])
                            except SchemaError as e:
                                formatted_error = formatted_error + \
                                    '→' + schema_error_to_string(e)

        abort(HTTPStatus.BAD_REQUEST,
              description='Invalid game: one of the values at the following path is invalid ' + formatted_error)

    if not current_user.has_role(Role.DESIGNER):
        abort(HTTPStatus.BAD_REQUEST, description='Not allowed')

    if game_id is None:
        game.bracketing_behavior = bracketing_behavior
        game.number_of_brackets = number_of_brackets
        current_user.games.append(game)
        db.session.commit()
        current_app.logger.info(f'Save {game}')

        return jsonify(message='Game has been saved', game_id=game.id, success=True)

    try:
        current_app.logger.info(f'Update {game_id}')
        target = Game.query.filter(
            Game.id == game_id,
            Game.owner_id == current_user.id,
            Game.status == GameStatus.INITIALIZED,
        ).one()

        target.bracketing_behavior = bracketing_behavior

        for attr in ('title', 'description', 'simulation', 'layout', 'number_of_brackets', 'leaderboard'):
            setattr(target, attr, getattr(game, attr))

        db.session.commit()
    except SQLAlchemyError as e:
        abort(HTTPStatus.BAD_REQUEST,
              description='Game not found or has been started' + str(e))

    current_app.logger.info(f'Update {game}')

    return jsonify(message='Game has been updated', success=True)


@game.route('/<int:game_id>/', methods=['DELETE'])
@jwt_required
def delete_game(game_id):

    try:

        game = Game.query.filter(
            Game.id == game_id,
            Game.owner_id == current_user.id,
            Game.status != GameStatus.STARTED,
        ).one()

    except SQLAlchemyError:
        abort(HTTPStatus.BAD_REQUEST, description='Game not found or is running')

    current_app.logger.info(f'Delete {game}')

    db.session.delete(game)
    db.session.commit()

    return jsonify(message='Game has been deleted', success=True)


@game.route('/joinAsync/<int:game_id>/', methods=['GET'])
@jwt_required
def join_game(game_id):
    game = Game.query.get(game_id)
    # check if the game exists
    if game is None:
        abort(HTTPStatus.NOT_FOUND, description='Game not found.')

    # check if the game is aync
    if game.status != GameStatus.ASYNC:
        abort(HTTPStatus.BAD_REQUEST, description='Game is not async.')

    # check if the user has a team
    team = Team.query.options(
        joinedload(Team.users),
    ).filter(
        Team.game_id == game_id,
        Team.users.any(User.id == current_user.id)
    ).first()

    if team is None:  # if not create a team and join user
        team = Team(name=str(current_user.name), bracket="")
        team.bracket = str(uuid.uuid4())
        save_team_util(team, game_id)
        join_team_by_id(current_user.id, team.id)
        current_app.logger.info(
            f'Joined {current_user.id} to {team.id} of {game_id}')

    current_app.logger.info(
        f'Found {team} of game {team.game.id} with users {team.users} in bracket {team.bracket} for user {current_user.id}')

    return jsonify(team_id=team.id, success=True)


@game.route('/template/')
@game.route('/template/<int:template_id>/')
@use_kwargs({
    'title': fields.Str(missing=''),
    'public': fields.Bool(missing=False),
    **pagination_kwargs,
}, location='querystring')
@jwt_required
def get_template(title, public, offset, limit, template_id=None):

    if template_id is None:

        q = GameTemplate.query.filter(
            GameTemplate.is_public if public else GameTemplate.designer_id == current_user.id
        )

        if title:
            q = q.filter(GameTemplate.title.ilike(f'%{title}%'))

        templates = q.offset(offset).limit(limit).all()

        exclude = ('simulation', 'layout')
        data = GameTemplateSchema(exclude=exclude).dump(templates, many=True)

        response = jsonify(data=data, success=True)
        response.headers['x-total-count'] = q.count()

        return response

    try:

        template = GameTemplate.query.filter(
            GameTemplate.id == template_id,
            or_(
                GameTemplate.is_public,
                GameTemplate.designer_id == current_user.id,
            )
        ).one()

    except SQLAlchemyError:
        abort(HTTPStatus.NOT_FOUND, description='Template not found')

    data = GameTemplateSchema().dump(template)

    return jsonify(data=data, success=True)


@game.route('/template/', methods=['POST'])
@use_kwargs({
    'game_id': fields.Int(required=True),
    'public': fields.Bool(missing=False),
})
@jwt_required
def save_template(game_id, public):

    try:

        game = Game.query.filter(
            Game.id == game_id,
            Game.owner_id == current_user.id,
        ).one()

    except SQLAlchemyError:
        abort(HTTPStatus.NOT_FOUND, description='Game not found')

    template = GameTemplate(
        title=game.title,
        description=game.description,
        is_public=public,
        layout=game.layout,
        leaderboard=game.leaderboard,
        simulation={'template': game.simulation['template']},
    )

    template.games.append(game)
    current_user.templates.append(template)
    db.session.commit()

    current_app.logger.info(f'Save {template}')

    return jsonify(message='Template has been saved', success=True)


@game.route('/template/<int:template_id>/', methods=['DELETE'])
@jwt_required
def delete_template(template_id):

    try:

        template = GameTemplate.query.filter(
            GameTemplate.id == template_id,
            GameTemplate.designer_id == current_user.id,
        ).one()

    except SQLAlchemyError:
        abort(HTTPStatus.NOT_FOUND, description='Template not found')

    current_app.logger.info(f'Delete {template}')

    db.session.delete(template)
    db.session.commit()

    return jsonify(message='Game has been deleted', success=True)


@game.route('/template/<int:template_id>/', methods=['POST'])
@use_kwargs({
    'simulation': fields.Dict(required=True)
})
@jwt_required
def update_template(template_id, simulation):

    try:

        template = GameTemplate.query.filter(
            GameTemplate.id == template_id,
            GameTemplate.designer_id == current_user.id,
        ).one()

    except SQLAlchemyError:
        abort(HTTPStatus.NOT_FOUND, description='Template not found')

    current_app.logger.info(f'Update {template}')

    template.simulation = simulation
    db.session.commit()

    return jsonify(message='Template has been Updated', success=True)


@game.route('/team/')
@game.route('/team/<team_id>/')
@use_kwargs({'title': fields.Str(missing=''), **pagination_kwargs}, location='querystring')
@jwt_required
def get_team(title, offset, limit, team_id=None):

    if team_id is None:

        q = Team.query.join(Team.game).filter(
            Team.users.any(User.id == current_user.id))

        if title:
            q = q.filter(Game.title.ilike(f'%{title}%'))

        teams = q.offset(offset).limit(limit).all()
        data = TeamSchema(exclude=('plant', 'game.teams',
                          'game.teams.plant')).dump(teams, many=True)

        response = jsonify(data=data, success=True)
        response.headers['x-total-count'] = q.count()

        return response

    team = Team.query.join(Team.game).filter(
        Team.id == team_id,
        or_(
            Team.users.any(User.id == current_user.id),
            Game.owner_id == current_user.id,
        )
    ).first()

    if team is None:
        abort(HTTPStatus.NOT_FOUND, description='Team not found')

    data = TeamSchema(exclude=('game.teams.users',
                      'game.teams.plant')).dump(team)

    return jsonify(data=data, success=True)


@game.route('/team/', methods=['POST'])
@use_kwargs({
    'game_id': fields.Int(required=True),
    'team': fields.Nested(TeamSchema(), required=True),
})
@jwt_required
def save_team(team, game_id):
    try:
        save_team_util(team, game_id, current_user.id)
    except TeamCreateionException as e:
        abort(HTTPStatus.BAD_REQUEST, description=str(e))

    current_app.logger.info(f'Save {team}')

    return jsonify(message='Team has been saved')


@game.route('/team/<team_id>/', methods=['DELETE'])
@jwt_required
def delete_team(team_id):

    try:

        team = Team.query.join(Game).filter(
            Team.id == team_id,
            Game.owner_id == current_user.id,
        ).one()

    except SQLAlchemyError:
        abort(HTTPStatus.BAD_REQUEST,
              description='Team not found or game is running')

    current_app.logger.info(f'Delete {team}')

    db.session.delete(team)
    db.session.commit()

    return jsonify(message='Team has been deleted', success=True)


@game.route('/start/', methods=['POST'])
@use_kwargs({
    'game_id': fields.Int(required=True),
    'start_at': fields.DateTime(missing=None),
})
@jwt_required
def start_game(game_id, start_at):

    try:

        game = Game.query.join(Game.teams).filter(
            Game.id == game_id,
            Game.owner_id == current_user.id,
            Game.status == GameStatus.INITIALIZED,
        ).group_by(
            Game.id
        ).having(
            func.count(Team.id) > 0
        ).one()

    except SQLAlchemyError:
        abort(HTTPStatus.BAD_REQUEST,
              description='Game not found, has no teams, or is already running')

    current_app.logger.info(f'Schedule start of {game}')
    now = datetime.utcnow().replace(tzinfo=timezone.utc)

    if start_at is None or start_at < now:
        game.start_at = now
        message = 'Game has been started'
    else:
        game.start_at = start_at
        message = 'Game has been suspended till start date'

    game.status = GameStatus.SUSPENDED
    db.session.commit()

    run_game_server(game_id)

    return jsonify(message=message, success=True)


@game.route('/restart/', methods=['POST'])
@use_kwargs({'team_id': fields.Str(missing='')})
@jwt_required
def restart_game(team_id):
    try:
        team = Team.query.options(
            joinedload(Team.users),
        ).filter(
            Team.id == team_id,
            Team.users.any(User.id == current_user.id)
        ).first()
    except SQLAlchemyError:
        abort(HTTPStatus.BAD_REQUEST, description='Team not found.')

    if team.game.status != GameStatus.ASYNC:
        abort(HTTPStatus.BAD_REQUEST, description='Game is not async.')

    game_id = team.game.id

    reset_async_game(game_id, team)

    for action in team.actions:
        db.session.delete(action)

    db.session.commit()

    start_async_game(game_id, team)

    return jsonify(message=f'Restarted.', success=True)


@game.route('/async/', methods=['POST'])
@use_kwargs({'game_id': fields.Int(required=True)})
@jwt_required
def async_game(game_id):
    try:

        game = Game.query.filter(
            Game.id == game_id,
            Game.owner_id == current_user.id,
            Game.status == GameStatus.INITIALIZED,
        ).one()

    except SQLAlchemyError:
        abort(HTTPStatus.BAD_REQUEST,
              description='Game not found, has no teams, or is already running')

    current_app.logger.info(f'Made {game} asynchronous')

    game.status = GameStatus.ASYNC
    db.session.commit()

    return jsonify(message=f'Game is now asynchronous', success=True)


@game.route('/abort/', methods=['POST'])
@use_kwargs({'game_id': fields.Int(required=True)})
@jwt_required
def abort_game(game_id):

    try:

        game = Game.query.filter(
            Game.id == game_id,
            Game.owner_id == current_user.id,
            Game.status.notin_([
                GameStatus.DONE,
                GameStatus.ABORTED,
            ]),
        ).one()

    except SQLAlchemyError:
        abort(HTTPStatus.BAD_REQUEST, description='Game not found or not running.')

    current_app.logger.warning(f'ABORT {game}')

    revoke_game(game_id)

    game.status = GameStatus.ABORTED
    game.stopped_at = datetime.utcnow()
    db.session.commit()

    return jsonify(message='Game has been aborted', success=True)


@game.route('/flow/<control>/', methods=['POST'])
@use_kwargs({
    'game_id': fields.Int(required=True),
    'reset_day': fields.Int(missing=0),
})
@jwt_required
def control_game_flow(game_id, reset_day, control):

    try:

        game = Game.query.filter(
            Game.id == game_id,
            Game.owner_id == current_user.id,
            Game.status.in_([
                GameStatus.STARTED,
                GameStatus.SUSPENDED,
            ]),
        ).one()

    except SQLAlchemyError:
        abort(HTTPStatus.BAD_REQUEST, description='Game not found or not running')

    try:
        brackets = set()
        for team in game.teams:
            brackets.add(team.bracket)
        for bracket in brackets:

            srv = SimulationServer(
                game.id,
                bracket,
                ns_host=os.getenv('PYRO_NS_HOST'),
                ns_port=os.getenv('PYRO_NS_PORT'),
            )

            if control == 'suspend':

                suspended = srv.client.toggle_suspend()
                game.status = GameStatus.SUSPENDED if suspended else GameStatus.STARTED
                db.session.commit()

                msg = f'Change game status to {game.status.name}'
                response = jsonify(
                    message=msg, status=game.status.name, success=True)

            elif control == 'fastforward':

                fastforward = srv.client.toggle_fast_forward()
                mode = 'FAST-FORWARD' if fastforward else 'NORMAL'

                msg = f'Change game mode to {mode}'
                response = jsonify(message=msg, mode=mode, success=True)

            elif control == 'reset':

                srv.client.reset_clock(reset_day)

                msg = f'Reset game clock to day {reset_day}.'
                response = jsonify(message=msg, success=True)

            else:
                raise ValueError(f'Unknown game flow control: {control}')

    except Exception as e:
        abort(HTTPStatus.BAD_REQUEST, description=str(e))

    current_app.logger.info(msg)

    return response


@game.route('/action/', methods=['POST'])
@use_kwargs({
    'team_id': fields.UUID(required=True),
    'unit_id': fields.Str(required=True, validate=validate.Length(min=1)),
    'param': fields.Str(
        required=True,
        validate=lambda p: p in {
            'queue_policy',
            'lot_size',
            'contract',
            'warehouse_reorder_point',
            'warehouse_order_size',
            'workshop_purchase_machine',
            'workshop_retire_machine',
        },
    ),
    'value': fields.Str(required=True, validate=validate.Length(min=1)),
})
@jwt_required
def apply_action(team_id, unit_id, param, value):
    try:

        team = Team.query.options(
            joinedload(Team.users),
            joinedload(Team.game)
        ).filter(
            Team.id == team_id,
            User.id == current_user.id,
        ).one()

    except SQLAlchemyError:
        abort(HTTPStatus.NOT_FOUND, description='Team not found')

    if team.game.status != GameStatus.STARTED and team.game.status != GameStatus.ASYNC:
        abort(HTTPStatus.BAD_REQUEST, description='Game not running')

    try:
        srv = SimulationServer(
            team.game.id,
            team.bracket,
            ns_host=os.getenv('PYRO_NS_HOST'),
            ns_port=os.getenv('PYRO_NS_PORT'),
        )

        try:
            if param == 'queue_policy':

                srv.client.set_queue_policy(team.plant_id, unit_id, value)
                desc = f'Change queue policy to {value.upper()}'

            elif param == 'lot_size':

                srv.client.set_lot_size(team.plant_id, unit_id, int(value))
                desc = f'Change lot size to {value}'

            elif param == 'contract':
                current_app.logger.info(f"{team.game.id}, {team.bracket}")
                srv.client.set_contract(team.plant_id, unit_id, value)
                current_app.logger.info("here2")
                desc = f'Change contract to "{value.upper()}"'

            elif param == 'warehouse_reorder_point':

                srv.client.set_reorder_point(
                    team.plant_id, unit_id, int(value))
                desc = f'Change reorder point to {value}'

            elif param == 'warehouse_order_size':

                srv.client.set_order_size(team.plant_id, unit_id, int(value))
                desc = f'Change order size to {value}'

            elif param == 'workshop_purchase_machine':
                n = None
                try:
                    n = srv.client.purchase_machine(
                        team.plant_id, unit_id, int(value))
                except ValueError as e:
                    if str(e).startswith("Day"):
                        raise e
                    abort(HTTPStatus.BAD_REQUEST,
                          description="Not enough funds.")
                desc = f'Purchase {n} machines'

            elif param == 'workshop_retire_machine':
                try:
                    n = srv.client.retire_machine(
                        team.plant_id, unit_id, int(value))
                except ValueError as e:
                    if str(e).startswith("Day"):
                        raise e
                    abort(HTTPStatus.BAD_REQUEST,
                          description="Either all machines are busy or the workshop doesnt have any machines.")
                desc = f'Retire {n} machines'
        except ValueError as e:
            if not str(e).startswith("Day"):
                raise e
            abort(HTTPStatus.BAD_REQUEST,
                  description=str(e))

    except CommunicationError as e:
        current_app.logger.info(type(e))
        current_app.logger.info(e)
        abort(HTTPStatus.BAD_REQUEST,
              description='Game is no longer running, you cannot submit any more actions')

    day = int(team.team_progress['day']) + 1
    minute = int(team.team_progress['minute'])
    minute = str(minute % 60)
    minute = minute if len(minute) == 2 else '0' + minute

    created_at = f'day {day}'

    meta = {'log': desc, 'unit_id': unit_id}
    act = GameAction(user_id=current_user.id, team_id=team.id,
                     meta=meta, created_at=created_at)

    db.session.add(act)
    db.session.commit()

    current_app.logger.info(f'Action {current_user} from {team.name}: {meta}')

    return jsonify(message='Action has been applied', success=True)
