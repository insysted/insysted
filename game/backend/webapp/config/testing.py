from webapp.config import POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_DB


TESTING = True
SECRET_KEY = 'secret-key-testing'
SQLALCHEMY_DATABASE_URI = f'postgresql+psycopg2://{POSTGRES_USER}:{POSTGRES_PASSWORD}@postgres_test:5432/{POSTGRES_DB}'
