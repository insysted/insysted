from datetime import timedelta

import redis
from pylti.common import LTI_PROPERTY_LIST

from webapp.config import (
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    POSTGRES_DB,
    LTI_CONSUMER_KEY,
    LTI_SHARED_SECRET,
)


SQLALCHEMY_DATABASE_URI = f'postgresql+psycopg2://{POSTGRES_USER}:{POSTGRES_PASSWORD}@postgres:5432/{POSTGRES_DB}'
SQLALCHEMY_TRACK_MODIFICATIONS = False

JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=15)
JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=10)
JWT_BLACKLIST_ENABLED = True
JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']

CACHE_TYPE = 'redis'
CACHE_REDIS_HOST = 'redis'
CACHE_REDIS_PORT = 6379
CACHE_DEFAULT_TIMEOUT = None

REDIS_URL = f'redis://{CACHE_REDIS_HOST}:{CACHE_REDIS_PORT}/0'

SESSION_TYPE = CACHE_TYPE
SESSION_REDIS = redis.from_url(REDIS_URL)

CELERY_BROKER_URL = 'amqp://guest:guest@rabbit:5672'
CELERY_RESULT_BACKEND_URL = REDIS_URL
CELERY_RESULT_EXPIRES = 30 * 60
CELERY_TASK_ROUTES = {
    'webapp.worker.tasks.*': {
        'queue': 'default',
    },
}

# Don't forget to add the following line to custom parameters when
# configuring an external tool in Moodle:
#
# group_id=$Moodle.Person.userGroupIds
#
LTI_GROUP_ID = 'custom_group_id'
LTI_PROPERTY_LIST.append(LTI_GROUP_ID)

PYLTI_CONFIG = {
    'consumers': {
        LTI_CONSUMER_KEY: {
            'secret': LTI_SHARED_SECRET
        }
    }
}
