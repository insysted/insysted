USER_PASSWORD_LENGTH_MIN = 6

RESULTS_LIMIT_DEFAULT = 25
RESULTS_LIMIT_MAX = 100

USER_ID_TOKEN_LIFE_TIME = 3 * 24 * 60 * 60
