from datetime import timedelta


DEBUG = True
BUNDLE_ERRORS = True

SECRET_KEY = b'\x1e\x1dq"\xc6\xc0\x12\x16\xa1\xefFf\xef\x9cd\xd6'

EMAIL_CONFIG = {
    'username': 'support@insysted.com',
    'password': 'secret',
    'smtp': {
        'host': 'mailhog',
        'port': 1025,
    },
}

JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=60)

USER_ALLOWED_EMAIL_DOMAIN_SUFFIXES = None
