import subprocess

import click
from flask.cli import AppGroup, with_appcontext

from webapp.db.models import User, Role, Team
from webapp.middleware import db, cache


init_cli = AppGroup('init', help='Run app initialization.')


@init_cli.command('db', help='Initialize the app database.')
@with_appcontext
def create_tables():
    db.drop_all()
    db.create_all()


@init_cli.command('purge', help="Remove all users and teams not that are not marked as active.")
@with_appcontext
def purge():
    # delete all users not marked as active
    users = User.query.all()
    for user in users:
        if user.keep:
            continue
        print(f"Deleting user {user.name}")
        db.session.delete(user)
    # delete all teams
    teams = Team.query.all()
    for team in teams:
        print(f"Deleting team {team.name}")
        db.session.delete(team)

    db.session.commit()

    print("Completed purge")


@init_cli.command('cache', help='Clear the app cache.')
@with_appcontext
def clear_cache():
    cache.clear()


celery_cli = AppGroup('celery', help='Manage the job queue.')


@celery_cli.command('purge', help='Purge all pending tasks.')
@click.option('-q', '--queue', default='default', help='Queue name')
@with_appcontext
def purge_tasks(queue):
    subprocess.run(
        ['celery', '-A', 'webapp.app.celery', 'amqp' 'queue.purge', queue],
        check=True,
    )


user_cli = AppGroup('user', help='Manage app users.')


@user_cli.command('add', help='Add a user.')
@click.option('-n', '--name', default='John Smith', help='User name')
@click.option('-e', '--email', default='john@mail.com', help='User email')
@click.option('-p', '--password', default='password', help='User password')
@click.option('--designer', is_flag=True)
@click.option('--admin', is_flag=True)
@with_appcontext
def add_user(name, email, password, designer, admin):
    user = User(
        name=name,
        email=email,
        password=password,
        is_confirmed=True,
        keep=True
    )

    user.grant_role(Role.PLAYER)

    if designer:
        user.grant_role(Role.DESIGNER)

    if admin:
        user.grant_role(Role.ADMIN)

    db.session.add(user)
    db.session.commit()
