from urllib.parse import urlparse, parse_qs


from moodle.moodleApi import MoodleApi, find_grouping_with_group
from webapp.config import MOODLE_API_TOKEN
from webapp.db.models import Team


def get_team_obj_using_moodle(game_id, user_data):
    bracket_name, team_name = find_team_data_using_moodle(user_data)
    return Team(name=team_name, bracket=bracket_name, game_id=game_id)


def find_team_data_using_moodle(user_data):
    m = MoodleApi(user_data['lis_outcome_service_url'], MOODLE_API_TOKEN)
    grouping, group = find_grouping_with_group(
        m.get_course_groupings_and_groups(user_data['context_id']), user_data['group'])
    return grouping.name, group.name


def get_teamname_using_moodle_or_default(user_data):
    try:
        if MOODLE_API_TOKEN is None or not (len(MOODLE_API_TOKEN) > 0):
            raise Exception()  # jump to default
        return find_team_data_using_moodle(user_data)[1]
    except Exception:
        return 'Group ID ' + user_data['group']
