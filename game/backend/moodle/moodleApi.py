import urllib
import json


class MoodleException(Exception):
    pass


class MoodleTokenInvalidException(MoodleException):
    pass


class MoodleInvalidCourseException(MoodleException):
    pass


class MoodleInvalidParameterException(MoodleException):
    pass


class MoodleNoGroupuingsException(MoodleException):
    pass


class MoodleUnknownException(MoodleException):
    pass


class Group():
    def __init__(self, _name, _id):
        self.name = _name
        self.id = _id

    @staticmethod
    def from_obj(obj):
        return Group(obj['name'], obj['id'])


class Grouping():
    def __init__(self, _id, _name, _groups=[]):
        self.id = _id
        self.name = _name
        self.groups = _groups

    @staticmethod
    def from_obj(obj):
        if 'groups' in obj:
            return Grouping(obj['id'], obj['name'], [Group.from_obj(group) for group in obj['groups']])
        else:
            return Grouping(obj['id'], obj['name'], [])


def find_grouping_with_group(groupings, group_id):
    for grouping in groupings:
        for group in grouping.groups:
            if str(group.id) == str(group_id):
                return grouping, group

    raise MoodleException('Could not find a grouping for the provided group.')


class MoodleApi:
    def __init__(self, url, token):
        if token is None or token == '':
            raise MoodleTokenInvalidException("The token provided was empty.")

        self._token = token
        self._url = '{uri.scheme}://{uri.netloc}'.format(
            uri=urllib.parse.urlparse(url))

    def check_obj_for_errors(self, obj):
        if 'exception' in obj:
            if obj['errorcode'] == 'invalidtoken':
                raise MoodleTokenInvalidException()
            elif obj['errorcode'] == 'errorcoursecontextnotvalid' or obj['errorcode'] == 'invalidrecord':
                raise MoodleInvalidCourseException()
            elif obj['errorcode'] == 'invalidparameter':
                raise MoodleInvalidParameterException()
            else:
                raise MoodleUnknownException(obj['errorcode'])

    def get_groupings(self, course_id):
        req_url = f'{self._url}/webservice/rest/server.php?wstoken={self._token}&wsfunction=core_group_get_course_groupings&moodlewsrestformat=json&courseid={course_id}'
        with urllib.request.urlopen(req_url) as response:
            obj = json.load(response)

            self.check_obj_for_errors(obj)

            return [Grouping(grouping['id'], grouping['name']) for grouping in obj]

    def get_groupings_groups(self, groupings, course_id):
        if groupings == []:
            req_url = f'{self._url}/webservice/rest/server.php?wstoken={self._token}&wsfunction=core_group_get_course_groups&moodlewsrestformat=json&courseid={course_id}'
            with urllib.request.urlopen(req_url) as response:
                obj = json.load(response)
                self.check_obj_for_errors(obj)

                ug = Grouping('-1', 'ungrouped',
                              [Group.from_obj(group) for group in obj])
                return [ug]
        else:
            ids = '&'.join(
                [f'groupingids[]={grouping.id}' for grouping in groupings])
            req_url = f'{self._url}/webservice/rest/server.php?wstoken={self._token}&wsfunction=core_group_get_groupings&moodlewsrestformat=json&{ids}&returngroups=1'
            with urllib.request.urlopen(req_url) as response:
                obj = json.load(response)

                self.check_obj_for_errors(obj)

                return [Grouping.from_obj(grouping) for grouping in obj]

    def get_course_groupings_and_groups(self, course_id):
        return self.get_groupings_groups(self.get_groupings(course_id), course_id)
