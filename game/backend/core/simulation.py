from threading import Thread, Event, RLock

from .base.schema import simulation_schema, plant_schema
from .base.exceptions import StopSimulation
from .base.simulation import SimulationBase, SerializerInterface
from .clock import Clock
from .matcher import InventoryMatcher
from .office import Office
from .plant import Plant
from .queue import ProductQueue
from .shipment import Shipment
from .warehouse import Warehouse
from .workshop import Workshop
from .utils.decorators import require_day_is_play_day, check_thread_alive, lock_plant_update, get_plant_unit


class Simulation(SimulationBase, SerializerInterface, Thread):

    def __init__(
            self,
            template,
            game_id=None,
            bracket=None,
            plants=None,
            clock=None,
            order_days=None,
            **kwargs):
        super().__init__(**kwargs)
        self._game_id = game_id
        self._bracket = bracket
        self._template = template
        self._plants = {}

        if plants is not None:

            for plant_id, plant_dict in plants.items():
                self.add_plant(plant_id, plant_dict)

        self._order_days = order_days or {}
        self._abort = Event()
        self._running = Event()
        self._fast_forward = Event()
        self._update_lock = RLock()
        self.clock = Clock.from_dict(clock or template.get('clock') or {})
        self.update_callback = None
        self.stop_callback = None
        self.exc = None

    @property
    def plants(self):
        return list(self._plants)

    @staticmethod
    def validate_config(conf_dict, schema=None):

        if schema is None:
            schema = simulation_schema

        return schema.validate(conf_dict)

    @classmethod
    def from_dict(cls, conf_dict):
        conf_dict = cls.validate_config(conf_dict)

        return cls(**conf_dict)

    def to_dict(self, include_plants=False):
        sim_dict = {
            'template': self._template,
        }

        if self._order_days:
            sim_dict['order_days'] = self._order_days

        if include_plants and self._plants:
            sim_dict['plants'] = {
                p.id_: p.to_dict() for p in self._plants.values()
            }

        return sim_dict

    @lock_plant_update
    def add_plant(self, plant_id, plant_dict=None):

        if not plant_dict:
            plant_dict = self._template['plant']
        else:
            plant_dict = self.validate_config(plant_dict, plant_schema)

        self._plants[plant_id] = Plant.from_dict({
            'id_': plant_id,
            'simulation': self,
            **plant_dict,
        })

    @lock_plant_update
    def get_plant(self, plant_id):

        try:
            return self._plants[plant_id]
        except KeyError:
            raise ValueError(f'Plant not found: {plant_id}')

    @lock_plant_update
    def remove_plant(self, plant_id):

        try:
            del self._plants[plant_id]
        except KeyError:
            raise ValueError(f'Plant not found: {plant_id}')

    @require_day_is_play_day
    @get_plant_unit((
        InventoryMatcher,
        Office,
        ProductQueue,
        Shipment,
        Workshop,
    ))
    @lock_plant_update
    @check_thread_alive
    def set_queue_policy(self, unit, val):
        unit.queue.policy = val
        self._notify_update()

    @require_day_is_play_day
    @get_plant_unit((Office,))
    @lock_plant_update
    @check_thread_alive
    def set_lot_size(self, unit, val):
        unit.lot_size = val
        self._notify_update()

    @require_day_is_play_day
    @get_plant_unit((Office,))
    @lock_plant_update
    @check_thread_alive
    def set_contract(self, unit, val):
        unit.contract = val
        self._notify_update()

    @require_day_is_play_day
    @get_plant_unit((Warehouse,))
    @lock_plant_update
    @check_thread_alive
    def set_reorder_point(self, unit, val):
        unit.reorder_point = val
        self._notify_update()

    @require_day_is_play_day
    @get_plant_unit((Warehouse,))
    @lock_plant_update
    @check_thread_alive
    def set_order_size(self, unit, val):
        unit.order_size = val
        self._notify_update()

    @get_plant_unit((Workshop,))
    @lock_plant_update
    @check_thread_alive
    def manage_machines(self, unit, quantity, todo):

        if quantity < 1:
            raise ValueError(f'Expected quantity >= 1, got {quantity}')

        try:

            if todo == 'purchase':
                if unit.can_purchase_machines(quantity):
                    for i in range(quantity):
                        unit.purchase_machine()
                else:
                    raise ValueError(
                        f'Not enough cash to buy {quantity} machines')
            else:
                if len(unit.machines) > quantity:
                    for i in range(quantity):
                        unit.retire_machine()
                else:
                    raise ValueError(f'Not enough machines to sell {quantity}')

        except RuntimeError:

            if not i:
                raise

        self._notify_update()

        return i + 1

    @check_thread_alive
    def toggle_suspend(self):

        if self._running.is_set():
            self._running.clear()

            return True

        self._running.set()

        return False

    @check_thread_alive
    def toggle_fast_forward(self):

        if self._fast_forward.is_set():
            self._fast_forward.clear()

            return False

        self._fast_forward.set()

        return True

    @lock_plant_update
    @check_thread_alive
    def reset_clock(self, day):

        if self.clock.day < day:
            raise ValueError(f'Cannot reset clock to day {day}.')

        self.clock.reset(day)
        new_plants = {}

        for cur_plant in self._plants.values():
            stats = cur_plant.stats[:day or 1]
            plant_conf = stats[-1]['state']
            new_plants[cur_plant.id_] = Plant.from_dict({
                'id_': cur_plant.id_,
                'simulation': self,
                'stats': stats if day else None,
                **plant_conf,
            })

        self._plants = new_plants
        self._notify_update()

    def run(self):
        self._log.info(f'START {self}')
        self._setup_supply_chain()
        self._running.set()

        try:

            if not self._plants:
                raise RuntimeError('Plants not found')

            while self._running.wait():

                with self._update_lock:
                    self._update_state()
                    self.clock.tick()

                if self.clock.fast_forward or self._fast_forward.is_set():

                    if self._abort.is_set():
                        raise StopSimulation

                    continue

                if self._abort.wait(self.clock.tick_delay):
                    raise StopSimulation

        except StopSimulation:
            self._log.info(f'FINISH {self}')
        except Exception as e:
            self._log.exception(f'Unexpected error in {self}:')
            self.exc = e
        finally:

            if self.stop_callback is not None:
                self.stop_callback()

    def stop(self, timeout=None):
        self._fast_forward.clear()
        self._running.set()
        self._abort.set()

        # discard the plants
        del self._plants

        self.join(timeout)

    def _setup_supply_chain(self):
        supply_chain = self._template.get('supply_chain')

        if supply_chain is None:
            return

        self._log.debug(f'Setup supply chain settings for {self}')

        for plant_id, options in supply_chain.items():

            if plant_id not in self._plants:
                continue

            for option in ('customers', 'suppliers'):

                for unit_id, settings in options.get(option, {}).items():

                    if type(settings) is dict:

                        if settings['plant'] not in self._plants:
                            continue

                    else:
                        settings = [
                            s for s in settings if s['plant'] in self._plants]

                        if not settings:
                            continue

                    unit = self.get_plant(plant_id).get_unit(unit_id)
                    unit.supply_chain = settings

    def _update_state(self):
        if self.clock.new_day:
            self._log.info(f'UPDATE {self}')

        if self.clock.ticks == 0:
            self._notify_update()

        for plant in self._plants.values():
            plant.update_state()

        if not self.clock.last_tick_of_day:
            return

        self._log.debug(f'UPDATE {self} plant rankings')

        plant_stats = [p.stats[-1] for p in self._plants.values()]
        plant_stats.sort(key=lambda s: s['state']['cash'], reverse=True)
        n_plants = len(self._plants)

        for rank, stats in enumerate(plant_stats):
            stats.update({
                'ranking': rank + 1,
                'score': 1 - rank / (n_plants - 1 or 1)
            })

        self._notify_update()

    def _notify_update(self):

        if self.update_callback is None:
            return

        update = {
            'progress': {
                'day': self.clock.day,
                'minute': self.clock.minute,
                'total_days': self.clock.total_days,
            },
            'state': self.to_dict(include_plants=True),
        }

        self.update_callback(update)

    def __str__(self):
        return f'<Simulation ({self._game_id}-{self._bracket}) day={self.clock.day}, min={self.clock.minute}>'
