from .base.exceptions import PlantWarehouseError, PlantPaymentError
from .base.simulation import PlantBase, StatisticsMixin, SerializerMixin
from .customer import CustomerMixin
from .matcher import InventoryMatcher


class Warehouse(PlantBase, CustomerMixin, StatisticsMixin, SerializerMixin):

    def __init__(
            self,
            plant,
            supplier,
            reorder_point,
            order_size,
            inventory,
            unit_storage_cost=None,
            **kwargs):
        super().__init__(plant, office=supplier, **kwargs)
        self._inventory = inventory
        self._unit_storage_cost = unit_storage_cost or 0
        self._orders = 0
        self.reorder_point = reorder_point
        self.order_size = order_size

    @property
    def supplier(self):

        if self.office.supply_chain is None:
            return self.office

        plant = self._plant.simulation.get_plant(
            self.office.supply_chain['plant'])

        return plant.get_unit(self.office.supply_chain['office'])

    @property
    def inventory(self):
        return self._inventory

    @property
    def reorder_point(self):
        return self._reorder_point

    @reorder_point.setter
    def reorder_point(self, val):

        if type(val) is not int or val < 0:
            raise ValueError(f'Invalid reorder point: {val}')

        self._reorder_point = val

    @property
    def order_size(self):
        return self._order_size

    @order_size.setter
    def order_size(self, val):

        if type(val) is not int or val <= 0:
            raise ValueError(f'Invalid order size: {val}')

        self._order_size = val

    def to_dict(self):
        return {
            'supplier': self.office.id_,
            'reorder_point': self._reorder_point,
            'order_size': self._order_size,
            'inventory': self._inventory,
            'unit_storage_cost': self._unit_storage_cost,
        }

    def find_connected_matchers(self):
        for unitName in self._plant.units:
            unit = self._plant.get_unit(unitName)
            if isinstance(unit, InventoryMatcher) and unit._warehouse == self:
                yield unit

    def calculate_total_needed_inventory(self):
        totalNeeded = 0
        for matcher in self.find_connected_matchers():
            totalNeeded += matcher.calculate_needed_inventory()

        return totalNeeded

    def calculate_on_hand(self):
        return self._inventory

    # calculate the sum of the inventory on hand and the inventory that is currently beeing shiped
    def calculate_available_stock(self):
        # calc items in unpaid finished orders
        finishedOrderItemCount = 0
        for order in self.finished_orders._items:
            finishedOrderItemCount += order.batch_size

        ongoingOrderItemCount = self.supplier._contract_terms['batch_size'] * self._orders

        return self.calculate_on_hand() + finishedOrderItemCount + ongoingOrderItemCount

    def calc_inventory_sum(self):
        return self.calculate_available_stock() - self.calculate_total_needed_inventory()

    def update_state(self):
        self._log.debug(f'UPDATE {self}')

        # receive completed orders
        while not self.finished_orders.empty:
            order = self.finished_orders.pop()
            # pay for the order
            try:
                self._plant.pay_order(order)
                self._log.debug(f'{order.revenue}')
            except PlantPaymentError as e:
                self._log.debug(f'{self} failed to pay {order}: {e}')
                self.finished_orders.put(order, back=True)
                break

            self._inventory += order.batch_size
            self._orders -= 1
            self._log.debug(f'{self} received finished {order} from supplier')

        if self.calc_inventory_sum() <= self.reorder_point:
            # reorder
            self._orders += self._order_size
            self.supplier.put_order(
                customer=self, number_of_orders=self._order_size)

        # pay storeage costs
        withdraw_storage_costs = (
            self._plant.simulation.clock.new_day and self._inventory and self._unit_storage_cost
        )

        if withdraw_storage_costs:
            storage_costs = self._inventory * self._unit_storage_cost
            self._plant.pay_costs(
                storage_costs, "warehouse storage", credit=True)
            self._log.debug(f'{self} billed storage costs {storage_costs}$')

        # update stats
        self._update_stats('on hand inventory',
                           self._inventory, aggregate='last')
        self._update_stats('available stock',
                           self.calc_inventory_sum(), aggregate='last')
        self._update_stats(
            'reorder point', self._reorder_point, aggregate='last')

    def withdraw_inventory(self, quantity):

        if self._inventory < quantity:
            raise PlantWarehouseError('Not enough inventory')

        self._inventory -= quantity

    def __str__(self):
        return f'<Warehouse {self.id_}, plant={self._plant.id_}, inventory={self._inventory}>'
