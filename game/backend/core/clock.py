import math

from .base.config import CLOCK_UPDATE_PERIOD
from .base.exceptions import StopSimulation
from .base.simulation import SerializerMixin

from datetime import datetime
from datetime import date
from datetime import timedelta


class Clock(SerializerMixin):

    def __init__(
            self,
            ticks=0,
            prior_days=50,
            play_days=100,
            post_days=50,
            days_per_hour=1,
            tick_seconds=CLOCK_UPDATE_PERIOD):
        self._ticks = ticks
        self._prior_days = prior_days
        self._play_days = play_days
        self._post_days = post_days
        self._tick_delay = tick_seconds / days_per_hour / 24
        self._ticks_per_day = 24 * 60 * 60 // tick_seconds
        self._days_per_hour = days_per_hour
        self._tick_seconds = tick_seconds

        # get current month as datetime i.e. if it is the 13th May 2022 we want 1st May 2022
        fistDayDate = date.today()
        self._created_at_datetime = datetime(
            day=1, month=fistDayDate.month, year=fistDayDate.year)

    def to_dict(self):
        return {
            'ticks': self._ticks,
            'tick_seconds': self._tick_seconds,
            'prior_days': self._prior_days,
            'play_days': self._play_days,
            'post_days': self._post_days,
            'days_per_hour': self._days_per_hour,
        }

    @property
    def day(self):
        return self._ticks // self._ticks_per_day

    @property
    def minute(self):
        return self._ticks % self._ticks_per_day * self._tick_seconds // 60

    @property
    def new_day(self):
        return not self._ticks % self._ticks_per_day

    @property
    def last_tick_of_day(self):
        return not (self._ticks + 1) % self._ticks_per_day

    @property
    def total_days(self):
        return self._prior_days + self._play_days + self._post_days

    @property
    def tick_delay(self):
        return self._tick_delay

    @property
    def fast_forward(self):
        prior_days = self.day < self._prior_days
        post_days = self.day >= self._prior_days + self._play_days

        return prior_days or post_days

    @property
    def ticks(self):
        return self._ticks

    def tick(self):
        self._ticks = self._ticks + 1

        if self.day > self.total_days - 1:
            raise StopSimulation

    def reset(self, day=0):

        if day < 0:
            raise ValueError(f'Invalid day value: {day}')

        self._ticks = self.to_ticks(day)

    def to_ticks(self, days):
        return math.ceil(days * self._ticks_per_day)

    def to_ticks_from_minutes(self, minutes):
        return minutes * 60 / CLOCK_UPDATE_PERIOD

    def to_ticks_from_hours(self, hours):
        return self.to_ticks_from_minutes(hours * 60)

    def to_seconds_from_ticks(self, ticks):
        return ticks * CLOCK_UPDATE_PERIOD

    def to_minutes_from_ticks(self, ticks):
        return self.to_seconds_from_ticks(ticks) / 60

    def to_hours_from_thisk(self, ticks):
        return self.to_ticks_from_minutes(ticks) / 60

    def ticks_to_formatted_time(self, ticks):
        datetime_with_offset = self._created_at_datetime + \
            timedelta(seconds=self.to_seconds_from_ticks(ticks))
        return datetime_with_offset.strftime("%m/%d/%Y %H:%M:%S")

    def __str__(self):
        return f'<Clock day={self.day}, min={self.minute}>'
