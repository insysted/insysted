import numpy as np

from .base.simulation import PlantBase, SerializerMixin
from .office import Office
from .utils.random import get_random_seed, notNegative


class DummyPlant:

    def __init__(self, simulation):
        self.simulation = simulation

    def receive_payment(self, cash):
        pass


class DeliveryTimeBehavior:
    Normal = 'normal'
    Constant = 'constant'


DeliveryTimeBehaviors = {
    DeliveryTimeBehavior.Normal,
    DeliveryTimeBehavior.Constant,
}


class Supplier(Office):

    def __init__(self, plant, warehouse, delivery_time_type, delivery_time_constant,
                 delivery_time_normal_avg,
                 delivery_time_normal_sigma, supply_chain=None, **kwargs):
        super().__init__(plant, **kwargs)
        self._warehouse_id = warehouse
        self._proc_ticks = list()
        self._orders = list()
        self._dummy_plant = DummyPlant(plant.simulation)
        self.supply_chain = supply_chain
        self._order_queue_len = 0
        self.delivery_time_type = delivery_time_type
        self.delivery_time_constant = delivery_time_constant
        self.delivery_time_normal_avg = delivery_time_normal_avg
        self.delivery_time_normal_sigma = delivery_time_normal_sigma

    @property
    def warehouse(self):
        return self._plant.get_unit(self._warehouse_id)

    def to_dict(self):
        supp_dict = {
            'delivery_time_type': self.delivery_time_type,
            'delivery_time_constant': self.delivery_time_constant,
            'delivery_time_normal_avg': self.delivery_time_normal_avg,
            'delivery_time_normal_sigma': self.delivery_time_normal_sigma,
            'warehouse': self.warehouse.id_,
            **super().to_dict(),
        }

        if self.supply_chain is not None:
            supp_dict['supply_chain'] = self.supply_chain

        return supp_dict

    def getProcTime(self):
        if self.delivery_time_type == DeliveryTimeBehavior.Constant:
            return self.delivery_time_constant
        elif self.delivery_time_type == DeliveryTimeBehavior.Normal:
            return notNegative(self.get_rng().normal(
                self.delivery_time_normal_avg,
                self.delivery_time_normal_sigma))

    def update_state(self):
        self._log.debug(f'UPDATE {self}')

        self._set_default('orders', [])

        # check order processing time
        completedIndexs = list()
        for i in range(len(self._proc_ticks)):
            if self._plant.simulation.clock._ticks >= self._proc_ticks[i]:
                completedIndexs.append(i)

        # finish orders
        completedIndexs.reverse()
        for i in completedIndexs:
            self._proc_ticks.pop(i)
            order = self._orders.pop(i)
            self._order_queue_len -= 1

            before_inventory = self.warehouse.inventory - \
                self.warehouse.calculate_total_needed_inventory()

            for lot in order.lots:
                lot.finish()

            order.customer.finished_orders.put(order)

            order_rep = order.to_dict()
            order_rep['inventory_before'] = before_inventory
            order_rep['inventory_after'] = self.warehouse.inventory - \
                self.warehouse.calculate_total_needed_inventory()
            del order_rep['customer'], order_rep['revenue'], order_rep['lead_time'], order_rep['lead_time_max']
            self._update_stats('orders', order_rep, aggregate=None)

        # add all new orders
        newOrder = self.get_order()
        while newOrder is not None:
            # calc random shipping time
            proc_time = self.getProcTime()
            proc_ticks = self._plant.simulation.clock._ticks + \
                self._plant.simulation.clock.to_ticks_from_hours(proc_time)
            # accept the order
            newOrder.accept(self._dummy_plant, pipeline_id=None)

            self._log.debug(
                f'{self} accepted {newOrder}, done in {proc_time:.2f} tick')
            self._order_queue_len += 1

            # append the order and shipping time
            self._orders.append(newOrder)
            self._proc_ticks.append(proc_ticks)

            newOrder = self.get_order()

        self._update_stats(
            'queue size', self._order_queue_len, aggregate='last')

    def __str__(self):
        return f'<Supplier {self.id_}, plant={self._plant.id_}>'
