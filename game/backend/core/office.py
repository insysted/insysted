from .base.simulation import PlantBase, StatisticsMixin, SerializerMixin
from .order import Order
from .queue import QueueMixin, QueueEmptyError


class Office(PlantBase, QueueMixin, StatisticsMixin, SerializerMixin):

    def __init__(
            self,
            plant,
            contracts,
            contract_active,
            lot_size,
            **kwargs):
        super().__init__(plant, **kwargs)
        self._contracts = contracts
        self._incoming_orders = 0
        self.contract = contract_active
        self.lot_size = lot_size

    @property
    def lot_size(self):
        return self._lot_size

    @lot_size.setter
    def lot_size(self, val):

        val = int(val)

        if val < 1:
            raise ValueError(f'Expected size > 0, got {val}')

        self._lot_size = val

    @property
    def contract(self):
        return self._contract_active

    @contract.setter
    def contract(self, val):

        if val not in self._contracts:
            raise ValueError(f'{val} not in {list(self._contracts)}')

        self._contract_active = val

    def to_dict(self):
        return {
            'contracts': self._contracts,
            'contract_active': self._contract_active,
            'lot_size': self._lot_size,
            'queue_policy': self.queue.policy,
        }

    def update_state(self):
        self._log.debug(f'UPDATE {self}')

        self._update_stats('incoming orders',
                           self._incoming_orders, aggregate='sum')
        self._incoming_orders = 0

    def put_order(self, customer, number_of_orders=1):
        charge_terms = dict(self._contract_terms)
        for _ in range(number_of_orders):
            order = Order(customer, lot_size=self._lot_size, **charge_terms)
            self.queue.put(order)
            self._log.debug(f'{self} received order from {customer}')
            self._incoming_orders += 1

            # only charge the fixed price on the first order.
            charge_terms['fix_price'] = 0

    def get_order(self):

        try:
            return self.queue.pop()
        except QueueEmptyError:
            pass

    @property
    def _contract_terms(self):
        return self._contracts[self._contract_active]

    def __str__(self):
        return f'<Office {self.id_}, plant={self._plant.id_}>'
