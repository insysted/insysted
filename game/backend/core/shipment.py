from collections import defaultdict

from .base.simulation import PlantBase, StatisticsMixin, SerializerMixin
from .queue import QueueMixin


class Shipment(PlantBase, StatisticsMixin, QueueMixin, SerializerMixin):

    def __init__(self, plant, unit_storage_cost=None, **kwargs):
        super().__init__(plant, **kwargs)
        self._unit_storage_cost = unit_storage_cost or 0
        self._order_lots = defaultdict(list)

    @property
    def total_lots(self):
        return sum(len(lots) for lots in self._order_lots.values())

    def to_dict(self):
        return {
            'unit_storage_cost': self._unit_storage_cost,
            'queue_policy': self.queue.policy,
        }

    def update_state(self):
        self._log.debug(f'UPDATE {self}')

        while not self.queue.empty:
            lot = self.queue.pop()
            lot.finish()
            self._order_lots[lot.order].append(lot)
            self._log.debug(f'{self} received {lot}')

        if not self._order_lots:
            return

        closed_orders = [
            order for order in self._order_lots
            if self._plant.close_order(order)
        ]

        for order in closed_orders:
            del self._order_lots[order]

        if self._plant.simulation.clock.new_day and self._unit_storage_cost:
            storage_costs = sum(
                sum(self._unit_storage_cost * lot.size for lot in lots)
                for lots in self._order_lots.values()
            )
            self._plant.pay_costs(
                storage_costs, "shipment storage", credit=True)
            self._log.debug(f'{self} billed {storage_costs}$ storage costs')

    def __str__(self):
        return f'<Shipment {self.id_}, plant={self._plant.id_}, lots={self.total_lots}>'
