from collections import deque

from .base.simulation import PlantBase, SerializerMixin


class QueuePolicy:
    FIFO = 'fifo'
    LIFO = 'lifo'


QUEUE_POLICIES = {
    QueuePolicy.FIFO,
    QueuePolicy.LIFO,
}


class QueueEmptyError(Exception):
    pass


class Queue:

    def __init__(self, policy=QueuePolicy.FIFO):
        self._items = deque()
        self._methods = {
            QueuePolicy.FIFO: {
                'pop': self._items.popleft,
                'push': self._items.appendleft,
            },
            QueuePolicy.LIFO: {
                'pop': self._items.pop,
                'push': self._items.append,
            },
        }
        self.policy = policy

    @property
    def policy(self):
        return self._policy

    @policy.setter
    def policy(self, val):
        val = val.lower()

        if val not in QUEUE_POLICIES:
            raise ValueError(f'Unknown queue type: {val}')

        self._policy = val

    @property
    def empty(self):
        return not len(self._items)

    def put(self, item, back=False):

        if back:
            self._methods[self._policy]['push'](item)
        else:
            self._items.append(item)

    def pop(self):

        try:
            return self._methods[self._policy]['pop']()
        except IndexError:
            raise QueueEmptyError

    def __len__(self):
        return len(self._items)


class QueueMixin:

    def __init__(self, queue_policy=QueuePolicy.FIFO, **kwargs):
        super().__init__(**kwargs)
        self.queue = Queue(queue_policy)


class ProductQueue(PlantBase, QueueMixin, SerializerMixin):

    def to_dict(self):
        return {'queue_policy': self.queue.policy}

    def update_state(self):
        self._log.debug(f'UPDATE {self}')

        while not self.queue.empty:
            lot = self.queue.pop()
            self._plant.pipeline_transfer(lot)

    def __str__(self):
        return ('<ProductQueue '
                f'{self.id_}, '
                f'policy={self.queue.policy}, '
                f'size={len(self.queue)}>')
