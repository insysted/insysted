from .customer import Customer
from .matcher import InventoryMatcher
from .office import Office
from .queue import ProductQueue
from .shipment import Shipment
from .supplier import Supplier
from .warehouse import Warehouse
from .workshop import Workshop


UNIT_CLASS_MAP = {
    'customer': Customer,
    'office': Office,
    'queue': ProductQueue,
    'matcher': InventoryMatcher,
    'shipment': Shipment,
    'supplier': Supplier,
    'warehouse': Warehouse,
    'workshop': Workshop,
}
CLASS_UNIT_MAP = {v: k for k, v in UNIT_CLASS_MAP.items()}
