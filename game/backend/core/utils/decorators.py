import functools


def check_thread_alive(method):

    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):

        if not self.is_alive():
            raise RuntimeError(f'{self} is not running')

        return method(self, *args, **kwargs)

    return wrapper


def lock_plant_update(method):

    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):

        with self._update_lock:
            return method(self, *args, **kwargs)

    return wrapper


def require_day_is_play_day(method):

    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):

        if self.clock.day < self.clock._prior_days or self.clock.day >= self.clock._prior_days + self.clock._play_days:
            raise ValueError(
                f'Day {self.clock.day} is not a play day. Play days are between {self.clock._prior_days} and {self.clock._prior_days + self.clock._play_days}.')

        return method(self, *args, **kwargs)

    return wrapper


def get_plant_unit(unit_types):

    def decorator(method):

        @functools.wraps(method)
        def wrapper(self, plant_id, unit_id, *args, **kwargs):
            unit = self.get_plant(plant_id).get_unit(unit_id)

            if not isinstance(unit, unit_types):
                raise TypeError(f'Wrong unit type: {unit}')

            return method(self, unit, *args, **kwargs)

        return wrapper

    return decorator
