import logging
from logging.config import dictConfig

from core.base.config import LOGGER_NAME


class LevelFilter(logging.Filter):

    def filter(self, record):
        return record.levelno in (logging.DEBUG, logging.INFO, logging.WARNING)


LOGGER_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '[%(levelname)s] %(asctime)s - %(name)s - %(message)s',
        },
        'debug': {
            'format': '[%(levelname)s] %(asctime)s %(filename)s:%(lineno)d - %(name)s - %(message)s',
        },
    },
    'filters': {
        'level': {
            '()': LevelFilter,
        },
    },
    'handlers': {
        'debug': {
            'level': 'DEBUG',
            'formatter': 'debug',
            'class': 'logging.StreamHandler',
        },
        'console': {
            'level': 'INFO',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
            'filters': ['level'],
        },
        'error': {
            'level': 'ERROR',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        LOGGER_NAME: {
            'handlers': ['console', 'error'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}


def configure(level=logging.INFO):
    log_conf = LOGGER_CONFIG['loggers'][LOGGER_NAME]
    log_conf['level'] = logging.getLevelName(level)

    if level == logging.DEBUG:
        log_conf['handlers'] = ['debug']

    dictConfig(LOGGER_CONFIG)

    return logging.getLogger(LOGGER_NAME)
