import random
import string


ALPHABET = string.ascii_lowercase + string.digits


def generate(length=10):
    return ''.join(random.choices(ALPHABET, k=length))
