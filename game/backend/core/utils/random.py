def notNegative(number):
    if number < 0:
        return 0
    else:
        return number


def get_random_seed(unit):
    seed = unit._plant.simulation._template['seed']
    ticks = unit._plant.simulation.clock._ticks
    name = unit.id_
    return sum(ord(c) for c in name) + ticks + seed
