from .base.simulation import PlantBase, SerializerMixin


class Order(PlantBase, SerializerMixin):

    def __init__(
            self,
            customer,
            batch_size,
            batch_price,
            fix_price,
            lead_time,
            lead_time_max,
            lot_size=1,
            **kwargs):
        super().__init__(plant=None, **kwargs)
        self.customer = customer
        self.batch_size = batch_size
        self.batch_price = batch_price
        self.fix_price = fix_price
        self.lead_time = lead_time
        self.lead_time_max = lead_time_max
        self.lot_size = lot_size
        self.lots = []
        self._accepted_at = None
        self._finished_at = None

    @property
    def plant(self):
        return self._plant

    @property
    def is_finished(self):
        return self._finished_at is not None

    @property
    def revenue(self):

        if not self.is_finished:
            return None

        rev = self.batch_price + self.fix_price
        actual_lead_time = self._finished_at - self._accepted_at

        lead_time_ticks = self._plant.simulation.clock.to_ticks_from_hours(
            self.lead_time)
        lead_time_max_ticks = self._plant.simulation.clock.to_ticks_from_hours(
            self.lead_time_max)

        if actual_lead_time <= lead_time_ticks:
            return rev

        elif actual_lead_time < lead_time_max_ticks:
            return rev * (1 - actual_lead_time / lead_time_max_ticks)

        else:
            return 0

    def to_dict(self):
        return {
            'customer': self.customer.id_,
            'batch_size': self.batch_size,
            'batch_price': self.batch_price,
            'fix_price': self.fix_price,
            'lead_time': self.lead_time,
            'lead_time_max': self.lead_time_max,
            'lot_size': self.lot_size,
            'accepted_at': self._accepted_at,
            'accepted_at_formatted': self._plant.simulation.clock.ticks_to_formatted_time(self._accepted_at),
            'finished_at': self._finished_at,
            'finished_at_formatted': self._plant.simulation.clock.ticks_to_formatted_time(self._finished_at),
            'actual_lead_time': self._plant.simulation.clock.to_hours_from_thisk(self._finished_at - self._accepted_at),
            'revenue': self.revenue,
        }

    def accept(self, plant, pipeline_id):
        self._accepted_at = plant.simulation.clock._ticks
        num, rem = divmod(self.batch_size, self.lot_size)

        self.lots = [
            OrderLot(self, self.lot_size, pipeline_id)
            for _ in range(num)
        ]

        if rem:
            self.lots.append(OrderLot(self, rem, pipeline_id))

        self._plant = plant

    def update_state(self):

        if self.is_finished:
            return

        if all(lot.is_finished for lot in self.lots):
            self._finished_at = self._plant.simulation.clock._ticks

    def __str__(self):
        return f'<Order lead_time={self.lead_time}, is_finished={self.is_finished}>'


class OrderLot:

    def __init__(self, order, size, pipeline_id):
        self.order = order
        self.size = size
        self.pipeline_id = pipeline_id
        self.unit_index = 0
        self._is_finished = False

    @property
    def is_finished(self):
        return self._is_finished

    def finish(self):

        if self._is_finished:
            return

        self._is_finished = True
        self.order.update_state()

    def __str__(self):
        return f'<OrderLot pipeline={self.pipeline_id}, size={self.size}>'
