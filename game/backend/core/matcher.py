from .base.exceptions import PlantWarehouseError
from .base.simulation import PlantBase, SerializerMixin, StatisticsMixin
from .queue import QueueMixin


class InventoryMatcher(PlantBase, StatisticsMixin, QueueMixin, SerializerMixin):

    def __init__(self, plant, warehouse, shortage_cost=0, **kwargs):
        super().__init__(plant, **kwargs)
        self._warehouse_id = warehouse

        self.shortage_cost = shortage_cost
        self.seen = set()

    @property
    def _warehouse(self):
        return self._plant.get_unit(self._warehouse_id)

    def to_dict(self):
        return {
            'warehouse': self._warehouse_id,
            'queue_policy': self.queue.policy,
            'shortage_cost': self.shortage_cost,
        }

    def calculate_needed_inventory(self):
        totalNeeded = 0
        for lot in self.queue._items:
            totalNeeded += lot.size

        return totalNeeded

    def update_state(self):
        self._log.debug(f'UPDATE {self}')

        while not self.queue.empty:
            lot = self.queue.pop()

            try:
                self._warehouse.withdraw_inventory(lot.size)
            except PlantWarehouseError:
                if lot not in self.seen:
                    self._plant.pay_costs(
                        self.shortage_cost, 'shortage', credit=True)

                self.seen.add(lot)

                self._log.debug(
                    f'{self} failed to get {lot.size} inv. from {self._warehouse}')
                self.queue.put(lot, True)
                break

            self._log.debug(
                f'{self} received {lot.size} inv. from {self._warehouse}')
            self._plant.pipeline_transfer(lot)
            if lot in self.seen:
                self.seen.remove(lot)

        self._update_stats_multi('queue size', len(
            self.queue), ['last', 'max', 'avg'])

    def __str__(self):
        return ('<InventoryMatcher '
                f'{self.id_}, '
                f'plant={self._plant.id_}, '
                f'queue_policy={self.queue.policy}, '
                f'queue_size={len(self.queue)}>')
