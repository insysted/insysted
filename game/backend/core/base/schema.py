from schema import Schema, And, Or, Use, Optional

from core.queue import QUEUE_POLICIES
from core.customer import OrderAmmountBehaviors, OrderInterarrivalTimeBehaviors
from core.supplier import DeliveryTimeBehaviors
from core.workshop import ItemProcTimeBehaviors
from core.workshop import LotSetupTimeBehaviors
from core.workshop import BreakdownBehaviors


office_params = {
    'contracts': {
        And(str, len): {
            'lead_time': And(Use(int), lambda x: x > 0),
            'lead_time_max': And(Use(int), lambda x: x > 0),
            'batch_price': And(Use(float), lambda x: x > 0),
            'fix_price': And(Use(float), lambda x: x >= 0),
            'batch_size': And(int, lambda x: x > 0),
        },
    },
    'contract_active': And(str, len),
    'lot_size': And(int, lambda x: x > 0),
    Optional('queue_policy'): And(str, lambda x: x in QUEUE_POLICIES),
}

shipment_params = {
    'unit_storage_cost': And(Use(float), lambda x: x >= 0),
    Optional('queue_policy'): And(str, lambda x: x in QUEUE_POLICIES),
}

workshop_params = {
    'machine': {
        'purchase_price': And(Use(float), lambda x: x >= 0),
        'retire_price': And(Use(float), lambda x: x >= 0),
        'item_processing_time_avg': And(Use(float), lambda x: x >= 0),
        'item_processing_time_sigma': And(Use(float), lambda x: x >= 0),
        'item_processing_time_constant': And(Use(float), lambda x: x >= 0),
        'item_processing_time_type': And(str, lambda x: x in ItemProcTimeBehaviors),
        Optional('breakdown'): {
            'proba': And(Use(float), lambda x: 0 <= x <= 1),
        },
        'lot_setup_time_avg': And(Use(float), lambda x: x >= 0),
        'lot_setup_time_sigma': And(Use(float), lambda x: x >= 0),
        'lot_setup_time_constant': And(Use(float), lambda x: x >= 0),
        'lot_setup_time_type': And(str, lambda x: x in LotSetupTimeBehaviors),
        'breakdown_avg': And(Use(float), lambda x: x >= 0),
        'breakdown_sigma': And(Use(float), lambda x: x >= 0),
        'breakdown_constant': And(Use(float), lambda x: x >= 0),
        'breakdown_type': And(str, lambda x: x in BreakdownBehaviors),
    },
    'machine_quantity': And(int, lambda x: x >= 0),
    'queue_policy': And(str, lambda x: x in QUEUE_POLICIES),
}

warehouse_params = {
    'supplier': And(str, len),
    'reorder_point': And(int, lambda x: x >= 0),
    'order_size': And(int, lambda x: x > 0),
    'inventory': And(Use(int), lambda x: x >= 0),
    'unit_storage_cost': And(Use(float), lambda x: x >= 0),
}

matcher_params = {
    'warehouse': And(str, len),
    Optional('queue_policy'): And(str, lambda x: x in QUEUE_POLICIES),
    'shortage_cost': And(int, lambda x: x >= 0),
}

customer_params = {
    'office': And(str, len),
    'order_interarrival_time_normal_avg': And(Use(float), lambda x: x >= 0),
    'order_interarrival_time_normal_sigma': And(Use(float), lambda x: x >= 0),
    'order_interarrival_time_constant': And(Use(float), lambda x: x >= 0),
    'order_interarrival_time_exponential_lambda': And(Use(float), lambda x: x > 0),
    'order_interarrival_time_type': And(str, lambda x: x in OrderInterarrivalTimeBehaviors),

    'order_ammount_normal_avg': And(Use(float), lambda x: x >= 0),
    'order_ammount_normal_sigma': And(Use(float), lambda x: x >= 0),
    'order_ammount_constant': And(Use(int), lambda x: x >= 0),
    'order_ammout_type': And(str, lambda x: x in OrderAmmountBehaviors),

    Optional('supply_chain'): [
        {
            'plant': And(str, len),
            'warehouse': And(str, len),
        },
    ],
}

supplier_params = {
    'warehouse': And(str, len),
    'delivery_time_type': And(str, lambda x: x in DeliveryTimeBehaviors),
    'delivery_time_constant': And(Use(float), lambda x: x >= 0),
    'delivery_time_normal_avg': And(Use(float), lambda x: x >= 0),
    'delivery_time_normal_sigma': And(Use(float), lambda x: x >= 0),
    Optional('supply_chain'): {
        'plant': And(str, len),
        'office': And(str, len),
    },
    **office_params,
}

queue_params = {
    'queue_policy': And(str, lambda x: x in QUEUE_POLICIES),
}

params_dict = {
    'office': Schema(office_params),
    'shipment': Schema(shipment_params),
    'workshop': Schema(workshop_params),
    'warehouse': Schema(warehouse_params),
    'matcher': Schema(matcher_params),
    'customer': Schema(customer_params),
    'supplier': Schema(supplier_params),
    'queue': Schema(queue_params),
}

plant = {
    'cash': And(Use(float), lambda x: x >= 0),
    'wip_orders_max': And(int, lambda x: x > 0),
    'units': {
        And(str, len): Or(
            {
                'class': 'office',
                'params': office_params,
            },
            {
                'class': 'shipment',
                'params': shipment_params,
            },
            {
                'class': 'workshop',
                'params': workshop_params,
            },
            {
                'class': 'warehouse',
                'params': warehouse_params,
            },
            {
                'class': 'matcher',
                'params': matcher_params,
            },
            {
                'class': 'customer',
                'params': customer_params,
            },
            {
                'class': 'supplier',
                'params': supplier_params,
            },
            {
                'class': 'queue',
                'params': queue_params
            },
        ),
    },
    'pipeline': {
        And(str, len): [
            {
                'unit': And(str, len),
            },
        ],
    },
}


clock = {
    'prior_days': And(int, lambda x: x >= 0),
    'play_days': And(int, lambda x: x >= 0),
    'post_days': And(int, lambda x: x >= 0),
    'days_per_hour': And(Use(float), lambda x: x > 0),
    Optional('tick_seconds'): And(int, lambda x: x > 0),
}


plant_schema = Schema({
    Optional('stats'): [{
        'units': dict,
        'state': dict,
        'ranking': And(int, lambda x: x >= 0),
        'score': And(Use(float), lambda x: 0 <= x <= 1),
    }],
    **plant,
})


simulation_schema = Schema({
    'template': {
        'clock': clock,
        'plant': plant,
        'seed': int,
        Optional('supply_chain'): {
            And(str, len): {  # Plant ID
                Optional('suppliers'): {
                    And(str, len): {  # Supplier ID
                        'plant': And(str, len),
                        'office': And(str, len),
                    },
                },
                Optional('customers'): {
                    And(str, len): [{  # Customer ID
                        'plant': And(str, len),
                        'warehouse': And(str, len),
                    }]
                },
            },
        },
    },
    Optional('clock'): {
        Optional('ticks'): And(int, lambda x: x >= 0),
        **clock,
    },
    Optional('order_days'): {
        And(str, len): And(int, lambda x: x >= 0),
    },
    Optional('plants'): {
        And(str, len): plant_schema,
    },
})
