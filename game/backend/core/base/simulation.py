import logging
import numpy as np
from abc import ABC, abstractmethod
from collections import defaultdict
from statistics import mean

from core.utils import uuid
from .config import UUID_LENGTH, LOGGER_NAME


class Mocklog:
    def __init__(self, logger):
        self.logger = logger

    def info(self, *args, **kwargs):
        self.logger.info(*args, **kwargs)

    def exception(self, *args, **kwargs):
        self.logger.exception(*args, **kwargs)

    def debug(self, *args, **kwargs):
        return
        self.logger.debug(*args, **kwargs)


class SimulationBase:

    def __init__(self, id_=None, **kwargs):
        super().__init__(**kwargs)
        self.id_ = id_ or uuid.generate(UUID_LENGTH)

    @property
    def _log(self):
        return Mocklog(logging.getLogger(LOGGER_NAME))


class UpdaterInterface(ABC):

    @abstractmethod
    def update_state(self):
        raise NotImplementedError


class PlantBase(SimulationBase, UpdaterInterface):

    def __init__(self, plant, **kwargs):
        super().__init__(**kwargs)
        self._plant = plant

        self.unique_seed = None
        self.last_rng_tick = None
        self.rng = None

    def get_rng(self):
        if self.unique_seed is None:
            self.unique_seed = self._plant.simulation._template['seed'] + sum(
                ord(c) for c in self.id_)
        ticks = self._plant.simulation.clock._ticks
        if self.last_rng_tick != ticks:
            self.last_rng_tick = ticks
            self.rng = np.random.Generator(
                np.random.MT19937(int(self.unique_seed + ticks)))
        return self.rng


class SerializerInterface(ABC):

    @classmethod
    @abstractmethod
    def from_dict(cls, conf_dict):
        raise NotImplementedError

    @abstractmethod
    def to_dict(self, *args, **kwargs):
        raise NotImplementedError


class SerializerMixin(SerializerInterface):

    @classmethod
    def from_dict(cls, conf_dict):

        try:
            return cls(**conf_dict)
        except TypeError as e:
            raise ValueError(f'Invalid parameters: {e}')


class StatisticsMixin:

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._data_types = ['avg', 'sum', 'last', 'first', 'max', 'min', None]
        self._reset()

    def _reset(self):
        self._data = {}
        self._default = {}
        self._avg_counts = {}

    def get_stats(self):
        stats = self._data

        for key in self._default.keys():
            if key not in stats:
                stats[key] = self._default[key]

        self._reset()

        return {**stats}

    def _set_default(self, metric, value):
        self._default[metric] = value

    def _update_stats(self, metric, value, aggregate=None):

        if aggregate not in self._data_types:
            raise ValueError(f'aggregate must be one of {self._data_types}')

        # handle first entry for all but None and avg
        if metric not in self._data and aggregate not in ['avg', None]:
            self._data[metric] = value
            return

        if aggregate == 'sum':
            self._data[metric] += value
        elif aggregate == 'last':
            self._data[metric] = value
        elif aggregate == 'max':
            self._data[metric] = max(self._data[metric], value)
        elif aggregate == 'min':
            self._data[metric] = min(self._data[metric], value)
        elif aggregate is None:
            if metric not in self._data:
                self._data[metric] = [value]
            else:
                self._data[metric].append(value)
        if aggregate == 'avg':
            if metric not in self._data:
                self._avg_counts[metric] = 1
                self._data[metric] = value
            else:
                self._data[metric] *= self._avg_counts[metric]
                self._data[metric] += value

                self._avg_counts[metric] += 1
                self._data[metric] /= self._avg_counts[metric]

    def _update_stats_multi(self, metric, value, aggregations):
        for aggregate in aggregations:
            self._update_stats(f"{metric} {str(aggregate)}", value, aggregate)
