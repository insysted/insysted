class PlantPaymentError(Exception):
    pass


class PlantWarehouseError(Exception):
    pass


class PlantWorkshopError(Exception):
    pass


class StopSimulation(Exception):
    pass


class AbortSimulation(Exception):
    pass
