from collections import Counter
from enum import Enum

import numpy as np

from .base.exceptions import PlantPaymentError, PlantWorkshopError
from .base.simulation import PlantBase, StatisticsMixin, SerializerMixin
from .queue import QueueMixin, QueueEmptyError
from .utils.random import get_random_seed, notNegative


class MachineStatus(Enum):
    READY = 0
    BUSY = 1
    BROKEN = 2


class ItemProcTimeBehavior:
    Normal = 'normal'
    Constant = 'constant'


ItemProcTimeBehaviors = {
    ItemProcTimeBehavior.Normal,
    ItemProcTimeBehavior.Constant,
}


class LotSetupTimeBehavior:
    Normal = 'normal'
    Constant = 'constant'


LotSetupTimeBehaviors = {
    LotSetupTimeBehavior.Normal,
    LotSetupTimeBehavior.Constant,
}


class BreakdownBehavior:
    Normal = 'normal'
    Constant = 'constant'


BreakdownBehaviors = {
    BreakdownBehavior.Normal,
    BreakdownBehavior.Constant,
}


class Workshop(PlantBase, StatisticsMixin, QueueMixin, SerializerMixin):

    def __init__(self, plant, machine, machine_quantity=0, **kwargs):
        super().__init__(plant, **kwargs)
        self._machine_params = machine
        self._machines = {}

        for _ in range(machine_quantity):
            m = self._initialize_machine()
            self._machines[m.id_] = m

    def _initialize_machine(self):
        return Machine.from_dict({'workshop': self, **self._machine_params})

    @property
    def plant(self):
        return self._plant

    @property
    def machines(self):
        return list(self._machines.values())

    def to_dict(self):
        return {
            'machine': self._machine_params,
            'machine_quantity': len(self._machines),
            'queue_policy': self.queue.policy,
        }

    def can_purchase_machines(self, quantity):
        return self._plant._cash > self._machine_params['purchase_price'] * quantity

    def purchase_machine(self):
        m = self._initialize_machine()

        try:
            self._plant.pay_costs(m.purchase_price, "machine purchase")
        except PlantPaymentError as e:
            raise RuntimeError(e)

        self._machines[m.id_] = m
        self._log.debug(f'{self} bought {m}')

    def retire_machine(self):

        for m in self._machines.values():

            if m.status == MachineStatus.BUSY:
                continue

            self._plant.receive_payment(m.retire_price)
            self._log.debug(f'{self} retired {m}')
            del self._machines[m.id_]

            break

        else:
            raise RuntimeError(
                'Workshop has no machines left, or all machines are busy')

    def update_state(self):
        self._log.debug(f'UPDATE {self}')
        self.calculate_stats()
        if not self._plant.wip:
            self._set_default('broken', 0)
            self._set_default('busy', 0)
            self._set_default('ready', 1)
            self._set_default('queue_size', len(self.queue))
            return

        for machine in self._machines.values():
            machine.update_state()

            if machine.status in (MachineStatus.BUSY, MachineStatus.BROKEN):
                continue

            lot = machine.get_lot()

            if lot is not None:
                self._plant.pipeline_transfer(lot)

            try:
                lot = self.queue.pop()
            except QueueEmptyError:
                continue

            machine.set_lot(lot)

    def calculate_stats(self):
        counter = Counter([m.status for m in self._machines.values()])
        machine_num = len(self._machines)
        for status in MachineStatus:
            status_frac = counter[status] / machine_num if machine_num else 0
            self._update_stats(status.name.lower(),
                               status_frac * 100, aggregate='avg')

        self._update_stats_multi('queue size', len(
            self.queue), ['last', 'max', 'avg'])

    def __str__(self):
        return ('<Workshop '
                f'{self.id_}, '
                f'plant={self._plant.id_}, '
                f'queue_policy={self.queue.policy}, '
                f'machines={len(self._machines)}>')


class Machine(PlantBase, SerializerMixin):

    def __init__(
            self,
            workshop,
            purchase_price,
            retire_price,
            item_processing_time_avg,
            item_processing_time_constant,
            item_processing_time_sigma,
            item_processing_time_type,
            lot_setup_time_avg,
            lot_setup_time_constant,
            lot_setup_time_sigma,
            lot_setup_time_type,
            breakdown_avg,
            breakdown_sigma,
            breakdown_constant,
            breakdown_type,
            breakdown=None,
            **kwargs):
        super().__init__(workshop.plant, **kwargs)
        self.purchase_price = purchase_price
        self.retire_price = retire_price
        self._status = MachineStatus.READY
        self._workshop = workshop
        self.item_processing_time_avg = item_processing_time_avg
        self.item_processing_time_constant = item_processing_time_constant
        self.item_processing_time_sigma = item_processing_time_sigma
        self.item_processing_time_type = item_processing_time_type
        self.lot_setup_time_avg = lot_setup_time_avg
        self.lot_setup_time_constant = lot_setup_time_constant
        self.lot_setup_time_sigma = lot_setup_time_sigma
        self.lot_setup_time_type = lot_setup_time_type
        self.breakdown_avg = breakdown_avg
        self.breakdown_sigma = breakdown_sigma
        self.breakdown_constant = breakdown_constant
        self.breakdown_type = breakdown_type
        self._breakdown = breakdown
        self._lot = None

    @property
    def status(self):
        return self._status

    def to_dict(self):
        params = {
            'purchase_price': self.purchase_price,
            'retire_price': self.retire_price,
            'item_processing_time_avg': self.item_processing_time_avg,
            'item_processing_time_sigma': self.item_processing_time_sigma,
            'item_processing_time_constant': self.item_processing_time_constant,
            'item_processing_time_type': self.item_processing_time_type,
            'lot_setup_time_avg': self.item_processing_time_avg,
            'lot_setup_time_sigma': self.item_processing_time_sigma,
            'lot_setup_time_constant': self.item_processing_time_constant,
            'lot_setup_time_type': self.item_processing_time_type,
            'breakdown_avg': self.breakdown_avg,
            'breakdown_sigma': self.breakdown_sigma,
            'breakdown_constant': self.breakdown_constant,
            'breakdown_type': self.breakdown_type,
        }

        if self._breakdown is not None:
            params['breakdown'] = self._breakdown

        return params

    def get_lot(self):

        if self._status in (MachineStatus.BUSY, MachineStatus.BROKEN):
            return

        lot = self._lot
        self._lot = None

        return lot

    def set_lot(self, lot):
        if self._status != MachineStatus.READY or self._lot is not None:
            raise PlantWorkshopError(f'{self} cannot install {lot}')

        if self.item_processing_time_type == ItemProcTimeBehavior.Normal:
            unit_proc_time = notNegative(self.get_rng().normal(
                self.item_processing_time_avg,
                self.item_processing_time_sigma))
        elif self.item_processing_time_type == ItemProcTimeBehavior.Constant:
            unit_proc_time = self.item_processing_time_constant

        if self.lot_setup_time_type == LotSetupTimeBehavior.Normal:
            lot_setup_time = notNegative(self.get_rng().normal(
                self.lot_setup_time_avg,
                self.lot_setup_time_sigma))
        elif self.lot_setup_time_type == LotSetupTimeBehavior.Constant:
            lot_setup_time = self.lot_setup_time_constant

        proc_mins = round(lot.size * unit_proc_time + lot_setup_time)
        proc_days = proc_mins / 60 / 24

        self._operation_ticks = self._plant.simulation.clock.to_ticks(
            proc_days) + self._plant.simulation.clock._ticks

        self._lot = lot
        self._status = MachineStatus.BUSY

        self._log.debug(
            f'{self} started processing {lot}, done in {proc_mins} min')

    def update_state(self):
        self._log.debug(f'UPDATE {self}')

        if self._status == MachineStatus.READY:
            return

        if self._status == MachineStatus.BUSY and self._boom:
            if self.breakdown_type == BreakdownBehavior.Normal:
                repair_minutes = notNegative(self.get_rng().normal(
                    self.breakdown_avg,
                    self.breakdown_sigma))
            elif self.breakdown_type == BreakdownBehavior.Constant:
                repair_minutes = self.breakdown_constant

            repair_ticks = self._plant.simulation.clock.to_ticks_from_minutes(
                repair_minutes)
            self._processing_ticks = self._operation_ticks + repair_ticks
            self._operation_ticks = repair_ticks + self._plant.simulation.clock._ticks
            self._status = MachineStatus.BROKEN
            self._log.debug(f'{self} repaired in {repair_minutes} minutes')
            return

        if self._plant.simulation.clock._ticks < self._operation_ticks:
            return

        if self._status == MachineStatus.BROKEN:
            self._operation_ticks = self._processing_ticks
            self._status = MachineStatus.BUSY
            self._log.debug(
                f'{self} breakdown fixed, continue processing {self._lot}')
            return

        self._status = MachineStatus.READY
        self._log.debug(f'{self} finished processing {self._lot}')

    @property
    def _boom(self):

        if self._breakdown is None:
            return False

        proba = self._breakdown['proba'] / \
            self._plant.simulation.clock.to_ticks(days=1)

        return self.get_rng().choice([True, False], p=[proba, 1 - proba])

    def __str__(self):
        return ('<Machine '
                f'{self.id_}, '
                f'workshop={self._workshop.id_}, '
                f'status={self._status}>')
