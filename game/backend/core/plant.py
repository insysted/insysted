import core
from .office import Office
from .shipment import Shipment
from .workshop import Workshop
from .matcher import InventoryMatcher
from .supplier import Supplier
from .warehouse import Warehouse
from .customer import Customer

from .base.exceptions import PlantPaymentError
from .base.simulation import (
    SimulationBase,
    UpdaterInterface,
    SerializerMixin,
    StatisticsMixin,
)


class Plant(SimulationBase, UpdaterInterface, StatisticsMixin, SerializerMixin):

    def __init__(
            self,
            simulation,
            cash=1e6,
            wip_orders_max=50000,
            units=None,
            pipeline=None,
            stats=None,
            **kwargs):
        super().__init__(**kwargs)
        self._simulation = simulation
        self._cash = cash
        self._wip_orders_max = wip_orders_max
        self._wip_orders = {}
        self._units = self._initialize_units(units or {})
        self._pipeline = pipeline or []
        self.stats = stats or []

    @property
    def simulation(self):
        return self._simulation

    @property
    def units(self):
        return list(self._units)

    @property
    def wip(self):
        return len(self._wip_orders)

    @property
    def cash(self):
        return round(self._cash)

    def to_dict(self, include_stats=True):
        units = {
            u.id_: {
                'class': core.CLASS_UNIT_MAP[u.__class__],
                'params': u.to_dict(),
            } for u in self._units.values()
        }

        output = {
            'cash': self._cash,
            'wip_orders_max': self._wip_orders_max,
            'units': units,
            'pipeline': self._pipeline,
        }

        if include_stats:
            output['stats'] = self.stats

        return output

    def close_order(self, order):

        if not order.is_finished:
            return False

        try:
            del self._wip_orders[order.id_]
        except KeyError:
            self._log.warning(f'{self} order not found: {order.id_}')

            return False

        order.customer.finished_orders.put(order)
        self._update_stats('closed_orders', order.to_dict())
        self._log.debug(f'{self} sent finished {order} to {order.customer}')

        return True

    def pipeline_transfer(self, lot):
        lot.unit_index += 1

        try:
            next_step = self._pipeline[lot.pipeline_id][lot.unit_index]
            unit = self.get_unit(next_step['unit'])
        except (IndexError, KeyError, ValueError) as e:
            self._log.error(f'{self} failed to transfer {lot}: {e}')

            return

        unit.queue.put(lot)
        self._log.debug(f'{self} transferred {lot} to next unit {unit}')

    def get_unit(self, unit_id):

        try:
            return self._units[unit_id]
        except KeyError:
            raise ValueError(f'Plant unit not found: {unit_id}')

    def update_state(self):
        self._log.debug(f'UPDATE {self}')
        self._update_stats('cash', self.cash, aggregate='last')
        self._update_stats('wip_orders', self.wip, aggregate='last')

        for cost in ['cost order', 'cost shipment storage', 'cost warehouse storage', 'cost machine purchase', 'cost shortage']:
            self._update_stats(cost, 0, aggregate='sum')

        # update each part of the factory

        # first let the customers place orders
        # then let them be processed by the offices
        # afterwards the plant meta unit has to put these orders into the pipeline

        update_order = [Customer, Office, Plant, Warehouse,
                        Supplier, InventoryMatcher, Workshop, Shipment]
        for class_type in update_order:
            if class_type == Plant:  # put orders into the pipeline
                for pipeline_id, order in self._new_orders:
                    order.accept(self, pipeline_id)
                    self._wip_orders[order.id_] = order
                    self._log.debug(f'{self} accepted new {order}')

                    for lot in order.lots:
                        self.pipeline_transfer(lot)
            else:
                for unit in self._units.values():
                    if isinstance(unit, class_type):
                        unit.update_state()

        if self._simulation.clock.last_tick_of_day:
            self._gather_stats()

    def pay_costs(self, value, kind, credit=False):

        if self._cash < value and not credit:
            raise PlantPaymentError(f'Not enough funds, balance {self._cash}$')

        self._cash -= value
        self._update_stats(f"cost {kind}", value, aggregate='sum')
        self._log.debug(f'{self} paid costs {value}$')

    def pay_order(self, order):
        self.pay_costs(order.revenue, "order")
        order.plant.receive_payment(order.revenue)
        self._log.debug(f'{self} paid order revenue {order.revenue}$')

    def receive_payment(self, value):
        self._cash += value
        self._log.debug(f'{self} received payment {value}$')

    def _initialize_units(self, units):
        unit_dict = {}

        for unit_id, unit_conf in units.items():

            try:
                unit_cls = core.UNIT_CLASS_MAP[unit_conf['class']]
            except KeyError:
                raise ValueError(f'Unknown unit class: {unit_conf["class"]}')

            conf_dict = {'id_': unit_id, 'plant': self, **unit_conf['params']}
            unit_dict[unit_id] = unit_cls.from_dict(conf_dict)

        return unit_dict

    def _gather_stats(self):
        unit_stats = {}

        for unit in self._units.values():

            if not isinstance(unit, StatisticsMixin):
                continue

            unit_stats[unit.id_] = unit.get_stats()

        self.stats.append({
            'units': unit_stats,
            'plant': self.get_stats(),
            'state': self.to_dict(include_stats=False),
        })

    @property
    def _new_orders(self):

        for pipeline_id, pipeline in self._pipeline.items():
            self._log.debug(f'{self} checking new orders for {pipeline_id}')

            try:

                # The office is supposed to be the first unit in the pipeline:
                office = self.get_unit(pipeline[0]['unit'])

                if not isinstance(office, Office):
                    raise TypeError(f'Expected office instance, got {office}')

            except (IndexError, KeyError, ValueError) as e:
                self._log.error(f'{self} failed to get new order: {e}')
                raise StopIteration

            while self.wip < self._wip_orders_max:
                order = office.get_order()

                if order is None:
                    break

                yield pipeline_id, order

    def __str__(self):
        return f'<Plant {self.id_}, wip={self.wip}, cash={self._cash}>'
