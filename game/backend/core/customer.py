import numpy as np

from .base.simulation import PlantBase, SerializerMixin
from .utils.random import get_random_seed, notNegative

from .queue import Queue


class OrderInterarrivalTimeBehavior:
    Exponential = 'exponential'
    Normal = 'normal'
    Constant = 'constant'


OrderInterarrivalTimeBehaviors = {
    OrderInterarrivalTimeBehavior.Exponential,
    OrderInterarrivalTimeBehavior.Normal,
    OrderInterarrivalTimeBehavior.Constant,
}


class OrderAmmountBehavior:
    Normal = 'normal'
    Constant = 'constant'


OrderAmmountBehaviors = {
    OrderAmmountBehavior.Normal,
    OrderAmmountBehavior.Constant,
}


class CustomerMixin:

    def __init__(self, office, **kwargs):
        super().__init__(**kwargs)
        self._office_id = office
        self.finished_orders = Queue()

    @property
    def office(self):
        return self._plant.get_unit(self._office_id)


class Customer(PlantBase, CustomerMixin, SerializerMixin):

    def __init__(
            self,
            plant,
            office,

            order_interarrival_time_normal_avg,
            order_interarrival_time_normal_sigma,
            order_interarrival_time_constant,
            order_interarrival_time_exponential_lambda,
            order_interarrival_time_type,

            order_ammount_normal_avg,
            order_ammount_normal_sigma,
            order_ammount_constant,
            order_ammout_type,

            supply_chain=None,
            **kwargs):
        super().__init__(plant, office=office, **kwargs)

        self.order_interarrival_time_normal_avg = order_interarrival_time_normal_avg
        self.order_interarrival_time_normal_sigma = order_interarrival_time_normal_sigma
        self.order_interarrival_time_constant = order_interarrival_time_constant
        self.order_interarrival_time_exponential_lambda = order_interarrival_time_exponential_lambda
        self.order_interarrival_time_type = order_interarrival_time_type

        self.order_ammount_normal_avg = order_ammount_normal_avg
        self.order_ammount_normal_sigma = order_ammount_normal_sigma
        self.order_ammount_constant = order_ammount_constant
        self.order_ammout_type = order_ammout_type

        self.next_order_time = None

        self.supply_chain = supply_chain

    def to_dict(self):
        cust_dict = {
            'order_interarrival_time_normal_avg': self.order_interarrival_time_normal_avg,
            'order_interarrival_time_normal_sigma': self.order_interarrival_time_normal_sigma,
            'order_interarrival_time_constant': self.order_interarrival_time_constant,
            'order_interarrival_time_exponential_lambda': self.order_interarrival_time_exponential_lambda,
            'order_interarrival_time_type': self.order_interarrival_time_type,

            'order_ammount_normal_avg': self.order_ammount_normal_avg,
            'order_ammount_normal_sigma': self.order_ammount_normal_sigma,
            'order_ammount_constant': self.order_ammount_constant,
            'order_ammout_type': self.order_ammout_type,

            'office': self._office_id,
        }

        if self.supply_chain is not None:
            cust_dict['supply_chain'] = self.supply_chain

        return cust_dict

    def get_time_to_next_order(self):
        if self.order_interarrival_time_type == OrderInterarrivalTimeBehavior.Constant:
            return self.order_interarrival_time_constant
        elif self.order_interarrival_time_type == OrderInterarrivalTimeBehavior.Normal:
            return notNegative(round(self.get_rng().normal(
                self.order_interarrival_time_normal_avg,
                self.order_interarrival_time_normal_sigma)))

        elif self.order_interarrival_time_type == OrderInterarrivalTimeBehavior.Exponential:
            return notNegative(round(self.get_rng().exponential(
                self.order_interarrival_time_exponential_lambda
            )))

    def get_order_ammount(self):
        if self.order_ammout_type == OrderAmmountBehavior.Constant:
            return self.order_ammount_constant
        elif self.order_ammout_type == OrderAmmountBehavior.Normal:
            return notNegative(round(self.get_rng().normal(
                self.order_ammount_normal_avg,
                self.order_ammount_normal_sigma)))

    def update_state(self):
        self._log.debug(f'UPDATE {self}')

        if self.supply_chain is not None:
            self._log.debug(
                f'{self.office.id_} involved in supply chain, no action required')
            return

        while not self.finished_orders.empty:
            order = self.finished_orders.pop()
            order.plant.receive_payment(order.revenue)
            self._log.debug(
                f'{self} received finished {order} from {order.plant}')

        # simulate ordering
        if self.next_order_time is None or self.next_order_time <= self._plant.simulation.clock._ticks:
            # we need to place new order(s)
            orderCount = self.get_order_ammount()
            self.office.put_order(customer=self, number_of_orders=orderCount)
            self._log.debug(f'{self} put {orderCount} orders to {self.office}')

            # clac new waiting time
            self.next_order_time = self._plant.simulation.clock._ticks + \
                self._plant.simulation.clock.to_ticks_from_minutes(
                    self.get_time_to_next_order())

    def __str__(self):
        return f'<Customer {self.id_}, plant={self._plant.id_}>'
