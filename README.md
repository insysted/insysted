# INSYSTED
# Live demo
[https://insysted.pom.tu-berlin.de](https://insysted.pom.tu-berlin.de)

# Download
```bash
git clone git@git.tu-berlin.de:insysted/insysted.git
```

# Running
## Production
- Configure `PROJECT_DOMAIN` and `ADMIN_EMAIL` in `.env`
- Configure `env/prod/lti.env`, `env/prod/rabbit.env`, and `env/prod/postgres.env` using the `_example` files as a reference.
- If you want to use the Moodle api to import names etc. into games you need to set the `MOODLE_API_TOKEN` in `env/prod/lti.env` to token provided by Moodle.
- Configure `game/backend/webapp/config/production.py` use `_example` as a refrence (set `USER_ALLOWED_EMAIL_DOMAIN_SUFFIXES` to `None` if you want to allow all domains).
```bash
docker-compose -f docker-compose.proxy.yml -f docker-compose.proxy.prod.yml -f docker-compose.game.yml -f docker-compose.game.prod.yml up --build
```

## Development
```bash
docker-compose -f docker-compose.game.yml -f docker-compose.game.dev.yml up --build
cd ./game/frontend/app/
npm install --include=dev
npm run serve
```

# Reverse Proxy to a remote instance
```
docker-compose -f docker-compose.reverseproxy.yml -f docker-compose.proxy.yml -f docker-compose.proxy.prod.yml up --build
```

## Create and modify the Database
```bash
docker exec -it insysted_game_backend_1 flask init db
docker exec -it insysted_game_backend_1 flask user add -n "John Doe" -e "john.doe@example.com" -p "john" --designer
```

## Running Tests
```bash
docker-compose -f docker-compose.game.test.yml build
docker-compose -f docker-compose.game.test.yml run --rm game_backend_test pytest tests/
```

# Tips
### LTI
When adding insysted as an external LTI Tool be sure that users are in a group and to set
```
group_id=$Moodle.Person.userGroupIds
```
and to use version 1.0/1.1. It may also be necessary allow the tool to set grades.

For more info see [Usage.md](Usage.md)
