# Usage
* Everyone who is to use INSYSTED has to be part of a group in moodle. These groups define the teams of each simulation.
![groups](images/groups.png)
* Start by creating a new game.
![new game](images/new_game.png)
* After making the appropriate changes save the game.
![save game](images/save_game.png)
* Once the game is saved a url for integration with a learning tool, e.g. moodle, is available.
![lti url](images/lti_url.png)
* INSYSTED can be added to a learning system as an external tool.
![external tool](images/external_tool.png)
* Next the settings of the tool need to be configured.
![add tool](images/add_tool.png)
* You need to configure the url, consumer key and the shared secret. These can be found and changed in `/env/prod/lti.env`. 
Be sure to forward the groups set in your learning system to INSYSTED, in moodle this can be achieved by adding `group_id=$Moodle.Person.userGroupIds` to the custom parameters.
![config tool](images/config_tool.png)
* The learning system also needs to be set up to forward the names and email addresses to INSYSTED. It may also be desired to accept grades from INSYSTED.
![tool privacy](images/tool_privacy.png)